
test:
	vendor/bin/phpstan analyze -c phpstan.neon
	XDEBUG_MODE=coverage vendor/bin/phpunit
