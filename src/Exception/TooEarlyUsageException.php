<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Exception;

final class TooEarlyUsageException extends \RuntimeException
{

}
