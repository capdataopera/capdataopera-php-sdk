<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\ValueObject;

final class BooleanObject implements ValueObject
{
    /**
     * @var bool|array<bool>
     */
    private $value;

    /**
     * @param bool|array<bool> $value
     */
    public function __construct($value)
    {
        if (\is_array($value) && \count($value) === 0) {
            throw new \InvalidArgumentException('Array value cannot be empty');
        }

        if (is_array($value)) {
            foreach ($value as $item) {
                $this->validateSingleValue($item);
            }
        } else {
            $this->validateSingleValue($value);
        }
        $this->value = $value;
    }

    public static function fromBoolean(bool $value): self
    {
        return new self($value);
    }

    /**
     * @param mixed $value
     */
    protected function validateSingleValue($value): void
    {
        if (!is_bool($value)) {
            throw new \InvalidArgumentException('Invalid boolean value');
        }
    }

    /**
     * @return bool|array<bool>
     */
    public function getValue()
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return implode(', ', array_map(
            fn (bool $value): string => $value ? 'true' : 'false',
            $this->serialize()
        ));
    }

    /**
     * @return bool[]
     */
    public function serialize(): array
    {
        if (!is_array($this->value)) {
            return [$this->value];
        }
        return $this->value;
    }

    public function isMultiple(): bool
    {
        return is_array($this->value);
    }
}
