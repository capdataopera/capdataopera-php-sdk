<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\ValueObject;

use CapDataOpera\PhpSdk\Exception\BadRelationClassException;
use CapDataOpera\PhpSdk\Exception\OutOfOntologyClassException;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\OntologyClass;

/**
 * @template T of OntologyClass|ExternalThing
 */
final class RelationObject implements ValueObject
{
    /**
     * @var T|array<T>
     */
    protected $value;

    /**
     * @var array<class-string<T>>
     */
    protected array $objectClass;

    /**
     * @param T|T[] $value
     * @param class-string<T>|array<class-string<T>> $objectClass
     */
    public function __construct($value, $objectClass)
    {
        if (!\is_array($objectClass)) {
            $this->objectClass = [$objectClass, ExternalThing::class];
        } else {
            $this->objectClass = $objectClass;
        }

        if (!\in_array(ExternalThing::class, $this->objectClass, true)) {
            $this->objectClass = [...$this->objectClass, ExternalThing::class];
        }

        if (\is_array($value) && \count($value) === 0) {
            throw new \InvalidArgumentException('Array value cannot be empty');
        }

        if (\is_array($value)) {
            foreach ($value as $item) {
                $this->validateSingleValue($item);
            }
        } else {
            $this->validateSingleValue($value);
        }

        $this->value = $value;
    }

    public function isMultiple(): bool
    {
        return is_array($this->value);
    }

    /**
     * @return T|T[]
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return array<class-string<T>>
     */
    public function getObjectClass(): array
    {
        return $this->objectClass;
    }

    public function __toString(): string
    {
        return implode(', ', $this->serialize());
    }

    /**
     * @return string[]
     */
    public function serialize(): array
    {
        if (!is_array($this->value)) {
            return [$this->value->getUri()];
        }
        return array_map(function (OntologyClass $item) {
            return $item->getUri();
        }, $this->value);
    }

    /**
     * @param object $value
     * @return void
     */
    protected function validateSingleValue(object $value): void
    {
        $valid = false;
        foreach ($this->objectClass as $singleObjectClass) {
            if ($value instanceof $singleObjectClass) {
                $valid = true;
            }
        }
        if (false === $valid) {
            throw new BadRelationClassException(sprintf(
                'Expects %s value, %s value given',
                implode('|', $this->objectClass),
                get_class($value)
            ));
        }

        if (!$value instanceof OntologyClass) {
            throw new OutOfOntologyClassException(sprintf(
                '%s class must extend %s',
                get_class($value),
                OntologyClass::class
            ));
        }
    }
}
