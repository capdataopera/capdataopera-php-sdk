<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\ValueObject;

trait StringObjectTrait
{
    /**
     * @var string|array<string>
     */
    private $value;

    /**
     * @param string|array<string> $value
     */
    public function __construct($value)
    {
        if (\is_array($value) && \count($value) === 0) {
            throw new \InvalidArgumentException('Array value cannot be empty');
        }

        if (is_array($value)) {
            foreach ($value as $item) {
                $this->validateSingleValue($item);
            }
        } else {
            $this->validateSingleValue($value);
        }
        $this->value = $value;
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    /**
     * @return string|array<string>
     */
    public function getValue()
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return implode(', ', $this->serialize());
    }

    public function isEmpty(): bool
    {
        if (!is_array($this->value)) {
            return trim($this->value) === '';
        }
        return count(array_filter($this->value)) === 0;
    }

    /**
     * @return string[]
     */
    public function serialize(): array
    {
        if (!is_array($this->value)) {
            return [$this->value];
        }
        return $this->value;
    }

    public function isMultiple(): bool
    {
        return is_array($this->value);
    }
}
