<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\ValueObject;

use DateTimeInterface;

final class DateTimeObject implements ValueObject
{
    /**
     * @var array<DateTimeInterface>|DateTimeInterface
     */
    private $value;

    /**
     * @param DateTimeInterface|int|string|array<DateTimeInterface|int|string> $value
     */
    public function __construct($value)
    {
        if (\is_array($value) && \count($value) === 0) {
            throw new \InvalidArgumentException('Array value cannot be empty');
        }
        if (is_array($value)) {
            $this->value = array_map(function ($item) {
                return $this->parseSingleValue($item);
            }, $value);
            return;
        }
        $this->value = $this->parseSingleValue($value);
    }

    public function diff(DateTimeObject $end): DateIntervalObject
    {
        $startValues = $this->getValue();
        $endValues = $end->getValue();
        if (\is_array($startValues) || \is_array($endValues)) {
            if (!\is_array($startValues)) {
                $startValues = [$startValues];
            }
            if (!\is_array($endValues)) {
                $endValues = [$endValues];
            }
            if (count($startValues) !== count($endValues)) {
                throw new \InvalidArgumentException(
                    'Both DateTimeObject must have the same number of values'
                );
            }
            $intervals = [];
            foreach ($startValues as $index => $startValue) {
                $endValue = $endValues[$index];
                $intervals[] = $startValue->diff($endValue);
            }
            return new DateIntervalObject($intervals);
        }
        return new DateIntervalObject($startValues->diff($endValues));
    }

    /**
     * @param DateTimeInterface|int|string $value
     * @return DateTimeInterface
     */
    private function parseSingleValue($value): DateTimeInterface
    {
        if ($value instanceof DateTimeInterface) {
            return $value;
        }
        if (is_int($value)) {
            return (new \DateTimeImmutable())->setTimestamp($value);
        }
        if (is_string($value)) {
            try {
                return new \DateTimeImmutable($value);
            } catch (\Exception $e) {
                throw new \InvalidArgumentException(
                    $e->getMessage(),
                    0,
                    $e
                );
            }
        }
        throw new \InvalidArgumentException(
            'Value must be a DateTimeInterface, a integer timestamp or a string'
        );
    }

    private function formatSingleValue(DateTimeInterface $value): string
    {
        if ($value instanceof \DateTimeImmutable) {
            return $value->setTimezone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:s\Z');
        }
        $printable = clone $value;
        return $printable->setTimezone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:s\Z');
    }

    public function isMultiple(): bool
    {
        return is_array($this->value);
    }

    /**
     * @return array<DateTimeInterface>|DateTimeInterface
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string Always format in UTC
     */
    public function __toString(): string
    {
        return implode(', ', $this->serialize());
    }

    /**
     * @return string[]
     */
    public function serialize(): array
    {
        if (!is_array($this->value)) {
            return [$this->formatSingleValue($this->value)];
        }
        return array_map(function (DateTimeInterface $item) {
            return $this->formatSingleValue($item);
        }, $this->value);
    }
}
