<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\ValueObject;

use CapDataOpera\PhpSdk\Model\Personne;

final class StringObject implements ValueObject
{
    use StringObjectTrait;

    /**
     * @param mixed $value
     */
    protected function validateSingleValue($value): void
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException('Invalid string value');
        }
    }

    /**
     * @param StringObject|string|array<string> $value
     * @return self
     */
    public function append($value): self
    {
        if (!($value instanceof StringObject)) {
            $toAppend = new StringObject($value);
        } else {
            $toAppend = $value;
        }
        $rootValues = $this->serialize();
        $toAppendValues = $toAppend->serialize();

        /*
         * Result stringObject must have the same count of values as the greater stringObject
         * We combine the values of the two stringObjects by index
         */
        if (count($rootValues) >= count($toAppendValues)) {
            foreach ($rootValues as $index => $rootValue) {
                if (isset($toAppendValues[$index])) {
                    $rootValues[$index] .= $toAppendValues[$index];
                } else {
                    $rootValues[$index] .= $toAppendValues[0] ?? '';
                }
            }
            return new StringObject($rootValues);
        }

        $newValues = [];
        foreach ($toAppendValues as $index => $toAppendValue) {
            if (isset($rootValues[$index])) {
                $newValues[$index] = $rootValues[$index] . $toAppendValue;
            } else {
                $newValues[$index] = $rootValues[0] . $toAppendValue;
            }
        }
        return new StringObject($newValues);
    }

    public function trim(): self
    {
        return new StringObject(array_map('trim', $this->serialize()));
    }
}
