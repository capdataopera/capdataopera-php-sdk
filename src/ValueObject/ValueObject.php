<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\ValueObject;

interface ValueObject
{
    public function isMultiple(): bool;
    public function __toString(): string;
    /**
     * @return array<string|boolean|numeric>
     */
    public function serialize(): array;

    /**
     * @return mixed
     */
    public function getValue();
}
