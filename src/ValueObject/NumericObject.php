<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\ValueObject;

final class NumericObject implements ValueObject
{
    /**
     * @var int|float|string|array<int|float|string>
     */
    private $value;

    /**
     * @param int|float|string|array<int|float|string> $value
     */
    public function __construct($value)
    {
        if (\is_array($value) && \count($value) === 0) {
            throw new \InvalidArgumentException('Array value cannot be empty');
        }

        if (is_array($value)) {
            foreach ($value as $item) {
                $this->validateSingleValue($item);
            }
        } else {
            $this->validateSingleValue($value);
        }
        $this->value = $value;
    }

    /**
     * @param int|float $value
     */
    public static function fromNumeric($value): self
    {
        return new self($value);
    }

    public static function fromInt(int $value): self
    {
        return new self($value);
    }

    public static function fromFloat(float $value): self
    {
        return new self($value);
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    /**
     * @param mixed $value
     */
    protected function validateSingleValue($value): void
    {
        if (!is_numeric($value)) {
            throw new \InvalidArgumentException('Invalid numeric value');
        }
    }

    /**
     * @return int|float|string|array<int|float|string>
     */
    public function getValue()
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return implode(', ', array_map(
            fn ($value): string => (string) $value,
            $this->serialize()
        ));
    }

    /**
     * @return array<int|float|string>
     */
    public function serialize(): array
    {
        if (!is_array($this->value)) {
            return [$this->value];
        }
        return $this->value;
    }

    public function isMultiple(): bool
    {
        return is_array($this->value);
    }
}
