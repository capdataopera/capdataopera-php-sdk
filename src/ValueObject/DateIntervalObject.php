<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\ValueObject;

use DateInterval;

final class DateIntervalObject implements ValueObject
{
    /**
     * @var array<DateInterval>|DateInterval
     */
    private $value;

    /**
     * @param DateInterval|string|array<DateInterval|string> $value
     */
    public function __construct($value)
    {
        if (\is_array($value) && \count($value) === 0) {
            throw new \InvalidArgumentException('Array value cannot be empty');
        }
        if (is_array($value)) {
            $this->value = array_map(function ($item) {
                return $this->parseSingleValue($item);
            }, $value);
            return;
        }
        $this->value = $this->parseSingleValue($value);
    }

    /**
     * @param DateInterval|string $value
     * @return DateInterval
     */
    private function parseSingleValue($value): DateInterval
    {
        if ($value instanceof DateInterval) {
            return $value;
        }

        if (is_string($value)) {
            try {
                return new DateInterval($value);
            } catch (\Exception $e) {
                throw new \InvalidArgumentException(
                    $e->getMessage(),
                    0,
                    $e
                );
            }
        }
        throw new \InvalidArgumentException(
            'Value must be a DateInterval or a string'
        );
    }

    private function formatSingleValue(DateInterval $value): string
    {
        if ($value->days > 0) {
            return $value->format('P%yY%mM%dDT%hH%iM%sS');
        }
        return $value->format('PT%hH%iM%sS');
    }

    public function isMultiple(): bool
    {
        return is_array($this->value);
    }

    /**
     * @return array<DateInterval>|DateInterval
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string Always format in UTC
     */
    public function __toString(): string
    {
        return implode(', ', $this->serialize());
    }

    /**
     * @return string[]
     */
    public function serialize(): array
    {
        if (!is_array($this->value)) {
            return [$this->formatSingleValue($this->value)];
        }
        return array_map(function (DateInterval $item) {
            return $this->formatSingleValue($item);
        }, $this->value);
    }
}
