<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\ValueObject;

final class UriObject implements ValueObject
{
    /*
     * UriObject cannot inherit from StringObject to avoid chain-of-responsibility
     * mapping issues with the LiteralConverter.
     */
    use StringObjectTrait;

    /**
     * @param string|array<string> $value
     */
    public function __construct($value)
    {
        if (\is_array($value) && \count($value) === 0) {
            throw new \InvalidArgumentException('Array value cannot be empty');
        }

        if (is_array($value)) {
            /** @var string|string[] $value */
            $value = array_map(function ($singleValue) {
                $singleValue = $this->normalizeUrl($singleValue);
                $this->validateSingleValue($singleValue);
                return $singleValue;
            }, $value);
        } else {
            $value = $this->normalizeUrl($value);
            /** @var string $value */
            $this->validateSingleValue($value);
        }
        $this->value = $value;
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    protected function normalizeUrl($value)
    {
        if (!\is_string($value)) {
            return $value;
        }

        return \preg_replace_callback(
            '/[^a-zA-Z0-9_\-\.~:\/\?#\[\]@!\$&\'\(\)\*\+,;=]/',
            static function (array $matches): string {
                return \sprintf('%%%02x', \ord($matches[0]));
            },
            $value
        );
    }

    /**
     * @param mixed $value
     */
    protected function validateSingleValue($value): void
    {
        if (!\is_string($value)) {
            throw new \InvalidArgumentException('Invalid string value');
        }

        if (filter_var($value, FILTER_VALIDATE_URL) === false) {
            throw new \InvalidArgumentException('Invalid URI');
        }
    }
}
