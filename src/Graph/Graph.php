<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Graph;

use CapDataOpera\PhpSdk\Model\OntologyClass;
use EasyRdf\Literal;
use EasyRdf\RdfNamespace;

/**
 * A graph is a set of nodes (ontology classes).
 *
 * @implements \IteratorAggregate<string, OntologyClass>
 */
final class Graph implements \IteratorAggregate
{
    /**
     * @var array<string> List of internal nodes uri that must be present in the graph
     */
    private array $requiredInternalNodesUri = [];
    /**
     * @var array<string> List of internal nodes uri that have been added to the graph
     */
    private array $addedInternalNodesUri = [];

    private bool $converted = false;

    private \EasyRdf\Graph $rdfGraph;
    /**
     * @var array<string, OntologyClass>
     */
    private array $nodes = [];

    public function __construct(string $rofLongNamespace = 'https://ontologie.capdataculture.fr/v1/owl/#')
    {
        $this->rdfGraph = new \EasyRdf\Graph();
        RdfNamespace::set($this->getRofNamespace(), $rofLongNamespace);
        RdfNamespace::set('schema', 'https://schema.org/');
    }

    public function add(OntologyClass $node): self
    {
        $this->nodes[$node->getUri()] = $node;
        return $this;
    }

    /**
     * @param string $resource
     * @param string $property
     * @param Literal|string|null $value
     * @param string|null $lang
     * @return $this
     */
    public function addLiteral(string $resource, string $property, $value, string $lang = null): self
    {
        if (null === $value) {
            return $this;
        }
        if (\is_string($value) && empty($value)) {
            return $this;
        }

        $this->rdfGraph->addLiteral(
            $resource,
            $property,
            $value,
            $lang,
        );
        return $this;
    }

    /**
     * @param string $resource
     * @param string $property
     * @param Literal|string $resource2
     * @return $this
     */
    public function addResource(string $resource, string $property, $resource2): self
    {
        if ($resource2 instanceof Literal) {
            $resource2 = $resource2->getValue();
        }
        if (null === $resource2) {
            return $this;
        }
        if (\is_string($resource2) && empty($resource2)) {
            return $this;
        }
        $this->rdfGraph->addResource(
            $resource,
            $property,
            $resource2
        );
        return $this;
    }

    public function getIterator(): \Traversable
    {
        yield from $this->nodes;
    }

    public function getRdfGraph(): \EasyRdf\Graph
    {
        return $this->rdfGraph;
    }

    public function getRofNamespace(): string
    {
        return 'rof';
    }

    /**
     * @return array<string>
     */
    public function getRequiredInternalNodesUri(): array
    {
        return $this->requiredInternalNodesUri;
    }

    public function requiresInternalNodeUri(string $uri): self
    {
        if (\in_array($uri, $this->requiredInternalNodesUri, true)) {
            return $this;
        }
        $this->requiredInternalNodesUri[] = $uri;
        return $this;
    }

    public function registerInternalNodeUri(string $uri): self
    {
        if (\in_array($uri, $this->addedInternalNodesUri, true)) {
            return $this;
        }
        $this->addedInternalNodesUri[] = $uri;
        return $this;
    }

    /**
     * Validate the graph, you MUST throw InvalidGraphException if violations are found.
     *
     * @return array<string> List of violations
     */
    public function validate(): array
    {
        $violationList = [];

        if (!$this->isConverted()) {
            $violationList[] = 'Graph must be converted before validation';
        }

        foreach ($this->requiredInternalNodesUri as $uri) {
            if (!\in_array($uri, $this->addedInternalNodesUri, true)) {
                $violationList[] = sprintf(
                    'Missing internal node uri "%s", did you add it to your graph?',
                    $uri
                );
            }
        }

        return $violationList;
    }

    public function isConverted(): bool
    {
        return $this->converted;
    }

    public function setConverted(): self
    {
        $this->converted = true;
        return $this;
    }
}
