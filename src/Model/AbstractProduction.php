<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\BooleanObject;
use CapDataOpera\PhpSdk\ValueObject\DateTimeObject;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;

abstract class AbstractProduction extends CatalogClass implements HasMedia, Thing, HasParticipation, HasLiveParticipation
{
    use HasImageTrait;
    use HasMediaTrait;
    use HasArkBnfTrait;
    use HasDescriptionTrait;
    use HasParticipationTrait;
    use HasLiveParticipationTrait;

    /**
     * @var RelationObject<Saison>|null
     */
    private ?RelationObject $aPourSaison = null;
    /**
     * @var RelationObject<TypeProduction>|null
     */
    private ?RelationObject $aPourTypeProduction = null;
    /**
     * @var RelationObject<TypePublic>|null
     */
    private ?RelationObject $aPourTypePublic = null;
    /**
     * @var RelationObject<HistoriqueProduction>|null
     */
    private ?RelationObject $historique = null;
    /**
     * @var RelationObject<Lieu>|null
     */
    private ?RelationObject $lieuPublication = null;
    /**
     * @var RelationObject<Oeuvre>|null
     */
    private ?RelationObject $oeuvreRepresentee = null;
    /**
     * @var RelationObject<ProductionPrimaire>|null
     */
    private ?RelationObject $productionPrimaire = null;
    /**
     * @var RelationObject<Collectivite>|null
     */
    private ?RelationObject $programmateur = null;
    private ?DateTimeObject $datePremiere = null;
    private ?DateTimeObject $datePublication = null;
    private ?BooleanObject $jeunePublic = null;
    private ?StringObject $titre = null;

    /**
     * @return RelationObject<Saison>|null
     */
    public function getAPourSaison(): ?RelationObject
    {
        return $this->aPourSaison;
    }

    /**
     * @param RelationObject<Saison>|array<Saison>|Saison|null $aPourSaison
     * @return $this
     */
    public function setAPourSaison($aPourSaison): AbstractProduction
    {
        $this->aPourSaison = $this->transformRelationObject($aPourSaison, Saison::class);
        return $this;
    }

    /**
     * @return RelationObject<TypeProduction>|null
     */
    public function getAPourTypeProduction(): ?RelationObject
    {
        return $this->aPourTypeProduction;
    }

    /**
     * @param RelationObject<TypeProduction>|array<TypeProduction>|TypeProduction|null $aPourTypeProduction
     * @return $this
     */
    public function setAPourTypeProduction($aPourTypeProduction): AbstractProduction
    {
        $this->aPourTypeProduction = $this->transformRelationObject($aPourTypeProduction, TypeProduction::class);
        return $this;
    }

    /**
     * @return RelationObject<TypePublic>|null
     */
    public function getAPourTypePublic(): ?RelationObject
    {
        return $this->aPourTypePublic;
    }

    /**
     * @param RelationObject<TypePublic>|array<TypePublic>|TypePublic|null $aPourTypePublic
     * @return $this
     */
    public function setAPourTypePublic($aPourTypePublic): AbstractProduction
    {
        $this->aPourTypePublic = $this->transformRelationObject($aPourTypePublic, TypePublic::class);
        return $this;
    }

    /**
     * @return RelationObject<HistoriqueProduction>|null
     */
    public function getHistorique(): ?RelationObject
    {
        return $this->historique;
    }

    /**
     * @param RelationObject<HistoriqueProduction>|array<HistoriqueProduction>|HistoriqueProduction|null $historique
     * @return $this
     */
    public function setHistorique($historique): AbstractProduction
    {
        $this->historique = $this->transformRelationObject($historique, HistoriqueProduction::class);
        return $this;
    }

    /**
     * @return RelationObject<Lieu>|null
     */
    public function getLieuPublication(): ?RelationObject
    {
        return $this->lieuPublication;
    }

    /**
     * @param RelationObject<Lieu>|array<Lieu>|Lieu|null $lieuPublication
     * @return $this
     */
    public function setLieuPublication($lieuPublication): AbstractProduction
    {
        $this->lieuPublication = $this->transformRelationObject($lieuPublication, Lieu::class);
        return $this;
    }

    /**
     * @return RelationObject<Oeuvre>|null
     */
    public function getOeuvreRepresentee(): ?RelationObject
    {
        return $this->oeuvreRepresentee;
    }

    /**
     * @param RelationObject<Oeuvre>|array<Oeuvre>|Oeuvre|null $oeuvreRepresentee
     * @return $this
     */
    public function setOeuvreRepresentee($oeuvreRepresentee): AbstractProduction
    {
        $this->oeuvreRepresentee = $this->transformRelationObject($oeuvreRepresentee, Oeuvre::class);
        return $this;
    }

    /**
     * @return RelationObject<ProductionPrimaire>|null
     */
    public function getProductionPrimaire(): ?RelationObject
    {
        return $this->productionPrimaire;
    }

    /**
     * @param RelationObject<ProductionPrimaire>|array<ProductionPrimaire>|ProductionPrimaire|null $productionPrimaire
     * @return $this
     */
    public function setProductionPrimaire($productionPrimaire): AbstractProduction
    {
        $this->productionPrimaire = $this->transformRelationObject($productionPrimaire, ProductionPrimaire::class);
        return $this;
    }

    /**
     * @return RelationObject<Collectivite>|null
     */
    public function getProgrammateur(): ?RelationObject
    {
        return $this->programmateur;
    }

    /**
     * @param RelationObject<Collectivite>|array<Collectivite>|Collectivite|null $programmateur
     * @return $this
     */
    public function setProgrammateur($programmateur): AbstractProduction
    {
        $this->programmateur = $this->transformRelationObject($programmateur, Collectivite::class);
        return $this;
    }

    public function getDatePremiere(): ?DateTimeObject
    {
        return $this->datePremiere;
    }

    /**
     * @param mixed $datePremiere
     * @return $this
     */
    public function setDatePremiere($datePremiere): AbstractProduction
    {
        $this->datePremiere = $this->getValueObjectOfType($datePremiere, DateTimeObject::class);
        return $this;
    }

    public function getDatePublication(): ?DateTimeObject
    {
        return $this->datePublication;
    }

    /**
     * @param mixed $datePublication
     * @return $this
     */
    public function setDatePublication($datePublication): AbstractProduction
    {
        $this->datePublication = $this->getValueObjectOfType($datePublication, DateTimeObject::class);
        return $this;
    }

    public function getJeunePublic(): ?BooleanObject
    {
        return $this->jeunePublic;
    }

    /**
     * @param mixed $jeunePublic
     * @return $this
     */
    public function setJeunePublic($jeunePublic): AbstractProduction
    {
        $this->jeunePublic = $this->getValueObjectOfType($jeunePublic, BooleanObject::class);
        return $this;
    }

    public function getTitre(): ?StringObject
    {
        return $this->titre;
    }

    public function getName(): ?StringObject
    {
        return $this->getTitre();
    }

    /**
     * @param mixed $titre
     * @return $this
     */
    public function setTitre($titre): AbstractProduction
    {
        $this->titre = $this->getValueObjectOfType($titre, StringObject::class);
        return $this;
    }
}
