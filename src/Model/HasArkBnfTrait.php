<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;

trait HasArkBnfTrait
{
    use HasRelationsTrait;
    /**
     * @var RelationObject<ArkBnf>|null
     */
    protected ?RelationObject $arkBnf = null;
    /**
     * @var StringObject|null
     */
    protected ?StringObject $frBnf = null;

    public function getArkBnf(): ?RelationObject
    {
        return $this->arkBnf;
    }

    /**
     * @param RelationObject<ArkBnf>|array<ArkBnf>|ArkBnf $arkBnf
     * @return $this
     */
    public function setArkBnf($arkBnf): self
    {
        $this->arkBnf = $this->transformRelationObject($arkBnf, ArkBnf::class);
        return $this;
    }

    public function getFrBnf(): ?StringObject
    {
        return $this->frBnf;
    }

    /**
     * @param mixed $frBnf
     * @return $this
     */
    public function setFrBnf($frBnf): self
    {
        $this->frBnf = $this->getValueObjectOfType($frBnf, StringObject::class);
        return $this;
    }
}
