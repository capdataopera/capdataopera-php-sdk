<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

/**
 * An ISNI is a Personne or Collectivite node identified by a URI.
 * It is an external resource, not defined in the graph.
 */
final class Isni extends AbstractOntologyClass
{
    public function __construct(string $uri)
    {
        parent::__construct($uri);
    }

    public function isExternalResource(): bool
    {
        return true;
    }
}
