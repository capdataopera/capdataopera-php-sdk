<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;

trait HasLiveParticipationTrait
{
    /**
     * @var RelationObject<Collaboration>|null
     */
    private ?RelationObject $aPourCollaboration = null;
    /**
     * @var RelationObject<Interpretation>|null
     */
    private ?RelationObject $aPourInterpretation = null;
    /**
     * @var RelationObject<MaitriseOeuvre>|null
     */
    private ?RelationObject $aPourMaitriseOeuvre = null;
    /**
     * @var RelationObject<Programmation>|null
     */
    private ?RelationObject $aPourProgrammation = null;


    /**
     * @return RelationObject<Collaboration>|null
     */
    public function getAPourCollaboration(): ?RelationObject
    {
        return $this->aPourCollaboration;
    }
    /**
     * @param RelationObject<Collaboration>|array<Collaboration>|Collaboration|null $aPourCollaboration
     * @return $this
     */
    public function setAPourCollaboration($aPourCollaboration): self
    {
        $this->aPourCollaboration = $this->transformRelationObject($aPourCollaboration, Collaboration::class);
        return $this;
    }
    /**
     * @return RelationObject<Interpretation>|null
     */
    public function getAPourInterpretation(): ?RelationObject
    {
        return $this->aPourInterpretation;
    }
    /**
     * @param RelationObject<Interpretation>|array<Interpretation>|Interpretation|null $aPourInterpretation
     * @return $this
     */
    public function setAPourInterpretation($aPourInterpretation): self
    {
        $this->aPourInterpretation = $this->transformRelationObject($aPourInterpretation, Interpretation::class);
        return $this;
    }
    /**
     * @return RelationObject<MaitriseOeuvre>|null
     */
    public function getAPourMaitriseOeuvre(): ?RelationObject
    {
        return $this->aPourMaitriseOeuvre;
    }
    /**
     * @param RelationObject<MaitriseOeuvre>|array<MaitriseOeuvre>|MaitriseOeuvre|null $aPourMaitriseOeuvre
     * @return $this
     */
    public function setAPourMaitriseOeuvre($aPourMaitriseOeuvre): self
    {
        $this->aPourMaitriseOeuvre = $this->transformRelationObject($aPourMaitriseOeuvre, MaitriseOeuvre::class);
        return $this;
    }
    /**
     * @return RelationObject<Programmation>|null
     */
    public function getAPourProgrammation(): ?RelationObject
    {
        return $this->aPourProgrammation;
    }
    /**
     * @param RelationObject<Programmation>|array<Programmation>|Programmation|null $aPourProgrammation
     * @return $this
     */
    public function setAPourProgrammation($aPourProgrammation): self
    {
        $this->aPourProgrammation = $this->transformRelationObject($aPourProgrammation, Programmation::class);
        return $this;
    }
}
