<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;

interface HasMedia extends OntologyClass
{
    /**
     * @return RelationObject<Media>|null
     */
    public function getMedia(): ?RelationObject;
}
