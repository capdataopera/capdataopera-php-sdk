<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\UriObject;

interface HasSocials extends OntologyClass
{
    /**
     * @return RelationObject<Isni>|null
     */
    public function getIsni(): ?RelationObject;

    public function getTwitter(): ?UriObject;

    public function getFacebook(): ?UriObject;

    public function getMusicStory(): ?UriObject;

    public function getSiteWeb(): ?UriObject;
}
