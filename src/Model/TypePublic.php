<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#TypePublic
 *
 * schema:audienceType
 */
final class TypePublic extends Referentiel
{

}
