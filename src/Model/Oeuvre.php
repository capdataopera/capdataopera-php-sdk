<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\DateIntervalObject;
use CapDataOpera\PhpSdk\ValueObject\DateTimeObject;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;
use CapDataOpera\PhpSdk\ValueObject\UriObject;

final class Oeuvre extends CatalogClass implements Thing, HasMedia, HasParticipation
{
    use HasImageTrait;
    use HasMediaTrait;
    use HasArkBnfTrait;
    use HasDescriptionTrait;
    use HasParticipationTrait;

    private ?StringObject $titre = null;
    private ?StringObject $titreFormeRejet = null;
    private ?StringObject $intrigue = null;
    private ?UriObject $sourceLivret = null;
    private ?DateTimeObject $dateDeCreation = null;
    private ?DateIntervalObject $duree = null;

    /**
     * @var RelationObject<Interpretation>|null
     */
    private ?RelationObject $aPourInterpretation = null;

    /**
     * @var RelationObject<CategorieOeuvre>|null
     */
    private ?RelationObject $categorieOeuvre = null;

    /**
     * @var RelationObject<GenreOeuvre>|null
     */
    private ?RelationObject $genreOeuvre = null;

    /**
     * @var RelationObject<Pays>|null
     */
    private ?RelationObject $paysDeCreation = null;

    /**
     * @var RelationObject<TypeOeuvre>|null
     */
    private ?RelationObject $typeOeuvre = null;

    /**
     * @var RelationObject<Role>|null
     */
    private ?RelationObject $personnage = null;

    public function getTitre(): ?StringObject
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     * @return $this
     */
    public function setTitre($titre): Oeuvre
    {
        $this->titre = $this->getValueObjectOfType($titre, StringObject::class);
        return $this;
    }

    public function getTitreFormeRejet(): ?StringObject
    {
        return $this->titreFormeRejet;
    }

    /**
     * @param mixed $titreFormeRejet
     * @return $this
     */
    public function setTitreFormeRejet($titreFormeRejet): Oeuvre
    {
        $this->titreFormeRejet = $this->getValueObjectOfType($titreFormeRejet, StringObject::class);
        return $this;
    }

    public function getIntrigue(): ?StringObject
    {
        return $this->intrigue;
    }

    /**
     * @param mixed $intrigue
     * @return $this
     */
    public function setIntrigue($intrigue): Oeuvre
    {
        $this->intrigue = $this->getValueObjectOfType($intrigue, StringObject::class);
        return $this;
    }

    public function getSourceLivret(): ?UriObject
    {
        return $this->sourceLivret;
    }

    /**
     * @param mixed $sourceLivret
     * @return $this
     */
    public function setSourceLivret($sourceLivret): Oeuvre
    {
        $this->sourceLivret = $this->getValueObjectOfType($sourceLivret, UriObject::class);
        return $this;
    }

    public function getDateDeCreation(): ?DateTimeObject
    {
        return $this->dateDeCreation;
    }

    /**
     * @param mixed $dateDeCreation
     * @return $this
     */
    public function setDateDeCreation($dateDeCreation): Oeuvre
    {
        $this->dateDeCreation = $this->getValueObjectOfType($dateDeCreation, DateTimeObject::class);
        return $this;
    }

    public function getDuree(): ?DateIntervalObject
    {
        return $this->duree;
    }

    /**
     * @param mixed $duree
     * @return $this
     */
    public function setDuree($duree): Oeuvre
    {
        $this->duree = $this->getValueObjectOfType($duree, DateIntervalObject::class);
        return $this;
    }

    /**
     * @return RelationObject<Interpretation>|null
     */
    public function getAPourInterpretation(): ?RelationObject
    {
        return $this->aPourInterpretation;
    }

    /**
     * @param RelationObject<Interpretation>|array<Interpretation>|Interpretation $aPourInterpretation
     * @return $this
     */
    public function setAPourInterpretation($aPourInterpretation): Oeuvre
    {
        $this->aPourInterpretation = $this->transformRelationObject($aPourInterpretation, Interpretation::class);
        return $this;
    }

    /**
     * @return RelationObject<CategorieOeuvre>|null
     */
    public function getCategorieOeuvre(): ?RelationObject
    {
        return $this->categorieOeuvre;
    }

    /**
     * @param RelationObject<CategorieOeuvre>|array<CategorieOeuvre>|CategorieOeuvre $categorieOeuvre
     * @return $this
     */
    public function setCategorieOeuvre($categorieOeuvre): Oeuvre
    {
        $this->categorieOeuvre = $this->transformRelationObject($categorieOeuvre, CategorieOeuvre::class);
        return $this;
    }

    /**
     * @return RelationObject<GenreOeuvre>|null
     */
    public function getGenreOeuvre(): ?RelationObject
    {
        return $this->genreOeuvre;
    }

    /**
     * @param RelationObject<GenreOeuvre>|array<GenreOeuvre>|GenreOeuvre $genreOeuvre
     * @return $this
     */
    public function setGenreOeuvre($genreOeuvre): Oeuvre
    {
        $this->genreOeuvre = $this->transformRelationObject($genreOeuvre, GenreOeuvre::class);
        return $this;
    }

    /**
     * @return RelationObject<Pays>|null
     */
    public function getPaysDeCreation(): ?RelationObject
    {
        return $this->paysDeCreation;
    }

    /**
     * @param RelationObject<Pays>|array<Pays>|Pays $paysDeCreation
     * @return $this
     */
    public function setPaysDeCreation($paysDeCreation): Oeuvre
    {
        $this->paysDeCreation = $this->transformRelationObject($paysDeCreation, Pays::class);
        return $this;
    }

    /**
     * @return RelationObject<TypeOeuvre>|null
     */
    public function getTypeOeuvre(): ?RelationObject
    {
        return $this->typeOeuvre;
    }

    /**
     * @param RelationObject<TypeOeuvre>|array<TypeOeuvre>|TypeOeuvre $typeOeuvre
     * @return $this
     */
    public function setTypeOeuvre($typeOeuvre): Oeuvre
    {
        $this->typeOeuvre = $this->transformRelationObject($typeOeuvre, TypeOeuvre::class);
        return $this;
    }

    /**
     * @return RelationObject<Role>|null
     */
    public function getPersonnage(): ?RelationObject
    {
        return $this->personnage;
    }

    /**
     * @param RelationObject<Role>|array<Role>|Role $personnage
     * @return $this
     */
    public function setPersonnage($personnage): Oeuvre
    {
        $this->personnage = $this->transformRelationObject($personnage, Role::class);
        return $this;
    }

    public function getName(): ?StringObject
    {
        return $this->getTitre();
    }
}
