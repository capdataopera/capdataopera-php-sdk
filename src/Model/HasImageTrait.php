<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\UriObject;

trait HasImageTrait
{
    /**
     * schema.org/image
     *
     * @var UriObject|null
     */
    protected ?UriObject $image = null;


    public function getImage(): ?UriObject
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return $this
     */
    public function setImage($image): self
    {
        $this->image = $this->getValueObjectOfType($image, UriObject::class);
        return $this;
    }
}
