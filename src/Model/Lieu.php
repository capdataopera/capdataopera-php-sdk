<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\UriObject;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#LieuGeographique
 * https://schema.org/Place
 */
class Lieu extends CatalogClass implements HasMedia, Thing
{
    use HasNameTrait;
    use HasDescriptionTrait;
    use HasImageTrait;
    use HasArkBnfTrait;
    use HasMediaTrait;

    /**
     * @var RelationObject<AdressePostale>|null
     */
    protected ?RelationObject $adresse = null;
    protected ?UriObject $openAgenda = null;

    /**
     * @return RelationObject<AdressePostale>|null
     */
    public function getAdresse(): ?RelationObject
    {
        return $this->adresse;
    }

    /**
     * @param RelationObject<AdressePostale>|array<AdressePostale>|AdressePostale|null $adresse
     * @return $this
     */
    public function setAdresse($adresse): Lieu
    {
        $this->adresse = $this->transformRelationObject($adresse, AdressePostale::class);
        return $this;
    }

    public function getOpenAgenda(): ?UriObject
    {
        return $this->openAgenda;
    }

    /**
     * @param mixed $openAgenda
     * @return $this
     */
    public function setOpenAgenda($openAgenda): Lieu
    {
        $this->openAgenda = $this->getValueObjectOfType($openAgenda, UriObject::class);
        return $this;
    }
}
