<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;

interface HasArkBnf extends OntologyClass
{
    /**
     * @return RelationObject<ArkBnf>|null
     */
    public function getArkBnf(): ?RelationObject;
    /**
     * @return StringObject|null
     */
    public function getFrBnf(): ?StringObject;
}
