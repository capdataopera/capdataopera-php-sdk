<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\UriObject;

interface HasImage extends OntologyClass
{
    public function getImage(): ?UriObject;
}
