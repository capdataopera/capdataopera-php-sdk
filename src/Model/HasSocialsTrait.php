<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\UriObject;

trait HasSocialsTrait
{
    protected ?UriObject $siteWeb = null;
    protected ?UriObject $facebook = null;
    protected ?UriObject $twitter = null;
    protected ?UriObject $musicStory = null;
    /**
     * @var RelationObject<Isni>|null
     */
    protected ?RelationObject $isni = null;

    public function getSiteWeb(): ?UriObject
    {
        return $this->siteWeb;
    }

    /**
     * @param mixed $siteWeb
     * @return $this
     */
    public function setSiteWeb($siteWeb): self
    {
        $this->siteWeb = $this->getValueObjectOfType($siteWeb, UriObject::class);
        return $this;
    }

    public function getFacebook(): ?UriObject
    {
        return $this->facebook;
    }

    /**
     * @param mixed $facebook
     * @return $this
     */
    public function setFacebook($facebook): self
    {
        $this->facebook = $this->getValueObjectOfType($facebook, UriObject::class);
        return $this;
    }

    public function getTwitter(): ?UriObject
    {
        return $this->twitter;
    }

    /**
     * @param mixed $twitter
     * @return $this
     */
    public function setTwitter($twitter): self
    {
        $this->twitter = $this->getValueObjectOfType($twitter, UriObject::class);
        return $this;
    }

    public function getMusicStory(): ?UriObject
    {
        return $this->musicStory;
    }

    /**
     * @param mixed $musicStory
     * @return $this
     */
    public function setMusicStory($musicStory): self
    {
        $this->musicStory = $this->getValueObjectOfType($musicStory, UriObject::class);
        return $this;
    }

    /**
     * @return RelationObject<Isni>|null
     */
    public function getIsni(): ?RelationObject
    {
        return $this->isni;
    }

    /**
     * @param RelationObject<Isni>|array<Isni>|Isni $isni
     * @return $this
     */
    public function setIsni($isni): self
    {
        $this->isni = $this->transformRelationObject($isni, Isni::class);
        return $this;
    }
}
