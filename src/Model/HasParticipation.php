<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;

interface HasParticipation extends OntologyClass
{
    /**
     * @return RelationObject<Auteur>|null
     */
    public function getAPourAuteur(): ?RelationObject;

    /**
     * @return RelationObject<MentionProduction>|null
     */
    public function getAPourMentionProduction(): ?RelationObject;

    /**
     * @return RelationObject<Partenariat>|null
     */
    public function getAPourPartenariat(): ?RelationObject;

    /**
     * @return RelationObject<Participation>|null
     */
    public function getAPourParticipation(): ?RelationObject;
}
