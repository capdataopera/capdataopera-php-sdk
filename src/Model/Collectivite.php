<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;
use CapDataOpera\PhpSdk\ValueObject\UriObject;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#Collectivite
 * https://schema.org/Organization
 */
class Collectivite extends CatalogClass implements HasSocials, HasMedia, Thing
{
    use HasNameTrait;
    use HasDescriptionTrait;
    use HasArkBnfTrait;
    use HasRelationsTrait;
    use HasSocialsTrait;
    use HasImageTrait;
    use HasMediaTrait;

    protected ?StringObject $siret = null;

    /**
     * @var RelationObject<Fonction>|null
     */
    protected ?RelationObject $aPourFonction = null;

    /**
     * @var RelationObject<StatusJuridique>|null
     */
    protected ?RelationObject $statusJuridique = null;

    /**
     * rof: nomFormeRejet
     *
     * @var StringObject|null
     */
    protected ?StringObject $nomFormeRejet = null;
    protected ?UriObject $openAgenda = null;
    /**
     * @var RelationObject<AdressePostale>|null Adresse administrative
     */
    protected ?RelationObject $adresse = null;
    /**
     * @var RelationObject<Lieu>|null Lieux vers les salles de spectacles de la collectivite
     */
    private ?RelationObject $aPourLieu = null;

    /**
     * @return StringObject|null
     */
    public function getNom(): ?StringObject
    {
        return $this->getName();
    }

    /**
     * @param mixed $nom
     * @return $this
     */
    public function setNom($nom): Collectivite
    {
        return $this->setName($nom);
    }

    public function getSiret(): ?StringObject
    {
        return $this->siret;
    }

    /**
     * @param mixed $siret
     * @return $this
     */
    public function setSiret($siret): Collectivite
    {
        $this->siret = $this->getValueObjectOfType($siret, StringObject::class);
        return $this;
    }

    /**
     * @return RelationObject<Fonction>|null
     */
    public function getAPourFonction(): ?RelationObject
    {
        return $this->aPourFonction;
    }

    /**
     * @param RelationObject<Fonction>|array<Fonction>|Fonction|null $aPourFonction
     * @return static
     */
    public function setAPourFonction($aPourFonction): self
    {
        $this->aPourFonction = $this->transformRelationObject($aPourFonction, Fonction::class);
        return $this;
    }

    /**
     * @return RelationObject<StatusJuridique>|null
     */
    public function getStatusJuridique(): ?RelationObject
    {
        return $this->statusJuridique;
    }

    /**
     * @param RelationObject<StatusJuridique>|array<StatusJuridique>|StatusJuridique|null $statusJuridique
     * @return $this
     */
    public function setStatusJuridique($statusJuridique): Collectivite
    {
        $this->statusJuridique = $this->transformRelationObject($statusJuridique, StatusJuridique::class);
        return $this;
    }

    public function getNomFormeRejet(): ?StringObject
    {
        return $this->nomFormeRejet;
    }

    /**
     * @param mixed $nomFormeRejet
     * @return $this
     */
    public function setNomFormeRejet($nomFormeRejet): Collectivite
    {
        $this->nomFormeRejet = $this->getValueObjectOfType($nomFormeRejet, StringObject::class);
        return $this;
    }

    public function getOpenAgenda(): ?UriObject
    {
        return $this->openAgenda;
    }

    /**
     * @param mixed $openAgenda
     * @return $this
     */
    public function setOpenAgenda($openAgenda): Collectivite
    {
        $this->openAgenda = $this->getValueObjectOfType($openAgenda, UriObject::class);
        return $this;
    }

    /**
     * @return RelationObject<AdressePostale>|null
     */
    public function getAdresse(): ?RelationObject
    {
        return $this->adresse;
    }

    /**
     * @param RelationObject<AdressePostale>|array<AdressePostale>|AdressePostale|null $adresse
     * @return $this
     */
    public function setAdresse($adresse): Collectivite
    {
        $this->adresse = $this->transformRelationObject($adresse, AdressePostale::class);
        return $this;
    }

    /**
     * @return RelationObject<Lieu>|null
     */
    public function getAPourLieu(): ?RelationObject
    {
        return $this->aPourLieu;
    }

    /**
     * @param RelationObject<Lieu>|array<Lieu>|Lieu|null $aPourLieu
     * @return $this
     */
    public function setAPourLieu($aPourLieu): Collectivite
    {
        $this->aPourLieu = $this->transformRelationObject($aPourLieu, Lieu::class);
        return $this;
    }
}
