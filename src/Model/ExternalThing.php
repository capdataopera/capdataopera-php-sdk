<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

/**
 * An external thing is an abstract graph node identified by a URI.
 * It is an external resource, not defined in the graph.
 */
final class ExternalThing extends AbstractOntologyClass
{
    public function __construct(string $uri)
    {
        parent::__construct($uri);
    }

    public function isExternalResource(): bool
    {
        return true;
    }
}
