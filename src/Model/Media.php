<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\StringObject;
use CapDataOpera\PhpSdk\ValueObject\UriObject;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#Media
 * https://schema.org/MediaObject
 */
class Media extends AbstractOntologyClass implements Thing
{
    use HasNameTrait;
    use HasImageTrait;
    use HasArkBnfTrait;
    use HasDescriptionTrait;

    protected ?StringObject $mentions = null;

    protected ?UriObject $contentUrl = null;


    public function getMentions(): ?StringObject
    {
        return $this->mentions;
    }

    /**
     * @param mixed $mentions
     * @return $this
     */
    public function setMentions($mentions): self
    {
        $this->mentions = $this->getValueObjectOfType($mentions, StringObject::class);
        return $this;
    }

    public function getContentUrl(): ?UriObject
    {
        return $this->contentUrl;
    }

    /**
     * @param mixed $contentUrl
     * @return $this
     */
    public function setContentUrl($contentUrl): self
    {
        $this->contentUrl = $this->getValueObjectOfType($contentUrl, UriObject::class);
        return $this;
    }

    /**
     * Alias for setContentUrl.
     *
     * @param mixed $source
     * @return $this
     */
    public function setSource($source): self
    {
        return $this->setContentUrl($source);
    }

    /**
     * Alias for getContentUrl.
     */
    public function getSource(): ?UriObject
    {
        return $this->getContentUrl();
    }
}
