<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;

/**
 * Specialized Participations for production, events.
 */
interface HasLiveParticipation extends OntologyClass
{
    /**
     * @return RelationObject<Programmation>|null
     */
    public function getAPourProgrammation(): ?RelationObject;

    /**
     * @return RelationObject<MaitriseOeuvre>|null
     */
    public function getAPourMaitriseOeuvre(): ?RelationObject;

    /**
     * @return RelationObject<Interpretation>|null
     */
    public function getAPourInterpretation(): ?RelationObject;

    /**
     * @return RelationObject<Collaboration>|null
     */
    public function getAPourCollaboration(): ?RelationObject;
}
