<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#Saison
 */
final class Saison extends Referentiel implements HasMedia
{
    use HasRelationsTrait;
    use HasMediaTrait;
}
