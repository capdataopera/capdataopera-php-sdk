<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;

interface OntologyClass
{
    /**
     * @return string Subject URI
     */
    public function getUri(): string;

    public function getIdentifiantRof(): ?StringObject;

    /**
     * @return RelationObject<ExternalThing>|null
     */
    public function getSameAs(): ?RelationObject;

    /**
     * @return bool True if the subject is an external resource (not defined in the graph)
     */
    public function isExternalResource(): bool;
}
