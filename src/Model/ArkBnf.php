<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

/**
 * An ArkBNF is a graph node identified by an URI.
 * It is an external resource, not defined in the graph.
 */
final class ArkBnf extends AbstractOntologyClass
{
    public function __construct(string $uri)
    {
        parent::__construct($uri);
    }

    public function isExternalResource(): bool
    {
        return true;
    }
}
