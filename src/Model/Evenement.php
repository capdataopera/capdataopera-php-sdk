<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\BooleanObject;
use CapDataOpera\PhpSdk\ValueObject\DateIntervalObject;
use CapDataOpera\PhpSdk\ValueObject\DateTimeObject;
use CapDataOpera\PhpSdk\ValueObject\NumericObject;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;
use CapDataOpera\PhpSdk\ValueObject\UriObject;

final class Evenement extends CatalogClass implements HasMedia, Thing, HasParticipation, HasLiveParticipation
{
    use HasImageTrait;
    use HasMediaTrait;
    use HasArkBnfTrait;
    use HasDescriptionTrait;
    use HasParticipationTrait;
    use HasLiveParticipationTrait;

    /**
     * @var RelationObject<Lieu>|null
     */
    private ?RelationObject $aPourLieu = null;
    /**
     * @var RelationObject<Production>|null
     */
    private ?RelationObject $aPourProduction = null;
    /**
     * @var RelationObject<TypePublic>|null
     */
    private ?RelationObject $aPourTypePublic = null;
    /**
     * @var RelationObject<TypeEvenement>|null
     */
    private ?RelationObject $typeEvenement = null;

    private ?StringObject $titre = null;
    private ?BooleanObject $annulation = null;
    private ?BooleanObject $passCulture = null;
    private ?NumericObject $passCultureNombrePlace = null;
    private ?NumericObject $passCulturePrix = null;
    private ?DateTimeObject $dateDebut = null;
    private ?DateTimeObject $dateFin = null;
    private ?DateIntervalObject $duree = null;
    protected ?UriObject $openAgenda = null;

    public function getName(): ?StringObject
    {
        return $this->getTitre();
    }

    /**
     * @return RelationObject<Lieu>|null
     */
    public function getAPourLieu(): ?RelationObject
    {
        return $this->aPourLieu;
    }

    /**
     * @param RelationObject<Lieu>|array<Lieu>|Lieu|null $aPourLieu
     * @return $this
     */
    public function setAPourLieu($aPourLieu): Evenement
    {
        $this->aPourLieu = $this->transformRelationObject($aPourLieu, Lieu::class);
        return $this;
    }

    /**
     * @return RelationObject<Production>|null
     */
    public function getAPourProduction(): ?RelationObject
    {
        return $this->aPourProduction;
    }

    /**
     * @param RelationObject<Production>|array<Production>|Production|null $aPourProduction
     * @return $this
     */
    public function setAPourProduction($aPourProduction): Evenement
    {
        $this->aPourProduction = $this->transformRelationObject($aPourProduction, Production::class);
        return $this;
    }

    /**
     * @return RelationObject<TypePublic>|null
     */
    public function getAPourTypePublic(): ?RelationObject
    {
        return $this->aPourTypePublic;
    }

    /**
     * @param RelationObject<TypePublic>|array<TypePublic>|TypePublic|null $aPourTypePublic
     * @return $this
     */
    public function setAPourTypePublic($aPourTypePublic): Evenement
    {
        $this->aPourTypePublic = $this->transformRelationObject($aPourTypePublic, TypePublic::class);
        return $this;
    }

    /**
     * @return RelationObject<TypeEvenement>|null
     */
    public function getTypeEvenement(): ?RelationObject
    {
        return $this->typeEvenement;
    }

    /**
     * @param RelationObject<TypeEvenement>|array<TypeEvenement>|TypeEvenement|null $typeEvenement
     * @return $this
     */
    public function setTypeEvenement($typeEvenement): Evenement
    {
        $this->typeEvenement = $this->transformRelationObject($typeEvenement, TypeEvenement::class);
        return $this;
    }

    public function getTitre(): ?StringObject
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     * @return $this
     */
    public function setTitre($titre): Evenement
    {
        $this->titre = $this->getValueObjectOfType($titre, StringObject::class);
        return $this;
    }

    public function getAnnulation(): ?BooleanObject
    {
        return $this->annulation;
    }

    /**
     * @param mixed $annulation
     * @return $this
     */
    public function setAnnulation($annulation): Evenement
    {
        $this->annulation = $this->getValueObjectOfType($annulation, BooleanObject::class);
        return $this;
    }

    public function getPassCulture(): ?BooleanObject
    {
        return $this->passCulture;
    }

    /**
     * @param mixed $passCulture
     * @return $this
     */
    public function setPassCulture($passCulture): Evenement
    {
        $this->passCulture = $this->getValueObjectOfType($passCulture, BooleanObject::class);
        return $this;
    }

    public function getPassCultureNombrePlace(): ?NumericObject
    {
        return $this->passCultureNombrePlace;
    }

    /**
     * @param mixed $passCultureNombrePlace
     * @return $this
     */
    public function setPassCultureNombrePlace($passCultureNombrePlace): Evenement
    {
        $this->passCultureNombrePlace = $this->getValueObjectOfType(
            $passCultureNombrePlace,
            NumericObject::class
        );
        return $this;
    }

    public function getPassCulturePrix(): ?NumericObject
    {
        return $this->passCulturePrix;
    }

    /**
     * @param mixed $passCulturePrix
     * @return $this
     */
    public function setPassCulturePrix($passCulturePrix): Evenement
    {
        $this->passCulturePrix = $this->getValueObjectOfType(
            $passCulturePrix,
            NumericObject::class
        );
        return $this;
    }

    public function getDateDebut(): ?DateTimeObject
    {
        return $this->dateDebut;
    }

    /**
     * @param mixed $dateDebut
     * @return $this
     */
    public function setDateDebut($dateDebut): Evenement
    {
        $this->dateDebut = $this->getValueObjectOfType(
            $dateDebut,
            DateTimeObject::class
        );
        return $this;
    }

    public function getDateFin(): ?DateTimeObject
    {
        return $this->dateFin;
    }

    /**
     * @param mixed $dateFin
     * @return $this
     */
    public function setDateFin($dateFin): Evenement
    {
        $this->dateFin = $this->getValueObjectOfType(
            $dateFin,
            DateTimeObject::class
        );
        return $this;
    }

    /**
     * @return DateIntervalObject|null Duration of the event if auto-calculated if dateDebut and dateFin are set
     */
    public function getDuree(): ?DateIntervalObject
    {
        if (null === $this->duree && null !== $this->dateDebut && null !== $this->dateFin) {
            return $this->dateDebut->diff($this->dateFin);
        }
        return $this->duree;
    }

    /**
     * @param mixed $duree
     * @return $this
     */
    public function setDuree($duree): Evenement
    {
        $this->duree = $this->getValueObjectOfType(
            $duree,
            DateIntervalObject::class
        );
        return $this;
    }

    public function getOpenAgenda(): ?UriObject
    {
        return $this->openAgenda;
    }

    /**
     * @param mixed $openAgenda
     * @return $this
     */
    public function setOpenAgenda($openAgenda): Evenement
    {
        $this->openAgenda = $this->getValueObjectOfType($openAgenda, UriObject::class);
        return $this;
    }
}
