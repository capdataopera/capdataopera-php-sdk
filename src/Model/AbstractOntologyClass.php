<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\Exception\TooEarlyUsageException;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;
use CapDataOpera\PhpSdk\ValueObject\UriObject;

abstract class AbstractOntologyClass implements OntologyClass
{
    use HasRelationsTrait;
    use HasValueObjectPropertiesTrait;

    private ?UriObject $uri = null;

    /**
     * @var RelationObject<ExternalThing>|null
     */
    private ?RelationObject $sameAs = null;

    private ?StringObject $identifiantRof = null;

    private bool $externalResource = false;

    /**
     * @param mixed $uri
     */
    public function __construct($uri = null)
    {
        if (\is_string($uri)) {
            $uri = trim($uri);
        }
        if ($uri !== null) {
            $this->setUri($uri);
        }
    }

    /**
     * @param mixed $uri
     * @return $this
     */
    public function setUri($uri): self
    {
        $this->uri = $this->getValueObjectOfType($uri, UriObject::class);
        return $this;
    }

    public function getUri(): string
    {
        if ($this->uri === null || $this->uri->isEmpty()) {
            throw new TooEarlyUsageException('Uri is not set yet');
        }
        return $this->uri->__toString();
    }

    /**
     * @return RelationObject<ExternalThing>|null
     */
    public function getSameAs(): ?RelationObject
    {
        return $this->sameAs;
    }

    /**
     * @param RelationObject<ExternalThing>|array<ExternalThing>|ExternalThing|null $sameAs
     * @return static
     */
    public function setSameAs($sameAs): self
    {
        $this->sameAs = $this->transformRelationObject($sameAs, ExternalThing::class);
        return $this;
    }

    public function getIdentifiantRof(): ?StringObject
    {
        return $this->identifiantRof;
    }

    /**
     * @param mixed $identifiantRof
     * @return $this
     */
    public function setIdentifiantRof($identifiantRof): self
    {
        $this->identifiantRof = $this->getValueObjectOfType($identifiantRof, StringObject::class);
        return $this;
    }

    public function isExternalResource(): bool
    {
        return $this->externalResource;
    }

    /**
     * @param bool $externalResource Whether this node is an external resource or not
     * @return $this
     */
    public function setExternalResource(bool $externalResource): self
    {
        $this->externalResource = $externalResource;
        return $this;
    }
}
