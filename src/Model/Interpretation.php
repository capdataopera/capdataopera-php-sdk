<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;

final class Interpretation extends Participation
{
    /**
     * @var RelationObject<Role>|null
     */
    protected ?RelationObject $aPourRole = null;

    /**
     * @return RelationObject<Role>|null
     */
    public function getAPourRole(): ?RelationObject
    {
        return $this->aPourRole;
    }

    /**
     * @param RelationObject<Role>|array<Role>|Role|null $aPourRole
     * @return $this
     */
    public function setAPourRole($aPourRole): Interpretation
    {
        $this->aPourRole = $this->transformRelationObject($aPourRole, Role::class);
        return $this;
    }
}
