<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;

trait HasParticipationTrait
{
    /**
     * @var RelationObject<Auteur>|null
     */
    private ?RelationObject $aPourAuteur = null;
    /**
     * @var RelationObject<MentionProduction>|null
     */
    private ?RelationObject $aPourMentionProduction = null;
    /**
     * @var RelationObject<Partenariat>|null
     */
    private ?RelationObject $aPourPartenariat = null;
    /**
     * Parent relationship.
     *
     * @var RelationObject<Participation>|null
     */
    private ?RelationObject $aPourParticipation = null;


    /**
     * @return RelationObject<Auteur>|null
     */
    public function getAPourAuteur(): ?RelationObject
    {
        return $this->aPourAuteur;
    }
    /**
     * @param RelationObject<Auteur>|array<Auteur>|Auteur|null $aPourAuteur
     * @return $this
     */
    public function setAPourAuteur($aPourAuteur): self
    {
        $this->aPourAuteur = $this->transformRelationObject($aPourAuteur, Auteur::class);
        return $this;
    }
    /**
     * @return RelationObject<MentionProduction>|null
     */
    public function getAPourMentionProduction(): ?RelationObject
    {
        return $this->aPourMentionProduction;
    }
    /**
     * @param RelationObject<MentionProduction>|array<MentionProduction>|MentionProduction|null $aPourMentionProduction
     * @return $this
     */
    public function setAPourMentionProduction($aPourMentionProduction): self
    {
        $this->aPourMentionProduction = $this->transformRelationObject($aPourMentionProduction, MentionProduction::class);
        return $this;
    }
    /**
     * @return RelationObject<Partenariat>|null
     */
    public function getAPourPartenariat(): ?RelationObject
    {
        return $this->aPourPartenariat;
    }
    /**
     * @param RelationObject<Partenariat>|array<Partenariat>|Partenariat|null $aPourPartenariat
     * @return $this
     */
    public function setAPourPartenariat($aPourPartenariat): self
    {
        $this->aPourPartenariat = $this->transformRelationObject($aPourPartenariat, Partenariat::class);
        return $this;
    }
    /**
     * @return RelationObject<Participation>|null
     */
    public function getAPourParticipation(): ?RelationObject
    {
        return $this->aPourParticipation;
    }
    /**
     * @param RelationObject<Participation>|array<Participation>|Participation|null $aPourParticipation
     * @return $this
     */
    public function setAPourParticipation($aPourParticipation): self
    {
        $this->aPourParticipation = $this->transformRelationObject($aPourParticipation, Participation::class);
        return $this;
    }
}
