<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#Participation
 */
class Participation extends AbstractOntologyClass
{
    use HasRelationsTrait;

    /**
     * @var RelationObject<Fonction>|null
     */
    protected ?RelationObject $aPourFonction = null;

    /**
     * @var RelationObject<Personne|Collectivite>|null
     */
    protected ?RelationObject $aPourParticipant = null;

    /**
     * @return RelationObject<Fonction>|null
     */
    public function getAPourFonction(): ?RelationObject
    {
        return $this->aPourFonction;
    }

    /**
     * @param RelationObject<Fonction>|array<Fonction>|Fonction|null $aPourFonction
     * @return $this
     */
    public function setAPourFonction($aPourFonction): Participation
    {
        $this->aPourFonction = $this->transformRelationObject($aPourFonction, Fonction::class);
        return $this;
    }

    /**
     * @return RelationObject<Personne|Collectivite>|null
     */
    public function getAPourParticipant(): ?RelationObject
    {
        return $this->aPourParticipant;
    }

    /**
     * @param RelationObject<Personne|Collectivite>|array<Personne|Collectivite>|Personne|Collectivite|null $aPourParticipant
     * @return $this
     */
    public function setAPourParticipant($aPourParticipant): Participation
    {
        $this->aPourParticipant = $this->transformRelationObject($aPourParticipant, [
            Personne::class,
            Collectivite::class
        ]);
        return $this;
    }
}
