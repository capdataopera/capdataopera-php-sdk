<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\DateTimeObject;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;

abstract class CatalogClass extends AbstractOntologyClass
{
    use HasRelationsTrait;

    protected ?DateTimeObject $dateCreationRessource = null;
    protected ?DateTimeObject $dateModificationRessource = null;
    protected ?DateTimeObject $catalogageSourceDate = null;

    /**
     * @var RelationObject<Pays>|null
     */
    protected ?RelationObject $catalogageSourcePays = null;

    /**
     * @var RelationObject<Collectivite>|null
     */
    protected ?RelationObject $catalogageSourceAgence = null;

    public function getDateCreationRessource(): ?DateTimeObject
    {
        return $this->dateCreationRessource;
    }

    /**
     * @param mixed $dateCreationRessource Date de creation de la ressource sur le site source
     * @return $this
     */
    public function setDateCreationRessource($dateCreationRessource): CatalogClass
    {
        $this->dateCreationRessource = $this->getValueObjectOfType($dateCreationRessource, DateTimeObject::class);
        return $this;
    }

    public function getDateModificationRessource(): ?DateTimeObject
    {
        return $this->dateModificationRessource;
    }

    /**
     * @param mixed $dateModificationRessource Date de derniere modification de la ressource sur le site source
     * @return $this
     */
    public function setDateModificationRessource($dateModificationRessource): CatalogClass
    {
        $this->dateModificationRessource = $this->getValueObjectOfType($dateModificationRessource, DateTimeObject::class);
        return $this;
    }

    public function getCatalogageSourceDate(): ?DateTimeObject
    {
        return $this->catalogageSourceDate;
    }

    /**
     * @param mixed $catalogageSourceDate Date de modification publique de la notice sur le site source
     * @return $this
     */
    public function setCatalogageSourceDate($catalogageSourceDate): CatalogClass
    {
        $this->catalogageSourceDate = $this->getValueObjectOfType($catalogageSourceDate, DateTimeObject::class);
        return $this;
    }

    /**
     * @return RelationObject<Pays>|null Date de modification publique de la notice sur le site source
     */
    public function getCatalogageSourcePays(): ?RelationObject
    {
        return $this->catalogageSourcePays;
    }

    /**
     * @param RelationObject<Pays>|array<Pays>|Pays|null $catalogageSourcePays
     * @return $this
     */
    public function setCatalogageSourcePays($catalogageSourcePays): CatalogClass
    {
        $this->catalogageSourcePays = $this->transformRelationObject($catalogageSourcePays, Pays::class);
        return $this;
    }

    /**
     * @return RelationObject<Collectivite>|null
     */
    public function getCatalogageSourceAgence(): ?RelationObject
    {
        return $this->catalogageSourceAgence;
    }

    /**
     * @param RelationObject<Collectivite>|array<Collectivite>|Collectivite|null $catalogageSourceAgence
     * @return $this
     */
    public function setCatalogageSourceAgence($catalogageSourceAgence): self
    {
        $this->catalogageSourceAgence = $this->transformRelationObject($catalogageSourceAgence, Collectivite::class);
        return $this;
    }
}
