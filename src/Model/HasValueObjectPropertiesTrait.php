<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\ValueObject;

trait HasValueObjectPropertiesTrait
{
    /**
     * @template T of ValueObject
     * @param mixed $value
     * @param class-string<T> $class
     * @return T|null
     */
    protected function getValueObjectOfType($value, string $class): ?ValueObject
    {
        if (null === $value || '' === $value) {
            return null;
        } elseif ($value instanceof $class) {
            return $value;
        }
        return new $class($value);
    }
}
