<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\StringObject;

/**
 * https://schema.org/Thing
 */
interface Thing extends HasImage, HasArkBnf
{
    public function getName(): ?StringObject;
    public function getDescription(): ?StringObject;
}
