<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#Personne
 * https://schema.org/Person
 */
class Personne extends CatalogClass implements HasSocials, HasMedia, Thing
{
    use HasRelationsTrait;
    use HasSocialsTrait;
    use HasImageTrait;
    use HasMediaTrait;
    use HasArkBnfTrait;
    use HasDescriptionTrait;

    /**
     * schema.org/Person: familyName
     * rof: nom
     *
     * @var StringObject|null
     */
    protected ?StringObject $nom = null;

    /**
     * rof: nomFormeRejet
     *
     * @var StringObject|null
     */
    protected ?StringObject $nomFormeRejet = null;
    /**
     * schema.org/Person: givenName
     * rof: prenom
     *
     * @var StringObject|null
     */
    protected ?StringObject $prenom = null;
    protected ?StringObject $biographie = null;
    protected ?StringObject $dates = null;

    /**
     * @var RelationObject<Fonction>|null
     */
    protected ?RelationObject $aPourFonction = null;

    /**
     * @var RelationObject<Fonction>|null
     */
    protected ?RelationObject $aPourProfession = null;

    public function getNom(): ?StringObject
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     * @return $this
     */
    public function setNom($nom): Personne
    {
        $this->nom = $this->getValueObjectOfType($nom, StringObject::class);
        return $this;
    }

    public function getNomFormeRejet(): ?StringObject
    {
        return $this->nomFormeRejet;
    }

    /**
     * @param mixed $nomFormeRejet
     * @return $this
     */
    public function setNomFormeRejet($nomFormeRejet): Personne
    {
        $this->nomFormeRejet = $this->getValueObjectOfType($nomFormeRejet, StringObject::class);
        return $this;
    }

    public function getPrenom(): ?StringObject
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     * @return $this
     */
    public function setPrenom($prenom): Personne
    {
        $this->prenom = $this->getValueObjectOfType($prenom, StringObject::class);
        return $this;
    }

    public function getName(): ?StringObject
    {
        if (null === $this->getPrenom()) {
            return $this->getNom();
        }
        if (null === $this->getNom()) {
            return $this->getPrenom();
        }
        // Merge nom and prenom into a single StringObject
        return $this->getPrenom()->trim()->append(' ')->append($this->getNom())->trim();
    }

    public function getBiographie(): ?StringObject
    {
        return $this->biographie;
    }

    /**
     * @param mixed $biographie
     * @return $this
     */
    public function setBiographie($biographie): Personne
    {
        $this->biographie = $this->getValueObjectOfType($biographie, StringObject::class);
        return $this;
    }

    public function getDates(): ?StringObject
    {
        return $this->dates;
    }

    /**
     * @param mixed $dates
     * @return $this
     */
    public function setDates($dates): Personne
    {
        $this->dates = $this->getValueObjectOfType($dates, StringObject::class);
        return $this;
    }

    /**
     * @return RelationObject<Fonction>|null
     */
    public function getAPourFonction(): ?RelationObject
    {
        return $this->aPourFonction;
    }

    /**
     * @param RelationObject<Fonction>|array<Fonction>|Fonction $aPourFonction
     * @return $this
     */
    public function setAPourFonction($aPourFonction): Personne
    {
        $this->aPourFonction = $this->transformRelationObject($aPourFonction, Fonction::class);
        return $this;
    }

    /**
     * @return RelationObject<Fonction>|null
     */
    public function getAPourProfession(): ?RelationObject
    {
        return $this->aPourProfession;
    }

    /**
     * @param RelationObject<Fonction>|array<Fonction>|Fonction $aPourProfession
     * @return $this
     */
    public function setAPourProfession($aPourProfession): Personne
    {
        $this->aPourProfession = $this->transformRelationObject($aPourProfession, Fonction::class);
        return $this;
    }
}
