<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#Referentiel
 *
 * @see http://www.w3.org/2004/02/skos/core#Concept
 */
class Referentiel extends AbstractOntologyClass implements Thing
{
    use HasImageTrait;
    use HasArkBnfTrait;
    use HasDescriptionTrait;

    /**
     * skos:prefLabel
     * schema:name
     *
     * @var StringObject|null
     */
    protected ?StringObject $label = null;

    /**
     * skos:prefLabel
     * schema:name
     *
     * @var StringObject|null
     */
    protected ?StringObject $altLabel = null;

    /**
     * skos:inScheme
     *
     * @var RelationObject<ExternalThing>|null
     */
    protected ?RelationObject $inScheme = null;

    public function getName(): ?StringObject
    {
        return $this->getLabel();
    }

    public function getLabel(): ?StringObject
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     * @return $this
     */
    public function setLabel($label): self
    {
        $this->label = $this->getValueObjectOfType($label, StringObject::class);
        return $this;
    }

    public function getAltLabel(): ?StringObject
    {
        return $this->altLabel;
    }

    /**
     * @param mixed $altLabel
     * @return $this
     */
    public function setAltLabel($altLabel): self
    {
        $this->altLabel = $this->getValueObjectOfType($altLabel, StringObject::class);
        return $this;
    }

    /**
     * @return RelationObject<ExternalThing>|null
     */
    public function getInScheme(): ?RelationObject
    {
        return $this->inScheme;
    }

    /**
     * @param RelationObject<ExternalThing>|array<ExternalThing>|ExternalThing $inScheme
     * @return $this
     */
    public function setInScheme($inScheme): self
    {
        $this->inScheme = $this->transformRelationObject($inScheme, ExternalThing::class);
        return $this;
    }
}
