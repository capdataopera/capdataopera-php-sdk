<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\StringObject;

trait HasDescriptionTrait
{
    protected ?StringObject $description = null;

    public function getDescription(): ?StringObject
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return $this
     */
    public function setDescription($description): self
    {
        $this->description = $this->getValueObjectOfType($description, StringObject::class);
        return $this;
    }
}
