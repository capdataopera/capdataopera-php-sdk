<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;

trait HasRelationsTrait
{
    /**
     * @template T of OntologyClass|ExternalThing
     * @param RelationObject<T>|array<T>|T|null $value
     * @param class-string<T>|array<class-string<T>> $class
     * @return RelationObject<T>|null
     */
    protected function transformRelationObject($value, $class): ?RelationObject
    {
        if (null === $value) {
            return null;
        }
        if ($value instanceof RelationObject) {
            if (!\is_array($class)) {
                $class = [$class, ExternalThing::class];
            }
            if (!\in_array(ExternalThing::class, $class, true)) {
                $class = [...$class, ExternalThing::class];
            }

            if (is_array($value->getObjectClass())) {
                if (array_intersect($value->getObjectClass(), $class) === []) {
                    throw new \InvalidArgumentException(sprintf(
                        'Value must be an instance of %s<%s>',
                        RelationObject::class,
                        implode('|', $class)
                    ));
                }
            } elseif (!\in_array($value->getObjectClass(), $class, true)) {
                throw new \InvalidArgumentException(sprintf(
                    'Value must be an instance of %s<%s>',
                    RelationObject::class,
                    implode('|', $class)
                ));
            }
            return $value;
        }
        return new RelationObject($value, $class);
    }
}
