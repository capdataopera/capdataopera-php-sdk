<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\StringObject;

trait HasNameTrait
{
    protected ?StringObject $name = null;

    public function getName(): ?StringObject
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name): self
    {
        $this->name = $this->getValueObjectOfType($name, StringObject::class);
        return $this;
    }
}
