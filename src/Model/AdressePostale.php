<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#Adresse
 * https://schema.org/PostalAddress
 */
final class AdressePostale extends AbstractOntologyClass implements Thing
{
    use HasNameTrait;
    use HasDescriptionTrait;
    use HasImageTrait;
    use HasArkBnfTrait;

    protected ?StringObject $adressePostale = null;
    protected ?StringObject $codePostal = null;
    protected ?StringObject $commune = null;
    /**
     * @var RelationObject<Pays>|null
     */
    private ?RelationObject $pays = null;

    public function getAdressePostale(): ?StringObject
    {
        return $this->adressePostale;
    }

    /**
     * @param mixed $adressePostale
     * @return $this
     */
    public function setAdressePostale($adressePostale): AdressePostale
    {
        $this->adressePostale = $this->getValueObjectOfType($adressePostale, StringObject::class);
        return $this;
    }

    public function getCodePostal(): ?StringObject
    {
        return $this->codePostal;
    }

    /**
     * @param mixed $codePostal
     * @return $this
     */
    public function setCodePostal($codePostal): AdressePostale
    {
        $this->codePostal = $this->getValueObjectOfType($codePostal, StringObject::class);
        return $this;
    }

    public function getCommune(): ?StringObject
    {
        return $this->commune;
    }

    /**
     * @param mixed $commune
     * @return $this
     */
    public function setCommune($commune): AdressePostale
    {
        $this->commune = $this->getValueObjectOfType($commune, StringObject::class);
        return $this;
    }



    /**
     * @return RelationObject<Pays>|null
     */
    public function getPays(): ?RelationObject
    {
        return $this->pays;
    }

    /**
     * @param RelationObject<Pays>|array<Pays>|Pays $pays
     * @return $this
     */
    public function setPays($pays): AdressePostale
    {
        $this->pays = $this->transformRelationObject($pays, Pays::class);
        return $this;
    }
}
