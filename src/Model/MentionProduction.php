<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

/**
 * https://ontologie.capdataculture.fr/v1/owl/#MentionProduction
 */
final class MentionProduction extends Participation
{
}
