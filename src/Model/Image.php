<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\UriObject;

final class Image extends Media
{
    /**
     * For BC with HasImageTrait
     *
     * @param mixed $image
     * @return $this
     */
    public function setImage($image): self
    {
        return $this->setContentUrl($image);
    }

    public function getImage(): ?UriObject
    {
        return $this->getContentUrl();
    }
}
