<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Model;

use CapDataOpera\PhpSdk\ValueObject\RelationObject;

trait HasMediaTrait
{
    /**
     * @var RelationObject<Media>|null
     */
    protected ?RelationObject $media = null;

    public function getMedia(): ?RelationObject
    {
        return $this->media;
    }

    /**
     * @param RelationObject<Media>|array<Media>|Media $media
     * @return $this
     */
    public function setMedia($media): self
    {
        $this->media = $this->transformRelationObject($media, Media::class);
        return $this;
    }
}
