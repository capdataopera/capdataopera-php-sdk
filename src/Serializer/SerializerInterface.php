<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer;

use CapDataOpera\PhpSdk\Graph\Graph;

interface SerializerInterface
{
    /**
     * @param Graph $graph
     * @param string $format
     * @param string[] $ontologies
     * @return mixed
     */
    public function serialize(Graph $graph, string $format, array $ontologies = []);
}
