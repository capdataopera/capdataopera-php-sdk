<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\UnsupportedConverterException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\ValueObject\DateTimeObject;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;
use EasyRdf\Literal;

final class DateTimeObjectLiteralConverter implements LiteralConverter
{
    public function convert(?ValueObject $object, Graph $graph): array
    {
        if ($object === null) {
            return [];
        }

        if (!$object instanceof DateTimeObject) {
            throw new UnsupportedConverterException('This converter only supports '.DateTimeObject::class);
        }

        return array_map(
            fn (string $date): Literal => new Literal($date, null,'xsd:dateTime'),
            $object->serialize()
        );
    }
}
