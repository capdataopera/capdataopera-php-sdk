<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\UnsupportedConverterException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\OntologyClass;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;

final class RelationObjectLiteralConverter implements LiteralConverter
{
    public function convert(?ValueObject $object, Graph $graph): array
    {
        if ($object === null) {
            return [];
        }

        if (!$object instanceof RelationObject) {
            throw new UnsupportedConverterException('This converter only supports '.RelationObject::class);
        }

        /*
         * Require all internal resource nodes to be present in the graph
         * This is needed to ensure that the graph is complete
         */
        $relations = $object->getValue();
        if (!\is_array($relations)) {
            $relations = [$relations];
        }
        $internalRelations = array_filter($relations, fn(OntologyClass $relation) => !$relation->isExternalResource());
        foreach ($internalRelations as $relation) {
            $graph->requiresInternalNodeUri($relation->getUri());
        }

        return $object->serialize();
    }
}
