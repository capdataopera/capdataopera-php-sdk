<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\UnsupportedConverterException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\ValueObject\DateIntervalObject;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;
use EasyRdf\Literal;

final class DateIntervalObjectLiteralConverter implements LiteralConverter
{
    public function convert(?ValueObject $object, Graph $graph): array
    {
        if ($object === null) {
            return [];
        }

        if (!$object instanceof DateIntervalObject) {
            throw new UnsupportedConverterException('This converter only supports '.DateIntervalObject::class);
        }

        return array_map(
            fn (string $date): Literal => new Literal($date, null,'xsd:duration'),
            $object->serialize()
        );
    }
}
