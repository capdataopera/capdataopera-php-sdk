<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\UnsupportedConverterException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;
use EasyRdf\Literal;

interface LiteralConverter
{
    /**
     * @param ValueObject|null $object
     * @param Graph $graph
     * @return array<Literal|string>
     * @throws UnsupportedConverterException when converter does not support given object
     */
    public function convert(?ValueObject $object, Graph $graph): array;
}
