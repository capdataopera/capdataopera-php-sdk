<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\UnsupportedConverterException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\ValueObject\StringObject;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;
use EasyRdf\Literal;

final class StringObjectLiteralConverter implements LiteralConverter
{
    public function convert(?ValueObject $object, Graph $graph): array
    {
        if ($object === null) {
            return [];
        }

        if (!$object instanceof StringObject) {
            throw new UnsupportedConverterException('This converter only supports '.StringObject::class);
        }

        return array_map(
            fn (string $value): Literal => new Literal(trim(strip_tags($value))),
            $object->serialize()
        );
    }
}
