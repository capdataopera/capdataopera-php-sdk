<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\UnsupportedConverterException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\ValueObject\UriObject;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;
use EasyRdf\Literal;

final class UriObjectLiteralConverter implements LiteralConverter
{
    public function convert(?ValueObject $object, Graph $graph): array
    {
        if ($object === null) {
            return [];
        }

        if (!$object instanceof UriObject) {
            throw new UnsupportedConverterException('This converter only supports '.UriObject::class);
        }

        return array_map(
            fn (string $uri): Literal => new Literal($uri, null, 'xsd:anyURI'),
            $object->serialize()
        );
    }
}
