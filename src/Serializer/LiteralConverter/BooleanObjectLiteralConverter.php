<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\UnsupportedConverterException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\ValueObject\BooleanObject;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;
use EasyRdf\Literal;

final class BooleanObjectLiteralConverter implements LiteralConverter
{
    public function convert(?ValueObject $object, Graph $graph): array
    {
        if ($object === null) {
            return [];
        }

        if (!$object instanceof BooleanObject) {
            throw new UnsupportedConverterException('This converter only supports '.BooleanObject::class);
        }

        return array_map(
            fn (bool $value): Literal => new Literal($value ? 'true' : 'false', null, 'xsd:boolean'),
            $object->serialize()
        );
    }
}
