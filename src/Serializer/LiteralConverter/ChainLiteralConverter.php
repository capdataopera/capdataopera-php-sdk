<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\UnsupportedConverterException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;

final class ChainLiteralConverter implements LiteralConverter
{
    /**
     * @var LiteralConverter[]
     */
    private array $converters;

    /**
     * @param LiteralConverter[] $converters
     */
    public function __construct(array $converters)
    {
        if (empty($converters)) {
            throw new \InvalidArgumentException('At least one converter must be provided');
        }
        if (count($converters) !== count(array_filter(
            $converters,
            fn($converter) => $converter instanceof LiteralConverter
        ))) {
            throw new \InvalidArgumentException(
                'All converters must implement ' . LiteralConverter::class
            );
        }
        $this->converters = $converters;
    }

    public function convert(?ValueObject $object, Graph $graph): array
    {
        if ($object === null) {
            return [];
        }

        foreach ($this->converters as $converter) {
            try {
                return $converter->convert($object, $graph);
            } catch (UnsupportedConverterException $e) {
                // continue
            }
        }
        throw new UnsupportedConverterException('No converter found for ' . get_class($object));
    }
}
