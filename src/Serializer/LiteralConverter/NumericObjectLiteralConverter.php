<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\UnsupportedConverterException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\ValueObject\NumericObject;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;
use EasyRdf\Literal;

final class NumericObjectLiteralConverter implements LiteralConverter
{
    public function convert(?ValueObject $object, Graph $graph): array
    {
        if ($object === null) {
            return [];
        }

        if (!$object instanceof NumericObject) {
            throw new UnsupportedConverterException('This converter only supports '.NumericObject::class);
        }

        return array_map(
            fn ($value): Literal => new Literal(
                (string) $value,
                null,
                \is_int($value) ? 'xsd:integer' : 'xsd:float'
            ),
            $object->serialize()
        );
    }
}
