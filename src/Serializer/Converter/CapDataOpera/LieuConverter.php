<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Lieu;

final class LieuConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Lieu) {
            return;
        }

        // Exceptional case: Lieu does not have capdata:nom or capdata:titre
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getName(),
            $graph,
            [$graph->getRofNamespace() . ':titre']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAdresse(),
            $graph,
            [$graph->getRofNamespace() . ':adresse']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getOpenAgenda(),
            $graph,
            [$graph->getRofNamespace() . ':openAgenda']
        );

        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':LieuGeographique');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
