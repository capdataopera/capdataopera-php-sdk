<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Personne;

final class PersonneConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Personne) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getNom(),
            $graph,
            [$graph->getRofNamespace() . ':nom']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getNomFormeRejet(),
            $graph,
            [$graph->getRofNamespace() . ':nomFormeRejet']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getPrenom(),
            $graph,
            [$graph->getRofNamespace() . ':prenom']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getBiographie(),
            $graph,
            [$graph->getRofNamespace() . ':biographie']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDates(),
            $graph,
            [$graph->getRofNamespace() . ':dates']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourFonction(),
            $graph,
            [$graph->getRofNamespace() . ':aPourFonction']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourProfession(),
            $graph,
            [$graph->getRofNamespace() . ':aPourProfession']
        );

        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Personne');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
