<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Auteur;
use CapDataOpera\PhpSdk\Model\Collaboration;
use CapDataOpera\PhpSdk\Model\Interpretation;
use CapDataOpera\PhpSdk\Model\MaitriseOeuvre;
use CapDataOpera\PhpSdk\Model\MentionProduction;
use CapDataOpera\PhpSdk\Model\Partenariat;
use CapDataOpera\PhpSdk\Model\Participation;
use CapDataOpera\PhpSdk\Model\Programmation;

final class ParticipationConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Participation) {
            return;
        }

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourFonction(),
            $graph,
            [$graph->getRofNamespace() . ':aPourFonction']
        );

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourParticipant(),
            $graph,
            [$graph->getRofNamespace() . ':aPourParticipant']
        );

        /**
         * Supports child class Interpretation.
         * @see https://ontologie.capdataculture.fr/v1/owl/#Participation
         */
        if ($object instanceof Auteur) {
            $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Auteur');
        }

        if ($object instanceof Collaboration) {
            $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Collaboration');
        }

        if ($object instanceof Interpretation) {
            $this->convertValueObjectToResource(
                $object->getUri(),
                $object->getAPourRole(),
                $graph,
                [$graph->getRofNamespace() . ':aPourRole']
            );
            // Always start with rof namespace
            $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Interpretation');
        }

        if ($object instanceof MaitriseOeuvre) {
            $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':MaitriseOeuvre');
        }

        if ($object instanceof MentionProduction) {
            $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':MentionProduction');
        }

        if ($object instanceof Partenariat) {
            $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Partenariat');
        }

        if ($object instanceof Programmation) {
            $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Programmation');
        }

        // Always start with rof namespace
        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Participation');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
