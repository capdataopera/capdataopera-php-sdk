<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Evenement;

final class EvenementConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Evenement) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDateDebut(),
            $graph,
            [$graph->getRofNamespace() . ':dateDebut']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDateFin(),
            $graph,
            [$graph->getRofNamespace() . ':dateFin']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDuree(),
            $graph,
            [$graph->getRofNamespace() . ':duree']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getAnnulation(),
            $graph,
            [$graph->getRofNamespace() . ':annulation']
        );

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getTitre(),
            $graph,
            [$graph->getRofNamespace() . ':titre']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getPassCulture(),
            $graph,
            [$graph->getRofNamespace() . ':passCulture']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getPassCultureNombrePlace(),
            $graph,
            [$graph->getRofNamespace() . ':passCultureNombrePlace']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getPassCulturePrix(),
            $graph,
            [$graph->getRofNamespace() . ':passCulturePrix']
        );

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourLieu(),
            $graph,
            [$graph->getRofNamespace() . ':aPourLieu']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourTypePublic(),
            $graph,
            [$graph->getRofNamespace() . ':aPourTypePublic']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourProduction(),
            $graph,
            [$graph->getRofNamespace() . ':aPourProduction']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getTypeEvenement(),
            $graph,
            [$graph->getRofNamespace() . ':typeEvenement']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getOpenAgenda(),
            $graph,
            [$graph->getRofNamespace() . ':openAgenda']
        );

        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Evenement');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
