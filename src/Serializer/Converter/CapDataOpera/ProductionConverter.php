<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\AbstractProduction;
use CapDataOpera\PhpSdk\Model\Production;
use CapDataOpera\PhpSdk\Model\ProductionPrimaire;

final class ProductionConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof AbstractProduction) {
            return;
        }

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourSaison(),
            $graph,
            [$graph->getRofNamespace() . ':aPourSaison']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourTypeProduction(),
            $graph,
            [$graph->getRofNamespace() . ':aPourTypeProduction']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourTypePublic(),
            $graph,
            [$graph->getRofNamespace() . ':aPourTypePublic']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getHistorique(),
            $graph,
            [$graph->getRofNamespace() . ':historique']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getJeunePublic(),
            $graph,
            [$graph->getRofNamespace() . ':jeunePublic']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getLieuPublication(),
            $graph,
            [$graph->getRofNamespace() . ':lieuPublication']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getOeuvreRepresentee(),
            $graph,
            [$graph->getRofNamespace() . ':oeuvreRepresentee']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getProductionPrimaire(),
            $graph,
            [$graph->getRofNamespace() . ':productionPrimaire']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getProgrammateur(),
            $graph,
            [$graph->getRofNamespace() . ':programmateur']
        );


        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getTitre(),
            $graph,
            [$graph->getRofNamespace() . ':titre']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDatePremiere(),
            $graph,
            [$graph->getRofNamespace() . ':datePremiere']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDatePublication(),
            $graph,
            [$graph->getRofNamespace() . ':datePublication']
        );

        if ($object instanceof Production) {
            $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Production');
        }
        if ($object instanceof ProductionPrimaire) {
            $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':ProductionPrimaire');
        }
        $graph->registerInternalNodeUri($object->getUri());
    }
}
