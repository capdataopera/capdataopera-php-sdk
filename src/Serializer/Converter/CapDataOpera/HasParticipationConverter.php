<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\HasParticipation;

final class HasParticipationConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof HasParticipation) {
            return;
        }

        /*
         * Parent relationship
         */
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourParticipation(),
            $graph,
            [
                $graph->getRofNamespace() . ':aPourParticipation',
            ]
        );

        /*
         * Children relationships
         */
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourAuteur(),
            $graph,
            [
                $graph->getRofNamespace() . ':aPourParticipation',
                $graph->getRofNamespace() . ':aPourAuteur',
            ]
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourMentionProduction(),
            $graph,
            [
                $graph->getRofNamespace() . ':aPourParticipation',
                $graph->getRofNamespace() . ':aPourMentionProduction',
            ]
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourPartenariat(),
            $graph,
            [
                $graph->getRofNamespace() . ':aPourParticipation',
                $graph->getRofNamespace() . ':aPourPartenariat',
            ]
        );
    }
}
