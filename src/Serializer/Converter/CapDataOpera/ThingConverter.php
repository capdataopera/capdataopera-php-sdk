<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Thing;

final class ThingConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Thing) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDescription(),
            $graph,
            [$graph->getRofNamespace() . ':description']
        );
    }
}
