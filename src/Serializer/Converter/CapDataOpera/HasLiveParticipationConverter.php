<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\HasLiveParticipation;

final class HasLiveParticipationConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof HasLiveParticipation) {
            return;
        }

        /*
         * Children relationships
         */
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourInterpretation(),
            $graph,
            [
                $graph->getRofNamespace() . ':aPourParticipation',
                $graph->getRofNamespace() . ':aPourInterpretation',
            ]
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourCollaboration(),
            $graph,
            [
                $graph->getRofNamespace() . ':aPourParticipation',
                $graph->getRofNamespace() . ':aPourCollaboration',
            ]
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourMaitriseOeuvre(),
            $graph,
            [
                $graph->getRofNamespace() . ':aPourParticipation',
                $graph->getRofNamespace() . ':aPourMaitriseOeuvre',
            ]
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourProgrammation(),
            $graph,
            [
                $graph->getRofNamespace() . ':aPourParticipation',
                $graph->getRofNamespace() . ':aPourProgrammation',
            ]
        );
    }
}
