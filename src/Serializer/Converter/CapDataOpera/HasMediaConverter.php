<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\HasMedia;

final class HasMediaConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof HasMedia) {
            return;
        }

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getMedia(),
            $graph,
            [$graph->getRofNamespace() . ':media']
        );
    }
}
