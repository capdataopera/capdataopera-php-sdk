<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Referentiel;

final class ReferentielConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Referentiel) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getLabel(),
            $graph,
            ['skos:prefLabel']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getAltLabel(),
            $graph,
            ['skos:altLabel']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getInScheme(),
            $graph,
            ['skos:inScheme']
        );

        $graph->addResource($object->getUri(), "rdf:type", 'skos:Concept');
    }
}
