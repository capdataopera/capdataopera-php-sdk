<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\AdressePostale;

final class AdressePostaleConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof AdressePostale) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getAdressePostale(),
            $graph,
            [$graph->getRofNamespace() . ':adressePostale']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getCodePostal(),
            $graph,
            [$graph->getRofNamespace() . ':codePostal']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getCommune(),
            $graph,
            [$graph->getRofNamespace() . ':commune']
        );

        // Always start with rof namespace
        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':AdressePostale');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
