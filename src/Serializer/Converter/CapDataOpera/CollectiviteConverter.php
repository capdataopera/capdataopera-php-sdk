<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Collectivite;

final class CollectiviteConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Collectivite) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getNom(),
            $graph,
            [$graph->getRofNamespace() . ':nom']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getNomFormeRejet(),
            $graph,
            [$graph->getRofNamespace() . ':nomFormeRejet']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getStatusJuridique(),
            $graph,
            [$graph->getRofNamespace() . ':statusJuridique']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAdresse(),
            $graph,
            [$graph->getRofNamespace() . ':adresse']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourLieu(),
            $graph,
            [$graph->getRofNamespace() . ':aPourLieu']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getSiret(),
            $graph,
            [$graph->getRofNamespace() . ':siret']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getOpenAgenda(),
            $graph,
            [$graph->getRofNamespace() . ':openAgenda']
        );

        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Collectivite');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
