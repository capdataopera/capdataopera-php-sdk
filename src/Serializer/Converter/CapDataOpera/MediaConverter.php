<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Image;
use CapDataOpera\PhpSdk\Model\Media;
use CapDataOpera\PhpSdk\Model\Sound;
use CapDataOpera\PhpSdk\Model\Text;

final class MediaConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Media) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getMentions(),
            $graph,
            [$graph->getRofNamespace() . ':mentions']
        );

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getName(),
            $graph,
            [$graph->getRofNamespace() . ':titre']
        );

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getContentUrl(),
            $graph,
            ['dc:source']
        );

        // Always start with rof namespace
        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Media');

        /*
         * DublinCore ontology is required for the following types in CapDataOpera ontology:
         */
        if ($object instanceof Image || $object instanceof Sound || $object instanceof Text) {
            $this->convertValueObjectToLiteral(
                $object->getUri(),
                $object->getName(),
                $graph,
                ['dc:title']
            );
            $this->convertValueObjectToLiteral(
                $object->getUri(),
                $object->getDescription(),
                $graph,
                ['dc:description']
            );
            $this->convertValueObjectToLiteral(
                $object->getUri(),
                $object->getMentions(),
                $graph,
                ['dc:rights']
            );
        }
        if ($object instanceof Image) {
            $graph->addResource($object->getUri(), "rdf:type", 'dc:Image');
        }
        if ($object instanceof Sound) {
            $graph->addResource($object->getUri(), "rdf:type", 'dc:Sound');
        }
        if ($object instanceof Text) {
            $graph->addResource($object->getUri(), "rdf:type", 'dc:Text');
        }

        $graph->registerInternalNodeUri($object->getUri());
    }
}
