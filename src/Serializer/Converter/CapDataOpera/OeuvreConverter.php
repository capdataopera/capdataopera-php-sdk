<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Oeuvre;

final class OeuvreConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Oeuvre) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDateDeCreation(),
            $graph,
            [$graph->getRofNamespace() . ':dateDeCreation']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDuree(),
            $graph,
            [$graph->getRofNamespace() . ':duree']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getIntrigue(),
            $graph,
            [$graph->getRofNamespace() . ':intrigue']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getSourceLivret(),
            $graph,
            [$graph->getRofNamespace() . ':sourceLivret']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getTitre(),
            $graph,
            [$graph->getRofNamespace() . ':titre']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getTitreFormeRejet(),
            $graph,
            [$graph->getRofNamespace() . ':titreFormeRejet']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getCategorieOeuvre(),
            $graph,
            [$graph->getRofNamespace() . ':categorieOeuvre']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getGenreOeuvre(),
            $graph,
            [$graph->getRofNamespace() . ':genreOeuvre']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getPaysDeCreation(),
            $graph,
            [$graph->getRofNamespace() . ':paysDeCreation']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getTypeOeuvre(),
            $graph,
            [$graph->getRofNamespace() . ':typeOeuvre']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getPersonnage(),
            $graph,
            [$graph->getRofNamespace() . ':personnage']
        );

        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Oeuvre');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
