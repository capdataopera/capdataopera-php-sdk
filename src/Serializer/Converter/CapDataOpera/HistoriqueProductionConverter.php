<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\HistoriqueProduction;

final class HistoriqueProductionConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof HistoriqueProduction) {
            return;
        }

        // Always start with rof namespace
        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':HistoriqueProduction');
        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Referentiel');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
