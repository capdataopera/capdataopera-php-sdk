<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\HasArkBnf;

final class HasArkBnfConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof HasArkBnf) {
            return;
        }

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getArkBnf(),
            $graph,
            [$graph->getRofNamespace() . ':arkBnf']
        );

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getFrBnf(),
            $graph,
            [
                $graph->getRofNamespace() . ':frBnf',
            ]
        );
    }
}
