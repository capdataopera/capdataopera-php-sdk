<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\CatalogClass;
use CapDataOpera\PhpSdk\Model\OntologyClass;

final class CatalogClassConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(OntologyClass $object, Graph $graph): void
    {
        if (!$object instanceof CatalogClass) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getIdentifiantRof(),
            $graph,
            [$graph->getRofNamespace() . ':identifiantRof']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getSameAs(),
            $graph,
            ['owl:sameAs']
        );

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDateCreationRessource(),
            $graph,
            [
                $graph->getRofNamespace() . ':dateCreationRessource',
            ]
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDateModificationRessource(),
            $graph,
            [
                $graph->getRofNamespace() . ':dateModificationRessource'
            ]
        );

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getCatalogageSourceDate(),
            $graph,
            [$graph->getRofNamespace() . ':catalogageSourceDate']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getCatalogageSourcePays(),
            $graph,
            [$graph->getRofNamespace() . ':catalogageSourcePays']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getCatalogageSourceAgence(),
            $graph,
            [$graph->getRofNamespace() . ':catalogageSourceAgence']
        );
    }
}
