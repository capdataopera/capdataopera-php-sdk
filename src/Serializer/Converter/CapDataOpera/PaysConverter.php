<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Pays;

final class PaysConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Pays) {
            return;
        }

        // Always start with rof namespace
        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Pays');
        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Referentiel');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
