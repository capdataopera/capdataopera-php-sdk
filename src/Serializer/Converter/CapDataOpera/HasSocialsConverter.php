<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\HasSocials;
use CapDataOpera\PhpSdk\Serializer\Converter\AbstractConverter;

final class HasSocialsConverter extends AbstractCapDataOperaConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof HasSocials) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getSiteWeb(),
            $graph,
            [
                $graph->getRofNamespace() . ':pageWeb',
                $graph->getRofNamespace() . ':siteWeb',
            ]
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getFacebook(),
            $graph,
            [
                $graph->getRofNamespace() . ':pageWeb',
                $graph->getRofNamespace() . ':facebook',
            ]
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getTwitter(),
            $graph,
            [
                $graph->getRofNamespace() . ':pageWeb',
                $graph->getRofNamespace() . ':twitter',
            ]
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getMusicStory(),
            $graph,
            [
                $graph->getRofNamespace() . ':pageWeb',
                $graph->getRofNamespace() . ':musicStory',
            ]
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getIsni(),
            $graph,
            [$graph->getRofNamespace() . ':isni']
        );
    }
}
