<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\OntologyClass;
use CapDataOpera\PhpSdk\Serializer\Converter\AbstractConverter;

abstract class AbstractCapDataOperaConverter extends AbstractConverter
{
    protected abstract function convertCapDataObject(OntologyClass $object, Graph $graph): void;

    public function convert(OntologyClass $object, Graph $graph, array $ontologies): void
    {
        if (!\in_array('capdata', $ontologies, true)) {
            return;
        }

        $this->convertCapDataObject($object, $graph);
    }
}
