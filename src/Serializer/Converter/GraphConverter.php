<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;

final class GraphConverter
{
    private ChainConverter $chainConverter;

    public function __construct(ChainConverter $chainConverter)
    {
        $this->chainConverter = $chainConverter;
    }

    /**
     * @param Graph $graph
     * @param string[] $ontologies
     * @return void
     */
    public function convert(Graph $graph, array $ontologies): void
    {
        if ($graph->isConverted()) {
            return;
        }
        foreach ($graph as $node) {
            $this->chainConverter->convert($node, $graph, $ontologies);
        }

        $graph->setConverted();
    }
}
