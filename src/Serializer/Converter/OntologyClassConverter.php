<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\OntologyClass;

interface OntologyClassConverter
{
    /**
     * @param OntologyClass $object
     * @param Graph $graph
     * @param array<string> $ontologies List of ontologies to use for the conversion
     * @return void
     */
    public function convert(OntologyClass $object, Graph $graph, array $ontologies): void;
}
