<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\OntologyClass;
use CapDataOpera\PhpSdk\Model\Participation;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\LiteralConverter;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\ValueObject;

abstract class AbstractConverter implements OntologyClassConverter
{
    protected LiteralConverter $literalConverter;

    public function __construct(LiteralConverter $literalConverter)
    {
        $this->literalConverter = $literalConverter;
    }

    /**
     * @param string $uri
     * @param ValueObject|null $value
     * @param Graph $graph
     * @param array<string> $mappingProperties
     * @return $this
     */
    protected function convertValueObjectToLiteral(
        string $uri,
        ?ValueObject $value,
        Graph $graph,
        array $mappingProperties
    ): self {
        if ($value === null) {
            return $this;
        }
        foreach ($this->literalConverter->convert($value, $graph) as $literal) {
            foreach ($mappingProperties as $mappingProperty) {
                $graph->addLiteral(
                    $uri,
                    $mappingProperty,
                    $literal
                );
            }
        }

        return $this;
    }

    /**
     * @template T of OntologyClass
     * @param string $uri
     * @param RelationObject<T>|null $value
     * @param Graph $graph
     * @param array<string> $mappingProperties
     * @return $this
     */
    protected function convertValueObjectToResource(
        string $uri,
        ?RelationObject $value,
        Graph $graph,
        array $mappingProperties
    ): self {
        if ($value === null) {
            return $this;
        }
        foreach ($this->literalConverter->convert($value, $graph) as $bResource) {
            foreach ($mappingProperties as $mappingProperty) {
                $graph->addResource(
                    $uri,
                    $mappingProperty,
                    $bResource
                );
            }
        }

        return $this;
    }

    /**
     * Resolve directly a Participation' participant object to a resource for schema.org.
     *
     * @template T of Participation
     * @param string $uri
     * @param RelationObject<T>|null $value
     * @param Graph $graph
     * @param array<string> $mappingProperties
     * @return $this
     */
    protected function convertParticipantObjectToResource(
        string $uri,
        ?RelationObject $value,
        Graph $graph,
        array $mappingProperties
    ): self {
        if ($value === null || $value->getValue() === null) {
            return $this;
        }
        /** @var T[]|T $participations */
        $participations = $value->getValue();
        if (\is_array($participations)) {
            foreach ($participations as $participation) {
                $this->convertValueObjectToResource(
                    $uri,
                    $participation->getAPourParticipant(),
                    $graph,
                    $mappingProperties
                );
            }
        } else {
            $this->convertValueObjectToResource(
                $uri,
                $participations->getAPourParticipant(),
                $graph,
                $mappingProperties
            );
        }

        return $this;
    }
}
