<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\AbstractProduction;

final class ProductionConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof AbstractProduction) {
            return;
        }

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourTypePublic(),
            $graph,
            ['schema:audience']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getLieuPublication(),
            $graph,
            ['schema:locationCreated']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getOeuvreRepresentee(),
            $graph,
            ['schema:isBasedOn']
        );

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getTitre(),
            $graph,
            ['schema:name']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDatePublication(),
            $graph,
            ['schema:datePublished']
        );

        $graph->addResource($object->getUri(), "rdf:type", "schema:CreativeWork");
        $graph->registerInternalNodeUri($object->getUri());
    }
}
