<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Collectivite;

final class CollectiviteConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Collectivite) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getNom(),
            $graph,
            ['schema:name']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAdresse(),
            $graph,
            ['schema:address']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourLieu(),
            $graph,
            ['schema:location']
        );

        $graph->addResource($object->getUri(), "rdf:type", "schema:Organization");
        $graph->registerInternalNodeUri($object->getUri());
    }
}
