<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\HasImage;

final class HasImageConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof HasImage) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getImage(),
            $graph,
            ['schema:image']
        );
    }
}
