<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Oeuvre;

final class OeuvreConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Oeuvre) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDateDeCreation(),
            $graph,
            ['schema:dateCreated']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDuree(),
            $graph,
            ['schema:duration']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getIntrigue(),
            $graph,
            ['schema:about']
        );

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getTitre(),
            $graph,
            ['schema:name']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getGenreOeuvre(),
            $graph,
            ['schema:genre']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getPaysDeCreation(),
            $graph,
            ['schema:countryOfOrigin']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getPersonnage(),
            $graph,
            ['schema:character']
        );

        $graph->addResource($object->getUri(), "rdf:type", "schema:CreativeWork");
        $graph->registerInternalNodeUri($object->getUri());
    }
}
