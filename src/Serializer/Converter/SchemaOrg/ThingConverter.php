<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Thing;

final class ThingConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Thing) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getName(),
            $graph,
            ['schema:name']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDescription(),
            $graph,
            ['schema:description']
        );

        $graph->addResource($object->getUri(), "rdf:type", 'schema:Thing');
    }
}
