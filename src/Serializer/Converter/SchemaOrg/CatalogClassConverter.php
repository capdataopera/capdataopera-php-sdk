<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\CatalogClass;
use CapDataOpera\PhpSdk\Model\OntologyClass;

final class CatalogClassConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(OntologyClass $object, Graph $graph): void
    {
        if (!$object instanceof CatalogClass) {
            return;
        }

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getSameAs(),
            $graph,
            ['schema:sameAs']
        );
    }
}
