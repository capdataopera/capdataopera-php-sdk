<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Referentiel;

final class ReferentielConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Referentiel) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getAltLabel(),
            $graph,
            ['schema:alternateName']
        );
    }
}
