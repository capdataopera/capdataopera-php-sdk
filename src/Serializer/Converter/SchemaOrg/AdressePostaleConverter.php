<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\AdressePostale;

final class AdressePostaleConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof AdressePostale) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getAdressePostale(),
            $graph,
            [
                'schema:streetAddress'
            ]
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getCodePostal(),
            $graph,
            ['schema:postalCode']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getCommune(),
            $graph,
            ['schema:addressLocality']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getPays(),
            $graph,
            ['schema:addressCountry']
        );

        // Always start with rof namespace
        $graph->addResource($object->getUri(), "rdf:type", "schema:PostalAddress");
        $graph->registerInternalNodeUri($object->getUri());
    }
}
