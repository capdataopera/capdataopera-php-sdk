<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Personne;

final class PersonneConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Personne) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getNom(),
            $graph,
            ['schema:familyName']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getPrenom(),
            $graph,
            ['schema:givenName']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getBiographie(),
            $graph,
            ['schema:biography']
        );

        $graph->addResource($object->getUri(), "rdf:type", 'schema:Person');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
