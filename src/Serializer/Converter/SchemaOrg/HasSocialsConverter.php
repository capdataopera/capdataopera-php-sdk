<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\HasSocials;

final class HasSocialsConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof HasSocials) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getSiteWeb(),
            $graph,
            [
                'schema:url'
            ]
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getFacebook(),
            $graph,
            [
                'schema:url'
            ]
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getTwitter(),
            $graph,
            [
                'schema:url'
            ]
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getMusicStory(),
            $graph,
            [
                'schema:url'
            ]
        );
    }
}
