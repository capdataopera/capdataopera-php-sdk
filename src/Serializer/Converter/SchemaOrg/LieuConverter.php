<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Lieu;

final class LieuConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Lieu) {
            return;
        }

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAdresse(),
            $graph,
            ['schema:address']
        );

        $graph->addResource($object->getUri(), "rdf:type", "schema:Place");
        $graph->registerInternalNodeUri($object->getUri());
    }
}
