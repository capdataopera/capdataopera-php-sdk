<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Evenement;

final class EvenementConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Evenement) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDateDebut(),
            $graph,
            ['schema:startDate']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDateFin(),
            $graph,
            ['schema:endDate']
        );
        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getDuree(),
            $graph,
            ['schema:duration']
        );

        if (null !== $object->getAnnulation() &&$object->getAnnulation()->serialize()[0] === true) {
            $graph->addResource($object->getUri(), "schema:eventStatus", "schema:EventCancelled");
        }

        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourLieu(),
            $graph,
            ['schema:location']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourTypePublic(),
            $graph,
            ['schema:audience']
        );
        $this->convertValueObjectToResource(
            $object->getUri(),
            $object->getAPourProduction(),
            $graph,
            ['schema:workPerformed']
        );

        $graph->addResource($object->getUri(), "rdf:type", 'schema:Event');
        $graph->registerInternalNodeUri($object->getUri());
    }
}
