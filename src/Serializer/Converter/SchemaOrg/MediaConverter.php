<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Image;
use CapDataOpera\PhpSdk\Model\Media;
use CapDataOpera\PhpSdk\Model\Sound;
use CapDataOpera\PhpSdk\Model\Text;

final class MediaConverter extends AbstractSchemaOrgConverter
{
    protected function convertCapDataObject(object $object, Graph $graph): void
    {
        if (!$object instanceof Media) {
            return;
        }

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getMentions(),
            $graph,
            ['schema:creditText']
        );

        $this->convertValueObjectToLiteral(
            $object->getUri(),
            $object->getContentUrl(),
            $graph,
            ['schema:contentUrl']
        );

        // Always start with rof namespace
        $graph->addResource($object->getUri(), "rdf:type", $graph->getRofNamespace() . ':Media');
        $graph->addResource($object->getUri(), "rdf:type", 'schema:MediaObject');

        if ($object instanceof Image) {
            $graph->addResource($object->getUri(), "rdf:type", 'schema:ImageObject');
        }
        if ($object instanceof Sound) {
            $graph->addResource($object->getUri(), "rdf:type", 'schema:SoundObject');
        }
        if ($object instanceof Text) {
            $graph->addResource($object->getUri(), "rdf:type", 'schema:TextObject');
        }

        $graph->registerInternalNodeUri($object->getUri());
    }
}
