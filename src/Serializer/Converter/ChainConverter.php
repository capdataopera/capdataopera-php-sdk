<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\OntologyClass;

final class ChainConverter implements OntologyClassConverter
{
    /**
     * @var OntologyClassConverter[]
     */
    private array $converters = [];

    /**
     * @param OntologyClassConverter[] $converters
     */
    public function __construct(array $converters)
    {
        if (empty($converters)) {
            throw new \InvalidArgumentException('At least one serializer must be provided');
        }
        if (count($converters) !== count(array_filter(
            $converters,
            fn($converter) => $converter instanceof OntologyClassConverter
        ))) {
            throw new \InvalidArgumentException(
                'All converters must implement ' . OntologyClassConverter::class
            );
        }
        $this->converters = $converters;
    }

    /**
     * @param OntologyClass $object
     * @param Graph $graph
     * @param string[] $ontologies
     * @return void
     */
    public function convert(OntologyClass $object, Graph $graph, array $ontologies): void
    {
        /*
         * Converter for resource are accumulated in the chain
         */
        foreach ($this->converters as $converter) {
            $converter->convert($object, $graph, $ontologies);
        }
    }
}
