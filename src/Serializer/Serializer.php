<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdk\Serializer;

use CapDataOpera\PhpSdk\Exception\InvalidGraphException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Serializer\Converter\CapDataOpera as CapDataConverter;
use CapDataOpera\PhpSdk\Serializer\Converter\SchemaOrg as SchemaConverter;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter as LiteralConverter;

final class Serializer implements SerializerInterface
{
    private Converter\GraphConverter $graphConverter;

    /**
     * @param array<Converter\OntologyClassConverter>|null $objectConverterList
     * @param array<LiteralConverter\LiteralConverter>|null $literalConverterList
     */
    public function __construct(?array $objectConverterList = null, ?array $literalConverterList = null)
    {
        $chainLiteralConverter = new LiteralConverter\ChainLiteralConverter(
            $literalConverterList ?? [
                new LiteralConverter\DateTimeObjectLiteralConverter(),
                new LiteralConverter\DateIntervalObjectLiteralConverter(),
                new LiteralConverter\BooleanObjectLiteralConverter(),
                new LiteralConverter\NumericObjectLiteralConverter(),
                new LiteralConverter\UriObjectLiteralConverter(),
                new LiteralConverter\RelationObjectLiteralConverter(),
                new LiteralConverter\StringObjectLiteralConverter(),
            ]
        );
        $this->graphConverter = new Converter\GraphConverter(new Converter\ChainConverter(
                $objectConverterList ?? [
                // Order is important between converters that supports abstract converters
                // and converters that supports concrete converters
                // - Abstract converters
                new CapDataConverter\ThingConverter($chainLiteralConverter),
                new SchemaConverter\ThingConverter($chainLiteralConverter),

                new CapDataConverter\HasArkBnfConverter($chainLiteralConverter),

                new CapDataConverter\ReferentielConverter($chainLiteralConverter),
                new SchemaConverter\ReferentielConverter($chainLiteralConverter),

                new CapDataConverter\HasSocialsConverter($chainLiteralConverter),
                new SchemaConverter\HasSocialsConverter($chainLiteralConverter),

                new SchemaConverter\HasImageConverter($chainLiteralConverter),
                new CapDataConverter\HasMediaConverter($chainLiteralConverter),
                new CapDataConverter\HasParticipationConverter($chainLiteralConverter),
                new CapDataConverter\HasLiveParticipationConverter($chainLiteralConverter),

                new CapDataConverter\CatalogClassConverter($chainLiteralConverter),
                new SchemaConverter\CatalogClassConverter($chainLiteralConverter),

                // - Concrete converters
                new CapDataConverter\PaysConverter($chainLiteralConverter),
                new SchemaConverter\PaysConverter($chainLiteralConverter),

                new CapDataConverter\StatusJuridiqueConverter($chainLiteralConverter),
                new CapDataConverter\FonctionConverter($chainLiteralConverter),

                new CapDataConverter\SaisonConverter($chainLiteralConverter),
                new CapDataConverter\RoleConverter($chainLiteralConverter),
                new CapDataConverter\TypeOeuvreConverter($chainLiteralConverter),
                new CapDataConverter\TypeProductionConverter($chainLiteralConverter),
                new CapDataConverter\TypeEvenementConverter($chainLiteralConverter),
                new CapDataConverter\GenreOeuvreConverter($chainLiteralConverter),
                new CapDataConverter\CategorieOeuvreConverter($chainLiteralConverter),
                new CapDataConverter\HistoriqueProductionConverter($chainLiteralConverter),

                new CapDataConverter\AdressePostaleConverter($chainLiteralConverter),
                new SchemaConverter\AdressePostaleConverter($chainLiteralConverter),

                new CapDataConverter\LieuConverter($chainLiteralConverter),
                new SchemaConverter\LieuConverter($chainLiteralConverter),

                new CapDataConverter\PersonneConverter($chainLiteralConverter),
                new SchemaConverter\PersonneConverter($chainLiteralConverter),

                new CapDataConverter\CollectiviteConverter($chainLiteralConverter),
                new SchemaConverter\CollectiviteConverter($chainLiteralConverter),

                new CapDataConverter\ParticipationConverter($chainLiteralConverter),

                new CapDataConverter\MediaConverter($chainLiteralConverter),
                new SchemaConverter\MediaConverter($chainLiteralConverter),

                new CapDataConverter\OeuvreConverter($chainLiteralConverter),
                new SchemaConverter\OeuvreConverter($chainLiteralConverter),

                new CapDataConverter\ProductionConverter($chainLiteralConverter),
                new SchemaConverter\ProductionConverter($chainLiteralConverter),

                new CapDataConverter\EvenementConverter($chainLiteralConverter),
                new SchemaConverter\EvenementConverter($chainLiteralConverter),
            ]
        ));
    }

    /**
     * @return Converter\GraphConverter
     * @internal
     */
    public function getGraphConverter(): Converter\GraphConverter
    {
        return $this->graphConverter;
    }

    /**
     * @param Graph $graph
     * @param string $format
     * @param string[] $ontologies
     * @return mixed
     */
    public function serialize(Graph $graph, string $format, array $ontologies = ['capdata'])
    {
        $this->graphConverter->convert($graph, $ontologies);
        $violations = $graph->validate();
        if (count($violations) > 0) {
            throw new InvalidGraphException('Invalid graph: ' . implode(', ', $violations));
        }

        if ($format === 'jsonld') {
            $options = [
                'compact' => true
            ];
        } else {
            $options = [];
        }

        return $graph->getRdfGraph()->serialise($format, $options);
    }
}
