<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\AdressePostale;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Lieu;

trait HasLieuTestTrait
{
    public function getTestLieu(?Graph $graph = null, string $id = "1"): Lieu
    {
        $address = new AdressePostale('https://mon-opera.fr/adresse/' . $id);
        $address
            ->setAdressePostale('1 place de la Comédie')
            ->setCodePostal('69001')
            ->setCommune('Lyon')
            ->setPays(new ExternalThing('https://mon-opera.fr/pays/1'))
        ;

        $lieu = new Lieu('https://mon-opera.fr/lieu/' . $id);
        $lieu
            ->setName('Opéra de Lyon')
            ->setOpenAgenda('https://openagenda.com/lieux/' . $id)
            ->setAdresse($address)
            ->setImage('https://assets.mon-opera.fr/media/grande-salle.jpg')
        ;
        if ($graph !== null) {
            $graph->add($address);
            $graph->add($lieu);
        }

        return $lieu;
    }
}
