<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\ArkBnf;
use CapDataOpera\PhpSdk\Model\Auteur;
use CapDataOpera\PhpSdk\Model\Interpretation;
use CapDataOpera\PhpSdk\Model\Oeuvre;
use CapDataOpera\PhpSdk\Model\Pays;
use CapDataOpera\PhpSdk\Model\Personne;
use CapDataOpera\PhpSdk\Model\Role;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;
use CapDataOpera\PhpSdkTests\Serializer\HasPersonneTestTrait;

class OeuvreConverterTest extends AbstractSerializerTest
{
    use HasPersonneTestTrait;

    protected function getOeuvre(Graph $graph): Oeuvre
    {
        $pays = (new Pays('https://mon-opera.fr/pays/2'))
            ->setLabel('NO')
        ;
        $graph->add($pays);

        $Ibsen = new Personne('https://mon-opera.fr/personne/99');
        $Ibsen
            ->setNom('Ibsen')
            ->setPrenom('Henrik')
            ->setDates('1828-1906')
        ;
        $graph->add($Ibsen);

        $auteur = new Auteur('https://mon-opera.fr/auteur/2');
        $auteur->setAPourParticipant($Ibsen);

        $oeuvre = new Oeuvre('https://mon-opera.fr/oeuvre/1');
        $oeuvre
            ->setTitre('Peer Gynt')
            ->setArkBnf(new ArkBnf('http://ark.bnf.fr/ark:/10946/00229735'))
            ->setImage('https://upload.wikimedia.org/wikipedia/commons/5/56/Henrik_Klausen_as_Peer_Gynt.jpg')
            ->setDuree(new \DateInterval('PT2H30M'))
            ->setDateDeCreation(new \DateTimeImmutable('2018-01-30T00:00:00+00:00'))
            ->setDescription('Peer Gynt est un drame poétique devenu pièce de théâtre de l\'auteur norvégien Henrik Ibsen sur une musique du compositeur Edvard Grieg. Elle est jouée pour la première fois au Christiania Theatre (en) d\'Oslo le 24 février 1876 et reçoit un accueil triomphal auquel la scénographie vivante et surtout la musique apportent leur concours. Elle s\'inscrit dans une série de trois pièces épiques avec Brand et Empereur et Galiléen')
            ->setIntrigue('Le personnage principal, Peer Gynt, est un jeune fanfaron d\'une vingtaine d\'années qui tente de fuir la réalité pour la pure vie idéale et accessoirement par le mensonge. Peer a la chance d\'obtenir la promesse de la main de Solveig, jeune fille vertueuse et fidèle, mais, par manque de persévérance, il enlève en pleine fête nuptiale une jeune épouse séduisante : Ingrid. Ayant été violée et pour finir abandonnée par Peer, Ingrid déplore son triste sort.')
            ->setSourceLivret('https://fr.wikipedia.org/wiki/Peer_Gynt')
            ->setTitreFormeRejet('Peer Gynt')
            ->setPaysDeCreation($pays)
            ->setAPourAuteur($auteur)
            ->setCatalogageSourceDate(new \DateTimeImmutable('2024-01-30T00:00:00+00:00'))
        ;
        $graph->add($auteur);
        $graph->add($oeuvre);

        return $oeuvre;
    }

    public function testConvert(): void
    {
        $graph = new Graph();
        $this->getOeuvre($graph);

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/oeuvreConverterTest.ttl', $actual);
    }
}
