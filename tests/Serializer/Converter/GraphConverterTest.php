<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Personne;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;
use CapDataOpera\PhpSdkTests\Serializer\HasPersonneTestTrait;

class GraphConverterTest extends AbstractSerializerTest
{
    use HasPersonneTestTrait;

    public function testEmpty(): void
    {
        $graph = new Graph();
        $this->assertFalse($graph->isConverted());

        $this->graphConverter->convert($graph, ['capdata', 'schema']);
        $this->assertTrue($graph->isConverted());

        // Always validate after conversion
        $violations = $graph->validate();
        $this->assertCount(0, $violations);

        $actual = $graph->getRdfGraph()->serialise('rdfxml');
        $this->assertIsString($actual);
        $expected = <<<RDF
<?xml version="1.0" encoding="utf-8" ?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">

</rdf:RDF>
RDF;

        $this->assertEquals(trim($expected), trim($actual));
    }

    public function testConvert(): void
    {
        $graph = new Graph();
        $this->getCatalogageSourceAgence($graph);
        $this->graphConverter->convert($graph, ['capdata', 'schema']);
        $violations = $graph->validate();
        $this->assertCount(0, $violations);

        $actual = $graph->getRdfGraph()->serialise('turtle');
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/graphConverterTestTestConvert.ttl', $actual);
    }

    public function testInvalidConvert(): void
    {
        $graph = new Graph();
        $ownOrg = $this->getCatalogageSourceAgence(null);

        $person = new Personne();
        $person
            ->setNom('Maupate')
            ->setPrenom('Ambroise')
            ->setCatalogageSourceAgence($ownOrg)
            ->setUri('https://mon-opera.fr/person/1');
        $graph->add($person);


        $this->graphConverter->convert($graph, ['capdata', 'schema']);

        $violations = $graph->validate();
        $this->assertCount(1, $violations);
        $this->assertEquals('Missing internal node uri "https://mon-opera.fr/organization/1", did you add it to your graph?', $violations[0]);
    }

    public function testComplexConvertTurtle(): void
    {
        $graph = new Graph();

        $ownOrg = $this->getCatalogageSourceAgence($graph);

        $person = $this->getTestPersonne();
        $person->setCatalogageSourceAgence($ownOrg);
        $graph->add($person);

        $this->graphConverter->convert($graph, ['capdata', 'schema']);

        $violations = $graph->validate();
        $this->assertCount(0, $violations);

        $actual = $graph->getRdfGraph()->serialise('turtle');
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/graphConverterTestTestComplexConvert.ttl', $actual);
    }

    public function testComplexConvertCapDataTurtle(): void
    {
        $graph = new Graph();

        $ownOrg = $this->getCatalogageSourceAgence($graph);

        $person = $this->getTestPersonne();
        $person->setCatalogageSourceAgence($ownOrg);
        $graph->add($person);

        $this->graphConverter->convert($graph, ['capdata']);

        $violations = $graph->validate();
        $this->assertCount(0, $violations);

        $actual = $graph->getRdfGraph()->serialise('turtle');
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/capdata/graphConverterTestTestComplexConvert.ttl', $actual);
    }

    public function testComplexConvertSchemaTurtle(): void
    {
        $graph = new Graph();

        $ownOrg = $this->getCatalogageSourceAgence($graph);

        $person = $this->getTestPersonne();
        $person->setCatalogageSourceAgence($ownOrg);
        $graph->add($person);

        $this->graphConverter->convert($graph, ['schema']);

        $violations = $graph->validate();
        $this->assertCount(0, $violations);

        $actual = $graph->getRdfGraph()->serialise('turtle');
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/schema/graphConverterTestTestComplexConvert.ttl', $actual);
    }

    public function testComplexConvertSchemaJsonLd(): void
    {
        $graph = new Graph();

        $ownOrg = $this->getCatalogageSourceAgence($graph);

        $person = $this->getTestPersonne();
        $person->setCatalogageSourceAgence($ownOrg);
        $graph->add($person);

        $this->graphConverter->convert($graph, ['schema']);

        $violations = $graph->validate();
        $this->assertCount(0, $violations);

        $actual = $graph->getRdfGraph()->serialise('jsonld', [
            'compact' => true
        ]);
        $this->assertIsString($actual);
        $this->assertJsonStringEqualsJsonFile(
            dirname(__FILE__) . '/../../fixture/schema/graphConverterTestTestComplexConvert.json',
            $actual
        );
    }
}
