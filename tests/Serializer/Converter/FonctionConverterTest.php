<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Fonction;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;

class FonctionConverterTest extends AbstractSerializerTest
{
    public function testConvert(): void
    {
        $graph = new Graph();

        $fonction = new Fonction('http://capdataculture.fr/graph/FONCTION/721');
        $fonction
            ->setLabel('Artiste lyrique')
            ->setAltLabel('chanteur')
            ->setInScheme(new ExternalThing('http://capdataculture.fr/graph/FONCTION/scheme'))
        ;

        $graph->add($fonction);


        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/fonctionConverterTest.ttl', $actual);
    }
}
