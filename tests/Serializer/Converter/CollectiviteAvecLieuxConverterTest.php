<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\AdressePostale;
use CapDataOpera\PhpSdk\Model\Collectivite;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Isni;
use CapDataOpera\PhpSdk\Model\Lieu;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;

class CollectiviteAvecLieuxConverterTest extends AbstractSerializerTest
{
    public function testConvert(): void
    {
        $graph = new Graph();

        $address = new AdressePostale('https://mon-opera.fr/adresse/1');
        $address
            ->setAdressePostale('1 place de la Comédie')
            ->setCodePostal('69001')
            ->setCommune('Lyon')
            ->setPays(new ExternalThing('https://mon-opera.fr/pays/1'))
        ;

        $lieu = new Lieu('https://mon-opera.fr/lieu/1');
        $lieu
            ->setName('Opéra de Lyon')
            ->setOpenAgenda('https://openagenda.com/lieux/1')
            ->setAdresse($address)
            ->setImage('https://assets.mon-opera.fr/media/grande-salle.jpg')
        ;

        $lieu2 = new Lieu('https://mon-opera.fr/lieu/2');
        $lieu2
            ->setName('Opéra Underground')
            ->setOpenAgenda('https://openagenda.com/lieux/2')
            ->setAdresse($address)
            ->setImage('https://assets.mon-opera.fr/media/underground.jpg')
        ;

        $collectivite = new Collectivite('https://mon-opera.fr/organization/1');
        $collectivite->setNom('Mon opéra de test')
            ->setFacebook('https://facebook.com/mon-opera')
            ->setSiteWeb('https://mon-opera.fr')
            ->setCatalogageSourceAgence($collectivite)
            ->setSameAs(new ExternalThing('https://www.tous-a-lopera.fr/TOUS-A-LOPERA/doc/SYRACUSE-AUT/53947'))
            ->setAdresse($address)
            ->setAPourLieu([
                $lieu,
                $lieu2
            ])
            ->setCatalogageSourceDate(new \DateTimeImmutable('2024-01-30T00:00:00+00:00'))
            ->setCatalogageSourcePays($this->getCatalogageSourcePays($graph))
            ->setIsni(new Isni('https://isni.org/isni/0000000122982840'));

        $graph->add($address);
        $graph->add($lieu);
        $graph->add($lieu2);
        $graph->add($collectivite);

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/collectiviteAvecLieuxTest.ttl', $actual);
    }
}
