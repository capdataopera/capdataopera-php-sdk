<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\ArkBnf;
use CapDataOpera\PhpSdk\Model\Collectivite;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Isni;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;

class CollectiviteConverterTest extends AbstractSerializerTest
{
    use HasCsvFileTrait;
    public function testConvert(): void
    {
        $data = $this->parseCsvToAssoc(dirname(__FILE__) . '/../../fixture/organizations.csv');
        foreach ($data as $row) {
            $graph = new Graph();
            if (!isset($row['identifiantRof'])) {
                echo \json_encode($row, JSON_PRETTY_PRINT);
                $this->fail('Missing identifiantRof');
            }
            $rof = $row['identifiantRof'];
            $collectivite = new Collectivite("https://mon-opera.fr/organization/{$rof}");
            $collectivite
                ->setNom($row['nom'])
                ->setDescription($row['description'])
                ->setStatusJuridique(
                    $row['statutJuridique'] ?
                        new ExternalThing('http://capdataculture.fr/graph/STATUSJURIDIQUE/' . $row['statutJuridique']) :
                        null
                )
                ->setIsni($row['isni'] ? new Isni($row['isni']) : null)
                ->setSiret($row['siret'])
                ->setArkBnf($row['arkBnf'] ? new ArkBnf($row['arkBnf']) : null)
                ->setFrBnf($row['frBnf'] ?? null)
                ->setSiteWeb($row['siteWeb'])
                ->setIdentifiantRof($row['identifiantRof'])
                ->setNomFormeRejet($row['nomFormeRejet'])
                ->setCatalogageSourceDate(new \DateTimeImmutable(
                    $row['catalogageSourceDate'],
                    new \DateTimeZone('Europe/Paris')
                ))
                ->setCatalogageSourceAgence(new ExternalThing(
                    'http://capdataculture.fr/graph/ORGANISME/' . $row['catalogageSourceAgence']
                ))
                ->setCatalogageSourcePays(new ExternalThing(
                    'http://capdataculture.fr/graph/PAYS/' . $row['catalogageSourcePays']
                ))
            ;
            $graph->add($collectivite);

            $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
            $this->assertIsString($actual);
            $this->assertStringEqualsFile(
                dirname(__FILE__) . "/../../fixture/organizations_{$rof}.ttl",
                $actual,
                "Not equals with organizations_{$rof}.ttl"
            );
        }
    }
}
