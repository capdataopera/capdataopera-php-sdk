<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\AdressePostale;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Lieu;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;

class LieuConverterTest extends AbstractSerializerTest
{
    public function testConvert(): void
    {
        $graph = new Graph();
        $address = new AdressePostale('https://mon-opera.fr/adresse/1');
        $address
            ->setAdressePostale('1 place de la Comédie')
            ->setCodePostal('69001')
            ->setCommune('Lyon')
            ->setPays(new ExternalThing('https://mon-opera.fr/pays/1'))
        ;

        $lieu = new Lieu('https://mon-opera.fr/lieu/1');
        $lieu
            ->setName('Opéra - Grande salle')
            ->setDescription('La <strong>grande salle</strong> de l\'Opéra de Lyon')
            ->setOpenAgenda('https://openagenda.com/lieux/1')
            ->setAdresse($address)
            ->setImage('https://assets.mon-opera.fr/media/grande-salle.jpg')
        ;
        $graph->add($address);
        $graph->add($lieu);

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/lieuConverterTest.ttl', $actual);
    }
}
