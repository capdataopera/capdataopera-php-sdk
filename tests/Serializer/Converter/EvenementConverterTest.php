<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Evenement;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\ValueObject\DateIntervalObject;
use CapDataOpera\PhpSdkTests\Serializer\HasLieuTestTrait;
use CapDataOpera\PhpSdkTests\Serializer\HasPersonneTestTrait;
use CapDataOpera\PhpSdkTests\Serializer\HasSourceAgenceTestTrait;

class EvenementConverterTest extends ProductionConverterTest
{
    use HasPersonneTestTrait;
    use HasSourceAgenceTestTrait;
    use HasLieuTestTrait;

    protected function getBaseEvenement(Graph $graph): Evenement
    {
        $lieu = $this->getTestLieu($graph);
        $production = $this->getProduction($graph);
        $evenement = new Evenement('https://mon-opera.fr/evenement/1');
        $evenement
            ->setTitre('Peer Gynt - édition 2023 - représentation 1')
            ->setAPourProduction($production)
            ->setAPourLieu($lieu)
            ->setDateDebut(new \DateTimeImmutable('2023-09-11T20:00:00+01:00'))
            ->setDateFin(new \DateTimeImmutable('2023-09-11T22:00:00+01:00'))
        ;
        $graph->add($evenement);
        return $evenement;
    }

    protected function getBaseEvenementWithExternalThingArray(Graph $graph): Evenement
    {
        $lieu = $this->getTestLieu($graph);
        $production = $this->getProduction($graph);
        $evenement = new Evenement('https://mon-opera.fr/evenement/1');
        $evenement
            ->setTitre('Peer Gynt - édition 2023 - représentation 1')
            ->setAPourProduction($production)
            ->setAPourLieu([
                new ExternalThing($lieu->getUri())
            ])
            ->setDateDebut(new \DateTimeImmutable('2023-09-11T20:00:00+01:00'))
            ->setDateFin(new \DateTimeImmutable('2023-09-11T22:00:00+01:00'))
        ;
        $graph->add($evenement);
        return $evenement;
    }

    public function testConvert(): void
    {
        $graph = new Graph();
        $evenement = $this->getBaseEvenement($graph);
        $evenement
            ->setAnnulation(false)
            ->setPassCulture(true)
            ->setPassCultureNombrePlace(75)
            ->setPassCulturePrix('10.90')
        ;

        $this->assertEquals(new DateIntervalObject('PT2H'), $evenement->getDuree());
        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/evenementConverterTest.ttl', $actual);
    }

    public function testConvertWithExternalLieu(): void
    {
        $graph = new Graph();
        $evenement = $this->getBaseEvenementWithExternalThingArray($graph);
        $evenement
            ->setAnnulation(false)
            ->setPassCulture(true)
            ->setPassCultureNombrePlace(75)
            ->setPassCulturePrix('10.90')
        ;

        $this->assertEquals(new DateIntervalObject('PT2H'), $evenement->getDuree());
        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/evenementConverterTest.ttl', $actual);
    }

    public function testAnnulationConvert(): void
    {
        $graph = new Graph();

        $evenement = $this->getBaseEvenement($graph);
        $evenement
            ->setAnnulation(true)
            ->setPassCulture(false)
        ;
        $this->assertEquals(new DateIntervalObject('PT2H'), $evenement->getDuree());

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/evenementAnnuleConverterTest.ttl', $actual);
    }
}
