<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Interpretation;
use CapDataOpera\PhpSdk\Model\MaitriseOeuvre;
use CapDataOpera\PhpSdk\Model\Partenariat;
use CapDataOpera\PhpSdk\Model\Production;
use CapDataOpera\PhpSdk\Model\ProductionPrimaire;
use CapDataOpera\PhpSdk\Model\Programmation;
use CapDataOpera\PhpSdk\Model\Role;
use CapDataOpera\PhpSdk\Model\Saison;
use CapDataOpera\PhpSdkTests\Serializer\HasLieuTestTrait;
use CapDataOpera\PhpSdkTests\Serializer\HasPersonneTestTrait;
use CapDataOpera\PhpSdkTests\Serializer\HasSourceAgenceTestTrait;

class ProductionConverterTest extends OeuvreConverterTest
{
    use HasPersonneTestTrait;
    use HasSourceAgenceTestTrait;
    use HasLieuTestTrait;

    protected function getSaison(Graph $graph): Saison
    {
        $saison = new Saison('https://mon-opera.fr/saison/1');
        $saison
            ->setLabel('2023-2024')
        ;
        $graph->add($saison);
        return $saison;
    }

    protected function getProduction(Graph $graph): Production
    {
        $oeuvre = $this->getOeuvre($graph);
        $saison = $this->getSaison($graph);

        $person = $this->getTestPersonne();

        $role = new Role('https://mon-opera.fr/role/1');
        $role->setLabel('Premier violon');
        $graph->add($role);

        $role2 = new Role('https://mon-opera.fr/role/2');
        $role2->setLabel('Chef d\'orchestre');
        $graph->add($role2);

        $person = $this->getTestPersonne();
        $graph->add($person);

        $interpretation = new Interpretation('https://mon-opera.fr/interpretation/1');
        $interpretation
            ->setAPourRole($role)
            ->setAPourParticipant($person)
        ;
        $graph->add($interpretation);

        $interpretation2 = new Interpretation('https://mon-opera.fr/interpretation/2');
        $interpretation2
            ->setAPourRole($role2)
            ->setAPourParticipant($person)
        ;
        $graph->add($interpretation2);

        $maitriseOeuvre = (new MaitriseOeuvre('https://mon-opera.fr/maitrise_oeuvre/1'))
            ->setAPourParticipant($this->getCatalogageSourceAgence($graph))
        ;
        $programmation = (new Programmation('https://mon-opera.fr/programmation/1'))
            ->setAPourParticipant($this->getCatalogageSourceAgence($graph))
        ;
        $partenariat = (new Partenariat('https://mon-opera.fr/partenariat/1'))
            ->setAPourParticipant($this->getCatalogageSourceAgence($graph))
        ;

        $production = new Production('https://mon-opera.fr/production/1');
        $production
            ->setTitre('Peer Gynt - édition 2023')
            ->setOeuvreRepresentee($oeuvre)
            ->setAPourSaison($saison)
            ->setCatalogageSourceDate(new \DateTimeImmutable('2023-03-01T00:00:00+01:00'))
            ->setDatePremiere(new \DateTimeImmutable('2023-09-11T20:00:00+01:00'))
            ->setDatePublication(new \DateTimeImmutable('2023-06-11T20:00:00+01:00'))
            ->setJeunePublic(false)
            ->setLieuPublication($this->getTestLieu($graph))
            ->setAPourProgrammation($programmation)
            ->setAPourInterpretation([$interpretation, $interpretation2])
            ->setAPourMaitriseOeuvre($maitriseOeuvre)
            ->setAPourPartenariat($partenariat)
            ->setProgrammateur($this->getCatalogageSourceAgence($graph))
            ->setCatalogageSourceAgence($this->getCatalogageSourceAgence($graph))
            ->setCatalogageSourcePays($this->getCatalogageSourcePays($graph))
        ;

        $graph->add($person);
        $graph->add($programmation);
        $graph->add($partenariat);
        $graph->add($maitriseOeuvre);
        $graph->add($production);
        return $production;
    }

    public function testConvert(): void
    {
        $graph = new Graph();

        $productionPrimaire = new ProductionPrimaire('https://mon-opera.fr/production/10');
        $productionPrimaire->setTitre('Peer Gynt - production primaire');
        $graph->add($productionPrimaire);

        $production = $this->getProduction($graph);
        $production->setProductionPrimaire($productionPrimaire);
        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/productionConverterTest.ttl', $actual);
    }

    public function testConvertWithCapdataOnly(): void
    {
        $graph = new Graph();

        $productionPrimaire = new ProductionPrimaire('https://mon-opera.fr/production/10');
        $productionPrimaire->setTitre('Peer Gynt - production primaire');
        $graph->add($productionPrimaire);

        $production = $this->getProduction($graph);
        $production->setProductionPrimaire($productionPrimaire);
        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/capdata/productionConverterTest.ttl', $actual);
    }
}
