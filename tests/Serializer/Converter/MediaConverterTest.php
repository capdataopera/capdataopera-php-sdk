<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Image;
use CapDataOpera\PhpSdk\Model\Media;
use CapDataOpera\PhpSdk\Model\Sound;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;

class MediaConverterTest extends AbstractSerializerTest
{
    public function testConvert(): void
    {
        $graph = new Graph();
        $media = new Media('https://mon-opera.fr/media/1');
        $media
            ->setName('Mon media')
            ->setDescription('Image de test')
            ->setMentions('Copyright 2024')
            ->setContentUrl('https://assets.mon-opera.fr/media/1.jpg')
        ;
        $graph->add($media);

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/mediaConverterTest.ttl', $actual);
    }
    public function testImageConvert(): void
    {
        $graph = new Graph();
        $media = new Image('https://mon-opera.fr/media/1');
        $media
            ->setName('Mon media')
            ->setDescription('Image de test')
            ->setMentions('Copyright 2024')
            ->setContentUrl('https://assets.mon-opera.fr/media/1.jpg')
        ;
        $graph->add($media);

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/imageConverterTest.ttl', $actual);
    }
    public function testSoundConvert(): void
    {
        $graph = new Graph();
        $media = new Sound('https://mon-opera.fr/media/2');
        $media
            ->setName('Mon media sonore')
            ->setDescription('Son de test')
            ->setMentions('Copyright 2024')
            ->setImage('https://assets.mon-opera.fr/media/2.jpg')
            ->setContentUrl('https://stream.mon-opera.fr/media/2.mp3')
        ;
        $graph->add($media);

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/soundConverterTest.ttl', $actual);
    }
}
