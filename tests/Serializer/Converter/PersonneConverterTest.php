<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\ArkBnf;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Fonction;
use CapDataOpera\PhpSdk\Model\Isni;
use CapDataOpera\PhpSdk\Model\Personne;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;

class PersonneConverterTest extends AbstractSerializerTest
{
    public function testPersonneName(): void
    {
        $person = new Personne('https://mon-opera.fr/person/1');
        $person->setNom('Maupate');
        $person->setPrenom('Ambroise');

        $this->assertEquals(['Maupate'], $person->getNom()->serialize());
        $this->assertEquals(['Ambroise'], $person->getPrenom()->serialize());
        $this->assertEquals(['Ambroise Maupate'], $person->getName()->getValue());
    }
    public function testPersonneMultipleName(): void
    {
        $person = new Personne('https://mon-opera.fr/person/1');
        $person->setNom('Maupate');
        $person->setPrenom(['Ambroise', 'Ambroise2', 'Ambroise3']);

        $this->assertEquals(['Maupate'], $person->getNom()->serialize());
        $this->assertEquals(['Ambroise', 'Ambroise2', 'Ambroise3'], $person->getPrenom()->serialize());
        $this->assertEquals(['Ambroise Maupate', 'Ambroise2 Maupate', 'Ambroise3 Maupate'], $person->getName()->getValue());
    }

    public function testConvert(): void
    {
        $graph = new Graph();

        $fonction = new Fonction('https://mon-opera.fr/fonction/1');
        $fonction
            ->setLabel('Directeur technique');
        $graph->add($fonction);

        $profession = new Fonction('https://mon-opera.fr/fonction/2');
        $profession
            ->setLabel('Développeur');
        $graph->add($profession);

        $personne = new Personne('https://mon-opera.fr/person/1');
        $personne
            ->setPrenom('Ambroise')
            ->setNom('Maupate')
            ->setAPourFonction($fonction)
            ->setAPourProfession($profession)
            ->setTwitter('https://twitter.com/ambroisemaupate')
            ->setFacebook('https://facebook.com/ambroisemaupate')
            ->setSiteWeb('https://rezo-zero.com')
            ->setImage('https://avatars.githubusercontent.com/u/380026?v=4')
            ->setDateCreationRessource(new \DateTimeImmutable('2024-01-30T00:00:00+00:00'))
            ->setDateModificationRessource(new \DateTimeImmutable('2024-01-30T00:00:00+00:00'))
            ->setCatalogageSourceDate(new \DateTimeImmutable('2024-01-30T00:00:00+00:00'))
        ;
        $graph->add($personne);

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/personneConverterTest.ttl', $actual);
    }
    public function testConvertWithIsni(): void
    {
        $graph = new Graph();

        $personne = new Personne('https://mon-opera.fr/person/1');
        $personne
            ->setPrenom('Ambroise')
            ->setNom('Maupate')
            ->setAPourFonction(new ExternalThing('https://mon-opera.fr/fonction/1'))
            ->setAPourProfession(new ExternalThing('https://mon-opera.fr/fonction/2'))
            ->setTwitter('https://twitter.com/ambroisemaupate')
            ->setFacebook('https://facebook.com/ambroisemaupate')
            ->setSiteWeb('https://rezo-zero.com')
            ->setImage('https://avatars.githubusercontent.com/u/380026?v=4')
            ->setCatalogageSourceDate(new \DateTimeImmutable('2024-01-30T00:00:00+00:00'))
        ;
        $graph->add($personne);

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/personneConverterTestWithIsni.ttl', $actual);
    }

    public function testConvertVerdi(): void
    {
        $graph = new Graph();

        $personne = new Personne('https://mon-opera.fr/person/1');
        $personne
            ->setPrenom('Giuseppe')
            ->setNomFormeRejet('Verdi')
            ->setNom('Verdi')
            ->setAPourProfession(new ExternalThing('http://capdataculture.fr/graph/FONCTION/230'))
            ->setArkBnf(new ArkBnf('http://ark.bnf.fr/ark:/12148/cb139008052'))
            ->setBiographie("Compositeur italien, figure majeure du romantisme musical, Verdi est célèbre pour ses opéras qui ont marqué l'histoire du genre. Né dans le petit village italien de Le Roncole (aujourd'hui Roncole Verdi) en 1813, il montre dès son plus jeune âge un talent musical exceptionnel. Après des débuts difficiles, il rencontre le succès avec Nabucco en 1842, qui lance sa carrière. Ses œuvres suivantes, comme Rigoletto, La Traviata et Aida, deviennent des chefs-d'œuvre populaires. Verdi s'engage également dans la vie politique italienne, devenant sénateur après l'unification du pays. Ses dernières compositions, Otello (1887) et Falstaff (1893), explorent de nouvelles dimensions musicales. Elles se distinguent par une orchestration plus riche, des motifs et leitmotivs, un développement musical plus sophistiqué, une harmonie plus audacieuse et une écriture vocale plus expressive. Mort en 1901, Verdi laisse un héritage immense, célébré encore aujourd'hui pour ses mélodies inoubliables, ses personnages vibrants, sa contribution à l'opéra italien et ses innovations musicales dans ses dernières œuvres. Ces innovations font d'Otello et Falstaff des chefs-d'œuvre uniques, démontrant la maîtrise et la créativité de Verdi qui a su explorer de nouvelles dimensions musicales pour exprimer des émotions et des situations dramatiques complexes.")
            ->setCatalogageSourceAgence(new ExternalThing('http://capdataculture.fr/graph/ORGANISME/36382'))
            ->setCatalogageSourcePays(new ExternalThing('http://capdataculture.fr/graph/PAYS/FR'))
            ->setCatalogageSourceDate(new \DateTimeImmutable('2015-09-14T00:00:00Z'))
            ->setDates('1813-1901')
            ->setDescription('Compositeur.')
            ->setIdentifiantRof('1190')
            ->setIsni(new Isni('http://isni.org/isni/0000000120957510'))
        ;
        $graph->add($personne);
        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/personneConverterTestConvertVerdi.ttl', $actual);
    }

    public function testConvertMozart(): void
    {
        $graph = new Graph();

        $personne = new Personne('https://mon-opera.fr/person/1');
        $personne
            ->setPrenom('Wolfgang Amadeus')
            ->setNomFormeRejet(['Mozart', 'Mozart'])
            ->setNom('Mozart')
            ->setAPourProfession([
                new ExternalThing('http://capdataculture.fr/graph/FONCTION/kpf'),
                new ExternalThing('http://capdataculture.fr/graph/FONCTION/svl'),
                new ExternalThing('http://capdataculture.fr/graph/FONCTION/250'),
                new ExternalThing('http://capdataculture.fr/graph/FONCTION/230'),
                new ExternalThing('http://capdataculture.fr/graph/FONCTION/kco'),
            ])
            ->setArkBnf(new ArkBnf('http://ark.bnf.fr/ark:/12148/cb14027233b'))
            ->setCatalogageSourceAgence(new ExternalThing('http://capdataculture.fr/graph/ORGANISME/36382'))
            ->setCatalogageSourcePays(new ExternalThing('http://capdataculture.fr/graph/PAYS/FR'))
            ->setCatalogageSourceDate(new \DateTimeImmutable('2015-09-14T00:00:00Z'))
            ->setDates('1756-1791')
            ->setDescription("Compositeur, pianiste, violoniste, organiste et chef d'orchestre. Prénoms de baptême : Joannes Chrisostomus Wolfgangus Theophilus.")
            ->setIdentifiantRof('351')
            ->setIsni(new Isni('http://isni.org/isni/121269154'))
        ;
        $graph->add($personne);
        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/personneConverterTestConvertMozart.ttl', $actual);
    }

    public function testConvertChagnon(): void
    {
        $graph = new Graph();

        $personne = new Personne('https://mon-opera.fr/person/1');
        $personne
            ->setPrenom('Marine')
            ->setNom('Chagnon')
            ->setAPourProfession([
                new ExternalThing('http://capdataculture.fr/graph/FONCTION/721')
            ])
            ->setArkBnf(new ArkBnf('http://ark.bnf.fr/ark:/12148/cb18011202z'))
            ->setBiographie("Marine Chagnon, nommée dans la catégorie Révélation Lyrique aux Victoires de la Musique Classique 2023, est une mezzo-soprano française au parcours éclectique. Danse modern-jazz, chant et art dramatique, c’est aujourd’hui dans l’art lyrique qu’elle se réalise après un master au CNSM de Paris dans la classe d’Elene Golgevit. Elle est ensuite artiste en résidence à l’Académie de l’Opéra de Paris et reçoit plusieurs distinctions telles que le prix Jeune Espoir du Concours de Mâcon, le prix spécial du jury du Concours de Canari et le prix de l’AROP pour la saison 2021/2022. Amoureuse du mot, c’est d’abord par la musique ancienne qu’elle découvre la scène avec Euridice de l’Orfeo à l’Opéra de Dijon, la Giuditta (rôle-titre) de Scarlatti (Les Accents) à la Grange au Lac/Chaise Dieu/Auditorium du Louvre, et Poppea de l’Incoronazione di Poppea (Le Poème Harmonique) au Théâtre de l’Athénée ainsi qu’à l’Opéra de Dijon. Marine fait ses débuts à l’Opéra National de Paris dans Il Paggio de Rigoletto (Sagripanti/Guth) à l’opéra Bastille, puis découvre le Palais Garnier avec Tisbe de la Cenerentola (Matheuz/Gallienne) et un gala lyrique dirigé par Gustavo Dudamel retransmis sur Arte. Son début de carrière l’amène à se produire dans d’autres belles salles comme celle de l’Opéra National de Bordeaux (Kate P./ Butterfly), l’Opéra National de Lorraine (Flora/Traviata), Philharmonie de Paris (Olympe la Rebelle), l’Opéra de Vichy (Zweite Dame/Zauberflöte), la Cité des Congrès de Nantes (concert de clôture de la Folle Journée de Nantes), … Elle sort son premier disque chez le label MIRARE, intitulé -Ljus- avec la pianiste Joséphine Ambroselli, qui met en lumière les mélodies suédoises de la première moitié du XXième siècle. Marine Chagnon sera membre de la Troupe de l’Opéra National de Paris pour la saison 2023/2024 et fera plusieurs prises de rôle telles que Zerlina, Don Giovanni à l’Opéra Bastille ou encore le rôle-titre de l’Enfant et les Sortilèges au Palais Garnier.")
            ->setCatalogageSourceAgence(new ExternalThing('http://capdataculture.fr/graph/ORGANISME/36382'))
            ->setCatalogageSourcePays(new ExternalThing('http://capdataculture.fr/graph/PAYS/FR'))
            ->setCatalogageSourceDate(new \DateTimeImmutable('2021-06-07T00:00:00Z'))
            ->setDates('19..-')
            ->setDescription("Artiste lyrique (mezzo-soprano).")
            ->setIdentifiantRof('50406')
            ->setIsni(new Isni('http://isni.org/isni/0000000506854186'))
        ;
        $graph->add($personne);
        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/personneConverterTestConvertChagnon.ttl', $actual);
    }
}
