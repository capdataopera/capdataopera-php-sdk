<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Interpretation;
use CapDataOpera\PhpSdk\Model\Role;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;
use CapDataOpera\PhpSdkTests\Serializer\HasPersonneTestTrait;

class InterpretationConverterTest extends AbstractSerializerTest
{
    use HasPersonneTestTrait;
    public function testConvert(): void
    {
        $graph = new Graph();

        $role = new Role('https://mon-opera.fr/role/1');
        $role->setLabel('Premier violon');
        $graph->add($role);

        $person = $this->getTestPersonne();
        $graph->add($person);

        $interpretation = new Interpretation('https://mon-opera.fr/interpretation/1');
        $interpretation
            ->setAPourRole($role)
            ->setAPourParticipant($person)
        ;
        $graph->add($interpretation);

        $actual = $this->serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/../../fixture/interpretationConverterTest.ttl', $actual);
    }
}
