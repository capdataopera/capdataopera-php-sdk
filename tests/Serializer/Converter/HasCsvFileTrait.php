<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\Converter;

trait HasCsvFileTrait
{
    protected function parseCsvToAssoc(string $filename): array
    {
        $data = [];
        $f = fopen($filename, 'r');

        if ($f === false) {
            $this->fail('Cannot open CSV file');
        }
        while (($row = fgetcsv($f, null, ';')) !== false) {
            $data[] = $row;
        }
        fclose($f);
        // use first row as keys
        $keys = array_shift($data);
        foreach ($data as $i => $row) {
            $data[$i] = array_combine($keys, $row);
        }

        return $data;
    }
}
