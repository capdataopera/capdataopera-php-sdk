<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\AdressePostale;
use CapDataOpera\PhpSdk\Model\Collectivite;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Isni;
use CapDataOpera\PhpSdk\Model\Pays;

trait HasSourceAgenceTestTrait
{
    protected function getCatalogageSourcePays(?Graph $graph): Pays
    {
        $pays = (new Pays('https://mon-opera.fr/pays/1'))
            ->setLabel('FR')
        ;
        if ($graph !== null) {
            $graph->add($pays);
        }
        return $pays;
    }

    protected function getCatalogageSourceAgence(?Graph $graph): Collectivite
    {
        $address = new AdressePostale('https://mon-opera.fr/adresse/1');
        $address
            ->setAdressePostale('1 place de la Comédie')
            ->setCodePostal('69001')
            ->setCommune('Lyon')
            ->setPays(new ExternalThing('https://mon-opera.fr/pays/1'))
        ;

        $ownOrg = new Collectivite('https://mon-opera.fr/organization/1');
        $ownOrg->setNom('Mon opéra de test')
            ->setFacebook('https://facebook.com/mon-opera')
            ->setSiteWeb('https://mon-opera.fr')
            ->setCatalogageSourceAgence($ownOrg)
            ->setAdresse($address)
            ->setCatalogageSourceDate(new \DateTimeImmutable('2024-01-30T00:00:00+00:00'))
            ->setCatalogageSourcePays($this->getCatalogageSourcePays($graph))
            ->setIsni(new Isni('https://isni.org/isni/0000000122982840'));
        if ($graph !== null) {
            $graph->add($address);
            $graph->add($ownOrg);
        }
        return $ownOrg;
    }
}
