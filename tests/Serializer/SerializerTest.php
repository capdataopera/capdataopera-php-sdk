<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Collectivite;
use CapDataOpera\PhpSdk\Model\Isni;
use CapDataOpera\PhpSdk\Serializer\Serializer;
use EasyRdf\Literal;
use PHPUnit\Framework\TestCase;

class SerializerTest extends TestCase
{
    use HasSourceAgenceTestTrait;

    public function testSerialize(): void
    {
        $serializer = new Serializer();
        $graph = new Graph();
        $this->getCatalogageSourceAgence($graph);

        $actual = $serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(
            dirname(__FILE__) . '/../fixture/graphConverterTestTestConvert.ttl',
            $actual
        );
    }

    public function testSerializeOnlyCapdata(): void
    {
        $serializer = new Serializer();
        $graph = new Graph();
        $this->getCatalogageSourceAgence($graph);

        $actual = $serializer->serialize($graph, 'turtle', ['capdata']);
        $this->assertIsString($actual);
        $this->assertStringEqualsFile(
            dirname(__FILE__) . '/../fixture/serializerTestTestSerializeOnlyCapdata.ttl',
            $actual
        );
    }

    public function testSerializeOnlyCapdataWithCustomProperty(): void
    {
        $serializer = new Serializer();
        $graph = new Graph();

        $ownOrg = new Collectivite('https://mon-opera.fr/organization/1');
        $ownOrg->setNom('Mon opéra de test')
            ->setFacebook('https://facebook.com/mon-opera')
            ->setSiteWeb('https://mon-opera.fr')
            ->setCatalogageSourceAgence($ownOrg)
            ->setDateCreationRessource(new \DateTimeImmutable('2022-01-30T00:00:00+00:00'))
            ->setDateModificationRessource(new \DateTimeImmutable('2024-01-30T00:00:00+00:00'))
            ->setIsni(new Isni('https://isni.org/isni/0000000122982840'));
        $graph->add($ownOrg);

        /**
         * Ajout d'une propriété personnalisée
         */
        $graph->addResource(
            'https://mon-opera.fr/organization/1',
            'https://schema.org/startDate',
            new Literal('2016-04-21T20:00', null, 'xsd:dateTime')
        );

        $actual = $serializer->serialize($graph, 'turtle', ['capdata']);

        $this->assertIsString($actual);
        $this->assertStringEqualsFile(
            dirname(__FILE__) . '/../fixture/serializerTestTestSerializeOnlyCapdataWithCustomProperty.ttl',
            $actual
        );
    }
}
