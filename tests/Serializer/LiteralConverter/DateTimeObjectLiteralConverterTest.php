<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\DateTimeObjectLiteralConverter;
use CapDataOpera\PhpSdk\ValueObject\DateTimeObject;
use PHPUnit\Framework\TestCase;

class DateTimeObjectLiteralConverterTest extends TestCase
{
    public function testConvert(): void
    {
        $graph = new Graph();
        $converter = new DateTimeObjectLiteralConverter();
        $dateTimeObject = new DateTimeObject('2021-01-01T00:00:00+00:00');

        $converted = $converter->convert($dateTimeObject, $graph);
        $this->assertIsArray($converted);
        $this->assertInstanceOf(\EasyRdf\Literal::class, $converted[0]);
        $this->assertEquals('2021-01-01T00:00:00Z', $converted[0]->getValue());
        $this->assertEquals('xsd:dateTime', $converted[0]->getDatatype());
    }

    public function testMultipleConvert(): void
    {
        $graph = new Graph();
        $converter = new DateTimeObjectLiteralConverter();
        $dateTimeObject = new DateTimeObject([
            '2021-01-01T00:00:00+00:00',
            new \DateTimeImmutable('2021-02-01T00:00:00+00:00'),
        ]);

        $converted = $converter->convert($dateTimeObject, $graph);
        $this->assertIsArray($converted);
        $this->assertInstanceOf(\EasyRdf\Literal::class, $converted[0]);
        $this->assertEquals('2021-01-01T00:00:00Z', $converted[0]->getValue());
        $this->assertEquals('xsd:dateTime', $converted[0]->getDatatype());
        $this->assertInstanceOf(\EasyRdf\Literal::class, $converted[1]);
        $this->assertEquals('2021-02-01T00:00:00Z', $converted[1]->getValue());
        $this->assertEquals('xsd:dateTime', $converted[1]->getDatatype());
    }
}
