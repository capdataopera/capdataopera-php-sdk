<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\Isni;
use CapDataOpera\PhpSdk\Model\Personne;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\BooleanObjectLiteralConverter;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\ChainLiteralConverter;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\DateTimeObjectLiteralConverter;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\NumericObjectLiteralConverter;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\RelationObjectLiteralConverter;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\StringObjectLiteralConverter;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\UriObjectLiteralConverter;
use CapDataOpera\PhpSdk\ValueObject\BooleanObject;
use CapDataOpera\PhpSdk\ValueObject\DateTimeObject;
use CapDataOpera\PhpSdk\ValueObject\NumericObject;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;
use CapDataOpera\PhpSdk\ValueObject\UriObject;
use EasyRdf\Literal;
use PHPUnit\Framework\TestCase;

class ChainLiteralConverterTest extends TestCase
{
    public function testConvert(): void
    {
        $graph = new Graph();
        $converter = new ChainLiteralConverter([
            new DateTimeObjectLiteralConverter(),
            new StringObjectLiteralConverter(),
            new BooleanObjectLiteralConverter(),
            new NumericObjectLiteralConverter(),
            new UriObjectLiteralConverter(),
            new RelationObjectLiteralConverter(),
        ]);

        $valueObjects = [
            new StringObject('Une chaine de caractères'),
            new StringObject(['Une chaine de caractères', 'Une autre chaine de caractères']),
            new BooleanObject(false),
            new DateTimeObject('2021-01-01T00:00:00+00:00'),
            new RelationObject(
                [
                    (new Personne())->setUri('https://opera-bordeaux.com/personne/1359'),
                    (new Personne())->setUri('https://opera-bordeaux.com/personne/1440'),
                    new Isni('https://isni.org/isni/0000000122982840'),
                ],
                [Personne::class, Isni::class]
            ),
            new UriObject([
                'https://twitter.com/operadebordeaux',
                'https://facebook.com/operadebordeaux'
            ]),
            new NumericObject([
                '192.001',
                10,
                0.87,
            ])
        ];

        $expectedValues = [
            [
                new Literal('Une chaine de caractères')
            ],
            [
                new Literal('Une chaine de caractères'),
                new Literal('Une autre chaine de caractères')
            ],
            [
                new Literal('false', null, 'xsd:boolean')
            ],
            [
                new Literal('2021-01-01T00:00:00Z', null, 'xsd:dateTime')
            ],
            [
                'https://opera-bordeaux.com/personne/1359',
                'https://opera-bordeaux.com/personne/1440',
                'https://isni.org/isni/0000000122982840',
            ],
            [
                new Literal('https://twitter.com/operadebordeaux', null, 'xsd:anyURI'),
                new Literal('https://facebook.com/operadebordeaux', null, 'xsd:anyURI')
            ],
            [
                new Literal('192.001', null, 'xsd:float'),
                new Literal('10', null, 'xsd:integer'),
                new Literal('0.87', null, 'xsd:float')
            ]
        ];

        foreach ($valueObjects as $key => $valueObject) {
            $converted = $converter->convert($valueObject, $graph);
            $this->assertEquals($expectedValues[$key], $converted);
        }
    }
}
