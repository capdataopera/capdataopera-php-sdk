<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\UriObjectLiteralConverter;
use CapDataOpera\PhpSdk\ValueObject\UriObject;
use PHPUnit\Framework\TestCase;

class UriObjectLiteralConverterTest extends TestCase
{
    public function testConvert(): void
    {
        $graph = new Graph();
        $converter = new UriObjectLiteralConverter();
        $uriObject = new UriObject('https://twitter.com/operadebordeaux');

        $converted = $converter->convert($uriObject, $graph);
        $this->assertIsArray($converted);
        $this->assertInstanceOf(\EasyRdf\Literal::class, $converted[0]);
        $this->assertEquals('https://twitter.com/operadebordeaux', $converted[0]->getValue());
        $this->assertEquals('xsd:anyURI', $converted[0]->getDatatype());
    }

    public function testMultipleConvert(): void
    {
        $graph = new Graph();
        $converter = new UriObjectLiteralConverter();
        $uriObject = new UriObject([
            'https://twitter.com/operadebordeaux',
            'https://facebook.com/operadebordeaux'
        ]);

        $converted = $converter->convert($uriObject, $graph);
        $this->assertIsArray($converted);
        $this->assertInstanceOf(\EasyRdf\Literal::class, $converted[0]);
        $this->assertEquals('https://twitter.com/operadebordeaux', $converted[0]->getValue());
        $this->assertEquals('xsd:anyURI', $converted[0]->getDatatype());
        $this->assertInstanceOf(\EasyRdf\Literal::class, $converted[1]);
        $this->assertEquals('https://facebook.com/operadebordeaux', $converted[1]->getValue());
        $this->assertEquals('xsd:anyURI', $converted[0]->getDatatype());
    }
}
