<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer\LiteralConverter;

use CapDataOpera\PhpSdk\Exception\TooEarlyUsageException;
use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Isni;
use CapDataOpera\PhpSdk\Model\Personne;
use CapDataOpera\PhpSdk\Serializer\LiteralConverter\RelationObjectLiteralConverter;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use PHPUnit\Framework\TestCase;

class RelationObjectLiteralConverterTest extends TestCase
{
    public function testExternalResourceConvert(): void
    {
        $graph = new Graph();
        $converter = new RelationObjectLiteralConverter();
        $relationObject = new RelationObject(
            new Isni('https://isni.org/isni/0000000122982840'),
            Isni::class
        );

        $converted = $converter->convert($relationObject, $graph);
        $this->assertIsArray($converted);
        $this->assertIsString($converted[0]);
        $this->assertEquals('https://isni.org/isni/0000000122982840', $converted[0]);
        $this->assertNotContains('https://isni.org/isni/0000000122982840', $graph->getRequiredInternalNodesUri());
    }
    public function testExternalThingConvert(): void
    {
        $graph = new Graph();
        $converter = new RelationObjectLiteralConverter();
        $relationObject = new RelationObject(
            new ExternalThing('https://isni.org/isni/0000000122982840'),
            Isni::class
        );

        $converted = $converter->convert($relationObject, $graph);
        $this->assertIsArray($converted);
        $this->assertIsString($converted[0]);
        $this->assertEquals('https://isni.org/isni/0000000122982840', $converted[0]);
        $this->assertNotContains('https://isni.org/isni/0000000122982840', $graph->getRequiredInternalNodesUri());
    }
    public function testEmptyExternalThingConvert(): void
    {
        $this->expectException(TooEarlyUsageException::class);

        $graph = new Graph();
        $converter = new RelationObjectLiteralConverter();
        $emptyRelationObject = new RelationObject(
            new ExternalThing(''),
            Isni::class
        );

        $converter->convert($emptyRelationObject, $graph);
    }

    public function testInternalResourceConvert(): void
    {
        $graph = new Graph();
        $converter = new RelationObjectLiteralConverter();
        $relationObject = new RelationObject(
            (new Personne())->setUri('https://opera-bordeaux.com/personne/1359'),
            Personne::class
        );

        $converted = $converter->convert($relationObject, $graph);
        $this->assertIsArray($converted);
        $this->assertIsString($converted[0]);
        $this->assertEquals('https://opera-bordeaux.com/personne/1359', $converted[0]);
        $this->assertContains('https://opera-bordeaux.com/personne/1359', $graph->getRequiredInternalNodesUri());
    }

    public function testMultipleInternalResourceConvert(): void
    {
        $graph = new Graph();
        $converter = new RelationObjectLiteralConverter();
        $relationObject = new RelationObject(
            [
                new Personne('https://opera-bordeaux.com/personne/1359'),
                new Personne('https://opera-bordeaux.com/personne/1440'),
                new ExternalThing('https://isni.org/isni/0000000122982840'),
            ],
            [Personne::class, Isni::class]
        );

        $converted = $converter->convert($relationObject, $graph);
        $this->assertIsArray($converted);
        $this->assertIsString($converted[0]);
        $this->assertEquals('https://opera-bordeaux.com/personne/1359', $converted[0]);
        $this->assertContains('https://opera-bordeaux.com/personne/1359', $graph->getRequiredInternalNodesUri());
        $this->assertEquals('https://opera-bordeaux.com/personne/1440', $converted[1]);
        $this->assertContains('https://opera-bordeaux.com/personne/1440', $graph->getRequiredInternalNodesUri());
        $this->assertEquals('https://isni.org/isni/0000000122982840', $converted[2]);
        $this->assertNotContains('https://isni.org/isni/0000000122982840', $graph->getRequiredInternalNodesUri());
    }
}
