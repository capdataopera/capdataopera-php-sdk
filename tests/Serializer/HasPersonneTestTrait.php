<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer;

use CapDataOpera\PhpSdk\Model\Personne;

trait HasPersonneTestTrait
{
    public function getTestPersonne(): Personne
    {
        $person = new Personne('https://mon-opera.fr/person/1');
        $person
            ->setNom('Maupate')
            ->setPrenom('Ambroise')
            ->setSiteWeb('https://rezo-zero.com')
            ->setImage('https://avatars.githubusercontent.com/u/380026?v=4')
            ->setCatalogageSourceDate(new \DateTimeImmutable('2024-01-30T00:00:00+00:00'))
        ;
        return $person;
    }
}
