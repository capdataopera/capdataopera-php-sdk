<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\Serializer;

use CapDataOpera\PhpSdk\Serializer\Converter\GraphConverter;
use CapDataOpera\PhpSdk\Serializer\Serializer;
use PHPUnit\Framework\TestCase;

abstract class AbstractSerializerTest extends TestCase
{
    use HasSourceAgenceTestTrait;
    protected GraphConverter $graphConverter;
    protected Serializer $serializer;

    /**
     * @param string|null $name
     * @param array<mixed> $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->serializer = new Serializer();
        $this->graphConverter = $this->serializer->getGraphConverter();
    }
}
