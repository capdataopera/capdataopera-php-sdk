<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\ValueObject;

use CapDataOpera\PhpSdk\ValueObject\BooleanObject;
use PHPUnit\Framework\TestCase;

class BooleanObjectTest extends TestCase
{
    public function testConstruct(): void
    {
        new BooleanObject(true);
        new BooleanObject(false);
        new BooleanObject([
            true,
            false
        ]);

        $this->expectNotToPerformAssertions();
    }

    public function testBadSingleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new BooleanObject(102);
    }

    public function testBadUriSingleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new BooleanObject('opera-bordeaux.com/taxonomy/term/527');
    }

    public function testBadMultipleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new BooleanObject(['My string', false]);
    }

    public function testSingleValue(): void
    {
        $object = new BooleanObject(true);

        $this->assertFalse($object->isMultiple());
        $this->assertTrue($object->getValue());
        $this->assertEquals('true', $object->__toString());
        $this->assertEquals('true', (string) $object);
        $this->assertEquals([true], $object->serialize());
    }
}
