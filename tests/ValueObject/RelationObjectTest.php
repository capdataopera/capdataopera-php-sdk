<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\ValueObject;

use CapDataOpera\PhpSdk\Exception\BadRelationClassException;
use CapDataOpera\PhpSdk\Exception\OutOfOntologyClassException;
use CapDataOpera\PhpSdk\Model\Collectivite;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Fonction;
use CapDataOpera\PhpSdk\Model\Isni;
use CapDataOpera\PhpSdk\Model\Personne;
use CapDataOpera\PhpSdk\Model\Saison;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;
use PHPUnit\Framework\TestCase;

class RelationObjectTest extends TestCase
{
    public function testConstruct(): void
    {
        $metteurEnScene = new Fonction();
        $metteurEnScene->setLabel(StringObject::fromString('Metteur en scène'));
        $styliste = new Fonction();
        $styliste->setLabel(StringObject::fromString('Styliste'));
        new RelationObject(
            $metteurEnScene,
            Fonction::class
        );
        new RelationObject(
            [$metteurEnScene, $styliste],
            Fonction::class
        );

        $this->expectNotToPerformAssertions();
    }

    public function testBadRelationClassException(): void
    {
        $metteurEnScene = new Fonction();
        $metteurEnScene->setLabel(StringObject::fromString('Metteur en scène'));

        $this->expectException(BadRelationClassException::class);
        $this->expectExceptionMessage(
            'Expects '.Saison::class.'|'.ExternalThing::class.' value, '.Fonction::class.' value given'
        );
        new RelationObject(
            $metteurEnScene,
            Saison::class
        );
    }

    public function testOutOfOntologyClassException(): void
    {
        $now = new \DateTime('now');

        $this->expectException(OutOfOntologyClassException::class);
        new RelationObject(
            // @phpstan-ignore-next-line
            $now,
            // @phpstan-ignore-next-line
            \DateTime::class
        );
    }

    public function testMultipleSerialize(): void
    {
        $metteurEnScene = new Fonction();
        $metteurEnScene->setUri('https://monopera.fr/fonction/1');
        $metteurEnScene->setLabel(StringObject::fromString('Metteur en scène'));
        $styliste = new Fonction();
        $styliste->setUri('https://monopera.fr/fonction/2');
        $styliste->setLabel(StringObject::fromString('Styliste'));

        $relation = new RelationObject(
            [$metteurEnScene, $styliste],
            Fonction::class
        );

        $this->assertTrue($relation->isMultiple());
        $this->assertEquals([
            'https://monopera.fr/fonction/1',
            'https://monopera.fr/fonction/2'
        ], $relation->serialize());
    }

    public function testSingleSerialize(): void
    {
        $metteurEnScene = new Fonction();
        $metteurEnScene->setUri('https://monopera.fr/fonction/1');
        $metteurEnScene->setLabel(StringObject::fromString('Metteur en scène'));

        $relation = new RelationObject(
            $metteurEnScene,
            Fonction::class
        );

        $this->assertFalse($relation->isMultiple());
        $this->assertEquals([
            'https://monopera.fr/fonction/1'
        ], $relation->serialize());
    }

    public function testSingleTypeSerialize(): void
    {
        $personne = new Personne();
        $personne->setUri('https://monopera.fr/person/1');
        $personne->setNom(StringObject::fromString('Maupate'));
        $personne->setPrenom(StringObject::fromString('Ambroise'));

        $organization = new Collectivite();
        $organization->setUri('https://monopera.fr/organization/1');
        $organization->setNom(StringObject::fromString('Rezo Zero'));

        $this->expectException(BadRelationClassException::class);
        $this->expectExceptionMessage(
            'Expects '.Personne::class.'|'.ExternalThing::class.' value, '.Collectivite::class.' value given'
        );
        $relation = new RelationObject(
            [$personne, $organization],
            Personne::class
        );
        $this->assertTrue($relation->isMultiple());
        $this->assertEquals([
            'https://monopera.fr/person/1',
            'https://monopera.fr/organization/1',
        ], $relation->serialize());
    }

    public function testMultipleTypesSerialize(): void
    {
        $personne = new Personne();
        $personne->setUri('https://monopera.fr/person/1');
        $personne->setNom(StringObject::fromString('Maupate'));
        $personne->setPrenom(StringObject::fromString('Ambroise'));

        $organization = new Collectivite();
        $organization->setUri('https://monopera.fr/organization/1');
        $organization->setNom(StringObject::fromString('Rezo Zero'));

        $relation = new RelationObject(
            [$personne, $organization],
            [Personne::class, Collectivite::class]
        );
        $this->assertTrue($relation->isMultiple());
        $this->assertEquals([
            'https://monopera.fr/person/1',
            'https://monopera.fr/organization/1',
        ], $relation->serialize());
    }

    public function testInvalidMultipleTypesSerialize(): void
    {
        $dateTime = new \DateTime();

        $this->expectException(BadRelationClassException::class);
        $this->expectExceptionMessage(
            'Expects '.Personne::class.'|'.Collectivite::class.'|'.ExternalThing::class.' value, '.\DateTime::class.' value given'
        );
        new RelationObject(
            $dateTime,
            [Personne::class, Collectivite::class]
        );
    }
}
