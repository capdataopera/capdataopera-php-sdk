<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\ValueObject;

use CapDataOpera\PhpSdk\ValueObject\StringObject;
use PHPUnit\Framework\TestCase;

class StringObjectTest extends TestCase
{
    public function testConstruct(): void
    {
        new StringObject('My string');
        new StringObject(['My string', 'My second string']);

        $this->expectNotToPerformAssertions();
    }
    public function testTrim(): void
    {
        $this->assertEquals(['My string'], (new StringObject('My string '))->trim()->serialize());
        $this->assertEquals(
            ['My string', 'My second string'],
            (new StringObject(['  My string  ', 'My second string  ']))->trim()->serialize()
        );
    }
    public function testConcat(): void
    {
        $this->assertEquals(
            ['My string'],
            (new StringObject('My  '))->trim()->append(' ')->append('string  ')->trim()->serialize()
        );
        $this->assertEquals(
            ['My string', 'My second string'],
            (new StringObject('My  '))->trim()->append(' ')->append(new StringObject(['string  ', 'second string  ']))->trim()->serialize()
        );
        $this->assertEquals(
            ['First string', 'Second string'],
            (new StringObject(['First', 'Second']))->trim()->append(' ')->append(new StringObject(['string']))->trim()->serialize()
        );
        $this->assertEquals(
            ['First string', 'Second object'],
            (new StringObject(['First', 'Second']))->trim()->append(' ')->append(new StringObject(['string', 'object']))->trim()->serialize()
        );
    }

    public function testBadSingleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new StringObject(102);
    }
    public function testBadMultipleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new StringObject(['My string', 102]);
    }

    public function testSingleValue(): void
    {
        $object = new StringObject('My string');

        $this->assertFalse($object->isMultiple());
        $this->assertEquals('My string', $object->getValue());
        $this->assertEquals('My string', $object->__toString());
        $this->assertEquals('My string', (string) $object);
        $this->assertEquals(['My string'], $object->serialize());
    }

    public function testMultipleValue(): void
    {
        $object = new StringObject(['My string', 'My second string']);

        $this->assertTrue($object->isMultiple());
        $this->assertEquals(['My string', 'My second string'], $object->getValue());
        $this->assertEquals('My string, My second string', $object->__toString());
        $this->assertEquals('My string, My second string', (string) $object);
        $this->assertEquals(['My string', 'My second string'], $object->serialize());
    }
}
