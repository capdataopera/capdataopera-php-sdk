<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\ValueObject;

use CapDataOpera\PhpSdk\ValueObject\DateTimeObject;
use PHPUnit\Framework\TestCase;

class DateTimeObjectTest extends TestCase
{
    public function testConstruct(): void
    {
        new DateTimeObject('now');
        new DateTimeObject('2024-01-29T17:12:00+01:00');
        new DateTimeObject(new \DateTimeImmutable('now'));
        new DateTimeObject(['2024-01-29T17:12:00+01:00', 'now']);
        new DateTimeObject(['2024-01-29T17:12:00+01:00', new \DateTimeImmutable('now')]);

        $this->expectNotToPerformAssertions();
    }

    public function testBadSingleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new DateTimeObject(102);
        new DateTimeObject('random string');
    }

    public function testBadMultipleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new DateTimeObject([102, '2024-01-29T17:12:00+01:00']);
        new DateTimeObject(['random string', new \DateTimeImmutable('now')]);
    }

    public function testSingleValue(): void
    {
        $object = new DateTimeObject('2024-01-29T17:12:00+01:00');

        $this->assertFalse($object->isMultiple());
        $this->assertEquals(new \DateTimeImmutable('2024-01-29T17:12:00+01:00'), $object->getValue());
        $this->assertEquals(new \DateTimeImmutable('2024-01-29T16:12:00Z'), $object->getValue());
        $this->assertEquals('2024-01-29T16:12:00Z', $object->__toString());
        $this->assertEquals('2024-01-29T16:12:00Z', (string) $object);
        $this->assertEquals(['2024-01-29T16:12:00Z'], $object->serialize());
    }

    public function testMultipleValue(): void
    {
        $object = new DateTimeObject([
            '2024-01-29T17:12:00+01:00',
            new \DateTimeImmutable('2024-01-30T17:12:00+01:00')
        ]);

        $this->assertTrue($object->isMultiple());
        $this->assertEquals([
            new \DateTimeImmutable('2024-01-29T17:12:00+01:00'),
            new \DateTimeImmutable('2024-01-30T17:12:00+01:00'),
        ], $object->getValue());
        $this->assertEquals('2024-01-29T16:12:00Z, 2024-01-30T16:12:00Z', $object->__toString());
        $this->assertEquals('2024-01-29T16:12:00Z, 2024-01-30T16:12:00Z', (string) $object);
        $this->assertEquals(['2024-01-29T16:12:00Z', '2024-01-30T16:12:00Z'], $object->serialize());
    }
}
