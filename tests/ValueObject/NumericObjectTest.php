<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\ValueObject;

use CapDataOpera\PhpSdk\ValueObject\NumericObject;
use PHPUnit\Framework\TestCase;

class NumericObjectTest extends TestCase
{
    public function testConstruct(): void
    {
        new NumericObject(10);
        new NumericObject(120.98);
        new NumericObject([
            102,
            '109.98'
        ]);

        $this->expectNotToPerformAssertions();
    }

    public function testBadSingleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new NumericObject(false);
    }

    public function testBadUriSingleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new NumericObject('opera-bordeaux.com/taxonomy/term/527');
    }

    public function testBadMultipleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new NumericObject(['My string', false]);
    }

    public function testSingleIntValue(): void
    {
        $object = new NumericObject(120);

        $this->assertFalse($object->isMultiple());
        $this->assertIsNumeric($object->getValue());
        $this->assertIsInt($object->getValue());
        $this->assertEquals('120', $object->__toString());
        $this->assertEquals('120', (string) $object);
        $this->assertEquals([120], $object->serialize());
    }

    public function testSingleFloatValue(): void
    {
        $object = new NumericObject(120.45);

        $this->assertFalse($object->isMultiple());
        $this->assertIsNumeric($object->getValue());
        $this->assertIsFloat($object->getValue());
        $this->assertEquals('120.45', $object->__toString());
        $this->assertEquals('120.45', (string) $object);
        $this->assertEquals([120.45], $object->serialize());
    }

    public function testSingleStringValue(): void
    {
        $object = new NumericObject('1230.45098');

        $this->assertFalse($object->isMultiple());
        $this->assertIsNumeric($object->getValue());
        $this->assertIsString($object->getValue());
        $this->assertEquals('1230.45098', $object->__toString());
        $this->assertEquals('1230.45098', (string) $object);
        $this->assertEquals(['1230.45098'], $object->serialize());
    }
}
