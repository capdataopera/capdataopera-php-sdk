<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests\ValueObject;

use CapDataOpera\PhpSdk\ValueObject\UriObject;
use PHPUnit\Framework\TestCase;

class UriObjectTest extends TestCase
{
    public function testConstruct(): void
    {
        new UriObject('https://isni.org/isni/0000000122982840');
        new UriObject('https://opera-bordeaux.com/thierry-de-mey-1359');
        new UriObject('https://opera-bordeaux.com/thiérry-de-mey-1359');
        new UriObject('https://hi.wikipedia.org/wiki/%E0%A4%AE%E0%A5%81%E0%A4%96%E0%A4%AA%E0%A5%83%E0%A4%B7%E0%A5%8D%E0%A4%A0');
        new UriObject('https://hi.wikipedia.org/wiki/मुखपृष्ठ');
        new UriObject([
            'https://capdataculture.fr/graph/identifier/4022',
            'https://opera-bordeaux.com/taxonomy/term/527'
        ]);
        new UriObject([
            'https://capdataculture.fr/graph/identifier/4022',
            'https://opera-bordeaux.com/thiérry-de-mey-1359'
        ]);

        $this->expectNotToPerformAssertions();
    }

    public function testEncodedUrlValue(): void
    {
        $this->assertEquals(
            'https://opera-bordeaux.com/thi%c3%a9rry-de-mey-1359',
            (new UriObject('https://opera-bordeaux.com/thiérry-de-mey-1359'))->getValue()
        );

        $this->assertEquals(
            ['https://opera-bordeaux.com/thi%c3%a9rry-de-mey-1359'],
            (new UriObject(['https://opera-bordeaux.com/thiérry-de-mey-1359']))->getValue()
        );

        $this->assertEquals(
            'https://hi.wikipedia.org/wiki/%e0%a4%ae%e0%a5%81%e0%a4%96%e0%a4%aa%e0%a5%83%e0%a4%b7%e0%a5%8d%e0%a4%a0',
            (new UriObject('https://hi.wikipedia.org/wiki/मुखपृष्ठ'))->getValue()
        );
    }

    public function testBadSingleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new UriObject(102);
    }

    public function testBadUriSingleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new UriObject('opera-bordeaux.com/taxonomy/term/527');
    }

    public function testBadMultipleConstruct(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new UriObject(['My string', 102]);
    }

    public function testSingleValue(): void
    {
        $object = new UriObject('https://opera-bordeaux.com/taxonomy/term/527');

        $this->assertFalse($object->isMultiple());
        $this->assertEquals('https://opera-bordeaux.com/taxonomy/term/527', $object->getValue());
        $this->assertEquals('https://opera-bordeaux.com/taxonomy/term/527', $object->__toString());
        $this->assertEquals('https://opera-bordeaux.com/taxonomy/term/527', (string) $object);
        $this->assertEquals(['https://opera-bordeaux.com/taxonomy/term/527'], $object->serialize());
    }

    public function testMultipleValue(): void
    {
        $object = new UriObject([
            'https://capdataculture.fr/graph/identifier/4022',
            'https://opera-bordeaux.com/taxonomy/term/527'
        ]);

        $this->assertTrue($object->isMultiple());
        $this->assertEquals([
            'https://capdataculture.fr/graph/identifier/4022',
            'https://opera-bordeaux.com/taxonomy/term/527'
        ], $object->getValue());
        $this->assertEquals(
            'https://capdataculture.fr/graph/identifier/4022, https://opera-bordeaux.com/taxonomy/term/527',
            $object->__toString()
        );
        $this->assertEquals(
            'https://capdataculture.fr/graph/identifier/4022, https://opera-bordeaux.com/taxonomy/term/527',
            (string) $object
        );
        $this->assertEquals([
            'https://capdataculture.fr/graph/identifier/4022',
            'https://opera-bordeaux.com/taxonomy/term/527'
        ], $object->serialize());
    }
}
