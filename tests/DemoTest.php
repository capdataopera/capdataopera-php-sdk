<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests;

use CapDataOpera\PhpSdk\Graph\Graph;
use CapDataOpera\PhpSdk\Model\AdressePostale;
use CapDataOpera\PhpSdk\Model\Evenement;
use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Image;
use CapDataOpera\PhpSdk\Model\Lieu;
use CapDataOpera\PhpSdk\Model\Production;
use CapDataOpera\PhpSdk\Model\Saison;
use CapDataOpera\PhpSdk\Model\Sound;
use CapDataOpera\PhpSdk\Serializer\Serializer;
use CapDataOpera\PhpSdkTests\Serializer\HasPersonneTestTrait;
use CapDataOpera\PhpSdkTests\Serializer\HasSourceAgenceTestTrait;
use PHPUnit\Framework\TestCase;

/*
 * This is a demo test
 *
 * ```
 * vendor/bin/phpunit tests/DemoTest.php
 * ```
 */
class DemoTest extends TestCase
{
    use HasPersonneTestTrait;
    use HasSourceAgenceTestTrait;

    public function testDemo(): void
    {
        $serializer = new Serializer();
        $graph = new Graph();

        $sourceAgence = $this->getCatalogageSourceAgence($graph);

        $image = (new Image('https://mon-opera.fr/saison/1/media/1'))
            ->setContentUrl('https://mon-opera.fr/saison/1/media/1.jpg');
        $sound = (new Sound('https://mon-opera.fr/saison/1/media/2'))
            ->setDescription('Ceci est un son')
            ->setContentUrl('https://mon-opera.fr/saison/1/media/1.mp3');
        $saison = new Saison('https://mon-opera.fr/saison/1');
        $saison
            ->setLabel('2023-2024')
            ->setMedia([
                $image,
                $sound,
            ])
            ->setDescription('La saison **2023-2024** de l\'Opéra de Paris')
        ;
        $graph->add($image);
        $graph->add($sound);
        $graph->add($saison);

        $production = new Production('https://mon-opera.fr/saison/1/production/1');
        $production
            ->setTitre('Carmen')
            ->setAPourSaison($saison)
            ->setCatalogageSourcePays(new ExternalThing('https://capdataopera.fr/PAYS/FR'))
            ->setCatalogageSourceDate(new \DateTimeImmutable('2023-01-01T12:00:00+01:00'))
            ->setCatalogageSourceAgence($sourceAgence)
        ;
        $graph->add($production);

        $address = new AdressePostale('https://mon-opera.fr/adresse/1');
        $address
            ->setAdressePostale('8 rue Scribe, 75009 Paris')
            ->setName('Opéra Garnier')
            ->setCodePostal('75009')
            ->setCommune('Paris')
        ;

        $lieu = new Lieu('https://mon-opera.fr/saison/1/lieu/1');
        $lieu
            ->setAdresse($address)
            ->setName('Opéra Garnier')
        ;

        $evenement = new Evenement('https://mon-opera.fr/saison/1/evenement/1');
        $evenement
            ->setAPourLieu($lieu)
            ->setDateDebut(new \DateTimeImmutable('2023-01-01T20:00:00+01:00'))
            ->setDateFin(new \DateTimeImmutable('2023-01-01T23:00:00+01:00'))
            ->setAPourProduction($production);

        $graph->add($address);
        $graph->add($lieu);
        $graph->add($evenement);

        $actual = $serializer->serialize($graph, 'turtle', ['capdata', 'schema']);
        $this->assertIsString($actual);

        // Uncomment to print the actual result
        //echo PHP_EOL . PHP_EOL . $actual . PHP_EOL;
    }
}
