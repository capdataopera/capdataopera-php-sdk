<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests;

use CapDataOpera\PhpSdk\Model\ExternalThing;
use CapDataOpera\PhpSdk\Model\Participation;
use CapDataOpera\PhpSdk\Model\Personne;
use CapDataOpera\PhpSdk\ValueObject\RelationObject;
use CapDataOpera\PhpSdk\ValueObject\StringObject;
use CapDataOpera\PhpSdkTests\Serializer\AbstractSerializerTest;
use CapDataOpera\PhpSdkTests\Serializer\HasPersonneTestTrait;

class ObjectSerializationTest extends AbstractSerializerTest
{
    use HasPersonneTestTrait;

    public function testSerializedObject(): void
    {
        $participation = new Participation('https://www.chatelet.com/participation/181400180129');
        $participation->setAPourParticipant(
            new RelationObject(
                [
                    new ExternalThing('https://www.chatelet.com/personne/181400180129'),
                    new ExternalThing('https://www.chatelet.com/personne/181400180130')
                ],
                Personne::class
            )
        );

        $serialized = serialize($participation);
        $unserialized = unserialize($serialized);

        $this->assertEquals($participation, $unserialized);
    }

    public function testSerializedLiteral(): void
    {
        $stringObject = new StringObject('Test');
        $serialized = serialize($stringObject);
        $unserialized = unserialize($serialized);
        $this->assertEquals($stringObject, $unserialized);


        $stringObject2 = new StringObject(['Test', 'Test2']);
        $serialized2 = serialize($stringObject2);
        $unserialized2 = unserialize($serialized2);
        $this->assertEquals($stringObject2, $unserialized2);
    }
}
