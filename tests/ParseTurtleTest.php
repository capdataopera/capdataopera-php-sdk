<?php

declare(strict_types=1);

namespace CapDataOpera\PhpSdkTests;

use EasyRdf\Graph;
use PHPUnit\Framework\TestCase;

class ParseTurtleTest extends TestCase
{
    public function testTurtleRdfFile(): void
    {
        $file = dirname(__FILE__) . '/fixture/graphConverterTestTestConvert.ttl';
        $fileContents = file_get_contents($file);

        if ($fileContents === false) {
            $this->fail('Failed to read file contents');
        }

        $graph = new Graph();
        $count = $graph->parse($fileContents, 'turtle');

        $this->assertEquals([
            'schema:Thing',
            'skos:Concept',
            'rof:Pays',
            'rof:Referentiel',
            'schema:Country',
        ], $graph->types('https://mon-opera.fr/pays/1'));
        $this->assertEquals(
            'FR',
            $graph->getLiteral('https://mon-opera.fr/pays/1', 'schema:name')->getValue()
        );
    }
}
