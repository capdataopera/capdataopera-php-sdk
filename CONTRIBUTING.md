# Contribuer au projet

https://www.rof.fr/rof/capdata-opera.aspx

## Référentiel

https://ontologie.capdataculture.fr/

## Créer une issue

https://gitlab.com/capdataopera/capdataopera-php-sdk/-/issues/new

## Créer une pull-request

https://gitlab.com/capdataopera/capdataopera-php-sdk/-/merge_requests/new

- Veuillez choisir la branche `develop` comme destination de votre pull-request.
- Vérifiez que votre éditeur de texte respecte les conventions de formatage de code : EditorConfig
- Vérifiez que votre code passe les tests : `make test`
- Si vous ajoutez des fonctionnalités, veuillez ajouter des tests unitaires.

## Tests

`make test`

ou

```shell
vendor/bin/phpstan analyze -c phpstan.neon
XDEBUG_MODE=coverage vendor/bin/phpunit
```
