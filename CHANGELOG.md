# Changelog

All notable changes to project will be documented in this file.

## [0.7.0](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.6.2...0.7.0) - 2024-05-28

### Bug Fixes

- Replace `dc:identifier` with `dc:source` for Media resource URL - ([3fb4c42](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/3fb4c42877379d9de75db52b5a53894deece01cd)) - Ambroise Maupate

### Documentation

- Add documentation and test with additional RDF property - ([38e750c](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/38e750cfe8dbab2e9c6da6c184f739ad8e8a5217)) - Ambroise Maupate

### Features

- Added missing `capdata:titre` on `LieuGeographique` - ([5af979b](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/5af979b6da93538eefa42d4858fc032c77c16bde)) - Ambroise Maupate

## [0.6.2](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.6.1...0.6.2) - 2024-05-23

### Bug Fixes

- Fixed fixture properties order - ([da5fc14](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/da5fc141e53e296d0b581b31530e994e8fcd6f83)) - Ambroise Maupate

## [0.6.1](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.6.0...0.6.1) - 2024-05-23

### Bug Fixes

- Removed `schema:alternateName` in `Referentiel` - ([b013e6f](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/b013e6fcfcc51aae216d6ceb2dd15618beef47a8)) - Ambroise Maupate

## [0.6.0](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.5.0...0.6.0) - 2024-05-23

### Bug Fixes

- Export only graph against `capdata` ontology by default - ([b6a3728](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/b6a37280cb904c00261db1ac0d5f834d4b51bee8)) - Ambroise Maupate
- Missing DublinCore properties for Media sub-classes - ([e6a0d93](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/e6a0d93148cbdb732f10600f933f76866c9aebe4)) - Ambroise Maupate

## [0.5.0](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.4.1...0.5.0) - 2024-04-19

### Bug Fixes

- Improved StringObject.php append method to concat multi-valued string together - ([1ebd633](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/1ebd633f3bccfc1d3f685ec8fb48f7abcb9791b8)) - Ambroise Maupate

### Documentation

- ml/json-ld is required for JSON-LD export - ([9166611](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/9166611c419697206cfd243b6d15c910d5718947)) - Ambroise Maupate
- Upgraded README TOC - ([7e37755](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/7e377550f7fac5f8807063a030105a0b491ec5e9)) - Ambroise Maupate
- `serialize` examples - ([3bbf8c3](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/3bbf8c3fa05082c2b7b871cb71c014be87e11cdc)) - Ambroise Maupate

### Features

- Segregate capdata and schema ontologies to export only one ontology at a time or both. Defaults to **both**. - ([128c15a](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/128c15a79b5d8c9c42362253754c0a43184c8de3)) - Ambroise Maupate

## [0.4.1](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.4.0...0.4.1) - 2024-04-11

### Bug Fixes

- Do not mess with native PHP __serialize methods, use `serialize` instead - ([4f5fcd5](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/4f5fcd58b1702e3bd6be0631d8dea7070f7075bb)) - Ambroise Maupate

### Testing

- Added StringObjectTrait.php isEmpty method to prevent handling empty UriObjects - ([730d0b9](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/730d0b99fa1a392e0a528dc49de378b6fd069501)) - Ambroise Maupate

## [0.4.0](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.3.2...0.4.0) - 2024-04-04

### ⚠ Breaking changes

- Removed obsolete `Atelier` class
- `ProductionPrimaire` is no longer a child class of `Production`
- `setAPourPartenaire()` relationship setter is replaced with `setAPourPartenariat()` and requires a `Partenariat` object

### Features

- Add *schema.org* interpolations for Participation relationships - ([66b4e65](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/66b4e65e694b1a973401460e924e0e699645ee83)) - Ambroise Maupate
-  [**breaking**]Removed obsolete `Atelier` class. Fixes #6 - ([e35cca6](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/e35cca6225ff2ea82155205b8c8a5e8c3e646185)) - Ambroise Maupate
-  [**breaking**]`ProductionPrimaire` is no longer a child class of `Production`. Both inherit `AbstractProduction`. fixes #5 - ([4d86942](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/4d869427bdc4eb01f41aceebf28ce677ffb2fa88)) - Ambroise Maupate
-  [**breaking**]Added `HasParticipation` and `HasLiveParticipation` interface, trait and Converter to deal with participation relationship and its children forms. fixes #7 #4 - ([b864e43](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/b864e43a2513d0a8b8b425fb71e04eabb4d64af5)) - Ambroise Maupate

## [0.3.2](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.3.1...0.3.2) - 2024-03-27

### Bug Fixes

- **(UriObject)** Allow passing URL with non-ascii characters but encode them with percent first. Fixed #3 - ([1a8a59a](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/1a8a59a52b497f7bf348265e145b7142092d4258)) - Ambroise Maupate

### Features

- Added `Participation` sub-classes - ([d26594d](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/d26594d1a8fb24abfe3f0e895ae89988f26b82ab)) - Ambroise Maupate

### Testing

- Added ParseTurtleTest.php to test if rdf:types and literal are valid - ([204e3b7](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/204e3b791d824d68b548be9e89d5809e676b26c4)) - Ambroise Maupate

## [0.3.1](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.3.0...0.3.1) - 2024-03-22

### Bug Fixes

- **(Namespaces)** Fixed namespace definitions and avoid overriding EasyRdf initial namespaces. - ([1e91a8b](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/1e91a8bfac26c2db33815aa3f26f1dc84545d9dd)) - Ambroise Maupate

## [0.3.0](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.2.0...0.3.0) - 2024-03-14

### ⚠ Breaking changes

- `Lieu::setAdresse` now only accepts `RelationObject<AdressePostale>` instead of `string`

### Bug Fixes

- Added missing `rof:Referentiel` rdf:type - ([ebc513a](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/ebc513ac65629882944d6af6bf73fb2bf6229281)) - Ambroise Maupate
- Added missing `identifiantRof` and `sameAs` to OntologyClass.php - ([71c2e3c](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/71c2e3c39e7c7b29fef4ec7cbd9f2e0bc38483fb)) - Ambroise Maupate
- Added missing `dateCreationRessource` and `dateModificationRessource` to CatalogClass.php - ([20dea65](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/20dea653b2733570714fb0b57e113f91b829bc80)) - Ambroise Maupate

### Features

- Added `Collectivite::adresse` and `Collectivite::aPourLieu` new relations - ([a811b10](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/a811b10bbb846a1cef7fc3fc52bc0f19e794c5dd)) - Ambroise Maupate
-  [**breaking**]Splitted `LieuGeographique` into 2 classes: `LieuGeographique` and `AdressePostale` - ([7dfbd84](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/7dfbd840c416d9ca6ab92e0cd7261755b8115cd2)) - Ambroise Maupate
- Added `frBnf` legacy property - ([aa6c5c3](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/aa6c5c3ada4061253300f21ce0368b5def5f83b1)) - Ambroise Maupate
- Added `pageWeb` parent property for `siteWeb`, `facebook`, `twitter`, `musicStory` - ([d8ba79a](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/d8ba79a991c6cbeb67dcb1ce091814c72f526f32)) - Ambroise Maupate

### Testing

- Added test with `Collectivite` having an `adresse` and 2 `lieux` - ([d0a9469](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/d0a9469cc2edd852bf72bc2ba712fcd5366429a4)) - Ambroise Maupate

## [0.2.0](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.1.1...0.2.0) - 2024-03-04

### ⚠ Breaking changes

- Every `RelationObject` setters will accept `ExternalThing` and not `Isni` no anymore.

### Bug Fixes

-  [**breaking**]Fixes #1, fixes #2: Do not use `Isni`  improperly to represent every external resources, use `ExternalThing` instead. - ([e0091f8](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/e0091f862a20fefec826462248d50c2a110eeaa1)) - Ambroise Maupate

## [0.1.1](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/compare/0.1.0...0.1.1) - 2024-03-04

### Bug Fixes

- Missing `rdf:type` for `ProductionPrimaire` - ([af497bc](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/af497bcf69872eb012c828dd4738aa522efd65b2)) - Ambroise Maupate

### Documentation

- Credits - ([ee6959d](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/ee6959dd2022f2cdc9e6baf9f599c7e26743ef3a)) - Ambroise Maupate
- Use Isni for external RelationObject - ([a17d0ba](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/a17d0ba9edd6b368c092e8c69da2977d7b5e07f8)) - Ambroise Maupate

## [0.1.0] - 2024-02-23

### Bug Fixes

- RelationObject always can hold Isni instead of its main model type. - ([19f17c9](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/19f17c9fee8d5ddfed54dfbd228ff8e2b0a98531)) - Ambroise Maupate

### CI/CD

- Parse coverage - ([51bfa44](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/51bfa44259b1628214e106d6ca2a20dbff2a2fd1)) - Ambroise Maupate
- Only run sonarqube if SONAR_HOST_URL is defined - ([4976031](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/4976031eb6d500b5492d294c101418de86654db5)) - Ambroise Maupate
- Ignore file for sonarqube - ([c9aef9b](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/c9aef9b8bfb3033ac7d823605b38ec0e603f85ba)) - Ambroise Maupate
- Use phpunit reports for Gitlab - ([9fe9cbf](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/9fe9cbff060f0ff534267acd0c0bfaac4b1cb7ba)) - Ambroise Maupate
- Display coverage from phpunit - ([ce7cb0e](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/ce7cb0ed20467482b0e96ee42387ebe1437876e9)) - Ambroise Maupate
- Get report from phpunit for sonarqube - ([b7c34c7](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/b7c34c7db9d1c2b44631835a18c1f56520ad7757)) - Ambroise Maupate
- Run code coverage - ([93c84b4](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/93c84b4746e403b9437cd270b3e9d04a9570c760)) - Ambroise Maupate
- Run code coverage - ([60f9a13](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/60f9a13042e00a9518f27789d5ac3bc8f5cc6e75)) - Ambroise Maupate
- Added unit-tests for Value objects - ([c32c42b](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/c32c42b0b57b1aac2fc2edc7bf1b5fd3a168a343)) - Ambroise Maupate
- Added SonarQube configuration - ([28f0d78](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/28f0d78f4ed49bd13c5430ce93d06857a46d75ca)) - Ambroise Maupate
- Install php exts - ([af9b2bb](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/af9b2bb30ab773b11cba260d2cf41ab9a201b2ca)) - Ambroise Maupate
- --prefer-dist - ([5c73be5](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/5c73be542fe9053e31f88540278ad53c34e4cca3)) - Ambroise Maupate
- Downgrade phpunit to be match php7.4 - ([6554d49](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/6554d496d1b56b185260df2967556640921f4c80)) - Ambroise Maupate
- Missing composer binary - ([73989cc](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/73989ccdac15bb39d09500d92fd745295566ebe2)) - Ambroise Maupate
- Added Gitlab CI matrix - ([2562605](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/25626052eb54485698a204a7aceb5bb6946cea4f)) - Ambroise Maupate

### Documentation

- Added CC-BY-SA license - ([8623bef](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/8623bef813d2a47d63640bb932d0acd36b069a82)) - Ambroise Maupate
- Added autogenerated CHANGELOG with git-cliff - ([684620d](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/684620dde3df00972ac8e76e03b85128eae4b4aa)) - Ambroise Maupate
- Added coverage badge - ([8b4b97e](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/8b4b97e92832c53fc6d5c5315b565d5d054fb07f)) - Ambroise Maupate
- Added badge - ([4fbef86](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/4fbef860216b22eff0483657529269989a3fc3f2)) - Ambroise Maupate
- Readme - ([ac08b7b](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/ac08b7b9e6440bb7936b4fd2e8cdfac61b32ed32)) - Ambroise Maupate
- Serializer test and documentation - ([214d8ad](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/214d8ad9b98d8feb2d44b33f366518d0cac32613)) - Ambroise Maupate
- Propriétés de classes - ([5d1160d](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/5d1160dd2ab9b17cde465a67cdd8280fe7343293)) - Ambroise Maupate
- Added TODO list for ontology classes - ([c9e53ce](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/c9e53ce934e6bbd354e3babf32b100e8db442890)) - Ambroise Maupate

### Features

- Added nomFormeRejet to Collectivite and missing converted properties - ([6046092](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/6046092fd71d6027ecd3775b710a2d9f367517be)) - Ambroise Maupate
- Added `schema:countryOfOrigin` for Oeuvre - ([9857573](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/98575735a2fc809911cd1a72652d3ddc0fc988d1)) - Ambroise Maupate
- Added ArkBnf external relation - ([f71c8d0](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/f71c8d04397e0d24532ca1c50d16deb2f6f74bf5)) - Ambroise Maupate
- Added Evenement and its unit tests - ([c91b903](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/c91b903a28a4829c0eb65f2395f630a1c38ebab6)) - Ambroise Maupate
- Added Production, Saison and their unit-tests - ([3db4bb4](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/3db4bb4f8a4aaba9092c3d91ef85bc861e6ea40c)) - Ambroise Maupate
- Throw custom InvalidGraphException - ([ff4ff53](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/ff4ff53f9fb48ebdb7dddd5ee4affd98ecb24182)) - Ambroise Maupate
- Oeuvre class can have media - ([df0a07a](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/df0a07a126e87e47404a2988e7e3b171fc0bc336)) - Ambroise Maupate
- Oeuvre class, converter and tests. Added DateIntervalObject - ([1dd33ba](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/1dd33ba07c56abff737b187f1fec36417b27b0b0)) - Ambroise Maupate
- Added Media and subclasses types and converters - ([2d3f836](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/2d3f8365f98ba02ee45dcc5866a9616ecb1ed9c1)) - Ambroise Maupate
- Added Pays referentiel - ([267c3dd](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/267c3dd913a60ce671f2906d85ab26b854bcca58)) - Ambroise Maupate
- Added Serializer class with default configuration for converters - ([c0387e9](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/c0387e9ad4d180ab9cbc39b7e7ffacc35ca5a0d8)) - Ambroise Maupate
- Added Boolean, Numeric value objects and tests - ([9eae3f3](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/9eae3f3f8d7783c8a689ff1702806830ff9179bb)) - Ambroise Maupate
- Added Converters for Ontology nodes and LiteralConverters for ValueObjects - ([8a7d1f2](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/8a7d1f2011fbe13d2a9d6c29a76aae3dbe574d0f)) - Ambroise Maupate

### Testing

- Missing timezone for CSV datetimes - ([bdf27e1](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/bdf27e1ec8516678e9da2e725062ec097a64d6f6)) - Ambroise Maupate
- Added unit tests from CSV file for Collectivite - ([2ffd8e0](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/2ffd8e0a8abcaf1384aa5a094ecaccfc4ccffcbe)) - Ambroise Maupate
- Added person example for Mozart, Verdi, Chagnon - ([1558409](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/155840959f38e3e489d055581870461356f03a1d)) - Ambroise Maupate
- InterpretationConverterTest and better OeuvreConverterTest - ([6581199](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/6581199add9d582532b3f39c730fd1bbdb5c5b33)) - Ambroise Maupate
- Swtich to turtle fixtures for better legibility - ([68532e0](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/68532e066d149c89b6b6877d52837c43066db76f)) - Ambroise Maupate
- Test RDF output for GraphConverter - ([6441313](https://gitlab.com/capdataopera/capdataopera-php-sdk/-/commit/64413135f82e59d33c3ff896a125dfdee895c7bc)) - Ambroise Maupate

<!-- generated by git-cliff -->
